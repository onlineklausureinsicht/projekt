/**
 * @author Marius Hagen
 * Stellt das Model eines Studiengangs
 */

package models;

public class Studiengang {

	private String bezeichnung;

	
	public Studiengang(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}

	/*// primary, dont use this
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}*/
	
	/**
	 * Liefert eine komplette Zeile für ein DefaultTableModel
	 * @return Object[]
	 */
	public Object[] getRow() {
		Object[] o = { bezeichnung };
		return o;
	}

}