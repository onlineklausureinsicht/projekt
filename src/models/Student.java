package models;

/**
 * @author A. Zensen
 *
 */
public class Student {

	private int matnr;
	private String name;
	private String vorname;
	private String benutzer;
	private String email;
	private String studiengang;


	public Student(int matnr, String name, String vorname, String email, String studiengang, String benutzer) {
		this.matnr = matnr;
		this.name = name;
		this.vorname = vorname;
		this.benutzer = benutzer;
		this.email= email;
		this.studiengang = studiengang;

	}
	
	
	public int getMatnr() {
		return this.matnr;
	}

	public void setMatnr(int matnr) {
		this.matnr = matnr;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return this.vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getBenutzer() {
		return this.benutzer;
	}

	public void setBenutzer(String benutzer) {
		this.benutzer = benutzer;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStudiengang() {
		return studiengang;
	}

	public void setStudiengang(String studiengang) {
		this.studiengang = studiengang;
	}
	
    /**
     * @return Object-Array f�r einfaches Hinzuf�gen zu einem TableModel
     * return = {matnr, name, vorname, email, studiengang, benutzer}
     */
    public Object[] getRow() {
        Object[] o = {matnr, name, vorname, email, studiengang, benutzer};
        return o;
    }

}