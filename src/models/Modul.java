/**
 * @author Marius Hagen
 * Stellt Das Model eines Moduls
 */

package models;

public class Modul {

	private String bezeichnung;

    public Modul(String bezeichnung){
        this.bezeichnung = bezeichnung;
    }
    

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	/*//unused!
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}*/

	/**
	 * Liefert eine komplette Zeile für ein DefaultTableModel
	 * @return Object[]
	 */
    public Object[] getRow() {
        Object[] o = { bezeichnung }; //Array-size will be 1
        return o;
    }

}