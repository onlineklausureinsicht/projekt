package models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import SQL.DBConnection;
import views.LogInV2;
/**
 * @author Alexander Klippert
 *
 */
public class LogInM2 {

	private LogInV2 view;
	
	public LogInM2() {
		// TODO Auto-generated constructor stub
	}
	
	public void register(LogInV2 view) {
		this.view = view;
	}
	public boolean getLogin(String user, String pw){
		
		boolean status = false;

		Connection con = DBConnection.getCon();
		String sql = "SELECT benutzer,passwort FROM verwalterlogin WHERE '" + pw + 
				"' = passwort AND benutzer = '" + user + "'";
//		System.out.println(sql); // Ueberpruefung der Benutzereingaben - zum Testen

		try {
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery(sql);
			
			/*
			 * Mit dem ResultSet wird ueberprueft, ob
			 * es den Benutzer in Kombination mit dem 
			 * Passwort gibt. Sollte die Eingabe korrekt
			 * sein, wird status = true gesetzt und mit
			 * return uebergeben. Ansonsten bleibt status 
			 * auf false und wird auch uebergeben.
			 * true und false entscheiden ob der Benutzer
			 * angemeldet ist oder nicht. Siehe hierfuer in
			 * die LogInV2.java.
			 */

			if (rs.next()) {
				
				status = true;
				
				rs.close();
		
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			System.err.println("Es ist ein Fehler augetreten:");
			System.err.println(e1);
			
		}
		return status;
	}
}
