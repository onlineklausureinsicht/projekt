/**
 * @author Marius Hagen
 * Stellt das Model eines Dozenten.
 */

package models;

public class Dozent {

    private int dozent_id;
    private String name;
    private String vorname;
    private String email;
    private boolean update;
    private boolean insert;
    private boolean delete;

    public Dozent(int dozent_id, String name, String vorname, String email) {
	    this.dozent_id = dozent_id;
	    this.name = name;
	    this.vorname = vorname;
	    this.email = email;
	    this.update = false;
	    this.insert = false;
	    this.delete = false;
	}

	//for dozenten creating inside our system
    public Dozent(boolean insert, int dozent_id, String name, String vorname, String email) {
		this.dozent_id = dozent_id;
	    this.name = name;
	    this.vorname = vorname;
	    this.email = email;
	    this.insert = true;
	    this.update = false;
	    this.delete = false;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public boolean isInsert() {
		return insert;
	}

	public void setInsert(boolean insert) {
		this.insert = insert;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public int getDozentId() {
        return this.dozent_id;
    }

//    public void setDozent_id(int dozent_id) {
//        this.dozent_id = dozent_id;
//    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
        this.update = true;
    }

    public String getVorname() {
        return this.vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
        this.update = true;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
        this.update = true;
    }

    public Object[] getRow() {
        Object[] o = {dozent_id, name, vorname, email};
        return o;
    }
}
