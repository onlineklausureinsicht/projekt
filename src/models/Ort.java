/*
 ******************************************************** 
 *Datei erstellt von | am
 *	Maik Nowack		| 28.11.2014
 ********************************************************
 *Änderung von		| am		 | was
 *	Maik Nowack		| 29.11.2014 | Konstruktor Ort()
 ********************************************************
 */

package models;

/**
 * Abbild der Ort Relation
 * 
 * @author Maik Nowack
 * @version 1.0
 */
public class Ort {

	private int ortid;
	private String standort;
	private String raum;
	private String adresse;
	private String ort;

	/**
	 * Konstruktor
	 * 
	 * @param standord
	 *            :String - Standort, Bezeichnung des Standortes
	 * @param raum
	 *            :String - Raum, Bezeichnung des Raumes oder Raumnummer
	 * @param adresse
	 *            :String - Adresse
	 * @param ort
	 *            :String - Ort
	 * @param ortid
	 *            :int - unique key ueber die der Ort in der Datenbank eindeutig
	 *            identifiziert werden kann
	 */
	public Ort(String standord, String raum, String adresse, String ort,
			int ortid) {
		this.ortid = ortid;
		this.standort = standord;
		this.raum = raum;
		this.adresse = adresse;
		this.ort = ort;
	}

	// 29.11.2014
	/**
	 * leerer Konstruktor
	 */
	Ort() {

	}

	/**
	 * @return :int; gibt den unique key zurueck, uber den dieser Ort in der
	 *         Datenbank zu finden ist.
	 */
	public int getOrtid() {
		return ortid;
	}

	/**
	 * @param ortid
	 *            :int - unique key ueber die der Ort in der Datenbank eindeutig
	 *            identifiziert werden kann
	 */
	public void setOrtid(int ortid) {
		this.ortid = ortid;
	}

	/**
	 * @return :String; gibt den aktuellen Standort des Ortes zurueck
	 */
	public String getStandort() {
		return standort;
	}

	/**
	 * @param standort
	 *            :String
	 */
	public void setStandort(String standort) {
		this.standort = standort;
	}

	/**
	 * @return :String; gibt die aktuelle Bezeichnung des Ortes zur�ck
	 */
	public String getRaum() {
		return raum;
	}

	/**
	 * @param raum
	 *            :String
	 */
	public void setRaum(String raum) {
		this.raum = raum;
	}

	/**
	 * @return :String; gibt die aktuele Adresse des Ortes zurueck
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse
	 *            :String
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return :String; gibt den Ort zurueck
	 */
	public String getOrt() {
		return ort;
	}

	/**
	 * @param ort
	 *            :String
	 */
	public void setOrt(String ort) {
		this.ort = ort;
	}

	/**
	 * @return :Object[]; gibt ein Array der Form {Standort:String, Raum:String,
	 *         Adresse:String, Ort:String, Ortid:int} zurueck
	 */
	public Object[] getRow() {
		Object[] row = { this.getStandort(), this.getRaum(), this.getAdresse(),
				this.getOrt(), this.getOrtid() };
		return row;
	}
}
