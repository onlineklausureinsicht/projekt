package models;

public class Termin {

	private int terminid;
	private String datum;
	private String standort;
	private String raum;
	private String zeit1;
	private String zeit2;
	private boolean gesperrt;
	private String studiengang;

	public Termin(int terminid, String datum, String standort, String raum,
			String zeit1, String zeit2, boolean gesperrt, String studiengang) {
		super();
		this.terminid = terminid;
		// SQL übergibt den Datumswert in der Form "2014.05.24 16:00"
		this.datum = new String(datum.substring(8, 10) + "." + datum.substring(5, 7) + "." + datum.substring(0, 4));
		this.standort = standort;
		this.raum = raum;
		this.zeit1 = zeit1;
		this.zeit2 = zeit2;
		this.gesperrt = gesperrt;
		this.studiengang = studiengang;
	}

	public int getTerminid() {
		return terminid;
	}

	public void setTerminid(int terminid) {
		this.terminid = terminid;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = new String(datum.substring(8, 10) + "." + datum.substring(5, 7) + "." + datum.substring(0, 4));
	}

	public void setDate(String datum) {
		this.datum = datum;
	}
	
	public String getStandort() {
		return standort;
	}

	public void setStandort(String standort) {
		this.standort = standort;
	}

	public String getRaum() {
		return raum;
	}

	public void setRaum(String raum) {
		this.raum = raum;
	}

	public String getZeit1() {
		return zeit1;
	}

	public void setZeit1(String zeit1) {
		this.zeit1 = zeit1;
	}

	public String getZeit2() {
		return zeit2;
	}

	public void setZeit2(String zeit2) {
		this.zeit2 = zeit2;
	}

	public boolean isGesperrt() {
		return gesperrt;
	}

	public void setGesperrt(boolean gesperrt) {
		this.gesperrt = gesperrt;
	}

	public String getStudiengang() {
		return studiengang;
	}

	public void setStudiengang(String studiengang) {
		this.studiengang = studiengang;
	}

	public Object[] getRow() {

		String sperre = "---";
		if (this.isGesperrt())
			sperre = "gesperrt";

		Object[] row = { this.getDatum(), this.getStandort(), this.getRaum(),
				this.getZeit1(), this.getZeit2(), sperre, this.terminid,
				this.studiengang };
		return row;
	}

}