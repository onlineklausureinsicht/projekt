/**
 * @author Marius Hagen
 * Stellt das Model einer Klausur zur verfügung, wobei die TerminDaten denen auf dem TerminModel bezogen sind.
 */

package models;

public class Klausur {
	private int matrikelNummer;
    private int dozentId;
    private int klausurId;
	private String modul;
	private String terminDatum;
	private String terminStandort;
	private String terminRaum;

	public Klausur(int matrikelNummer, String modul, int dozentId,
				   String terminDatum, String terminStandort, String terminRaum, int klausurId) {
		
	        this.matrikelNummer = matrikelNummer;
	        this.modul = modul;
	        this.dozentId = dozentId;
//	        this.terminDatum = terminDatum;
	        //terminDatum can be null or set to "". to handle the right format, just approve the size of the input
	        if(terminDatum != null) if(terminDatum.length() >= 10) this.terminDatum = new String(terminDatum.substring(8, 10) + "." + terminDatum.substring(5, 7) + "." + terminDatum.substring(0, 4));
	        else this.terminDatum = null; //if klausur-terminDatum is empty, internally its handled as null
	        this.terminStandort = terminStandort;
	        this.terminRaum = terminRaum;
	        this.klausurId = klausurId;
	}

	public int getMatrikelNummer() {
            return matrikelNummer;
	}

	public String getModul() {
            return modul;
	}

	public int getDozentId() {
            return dozentId;
	}

	public String getTerminDatum() {
            return terminDatum;
	}

	public String getTerminStandort() {
            return terminStandort;
	}

	public String getTerminRaum() {
            return terminRaum;
	}

    public int getKlausurId() {
            return klausurId;
        }

	public void setTerminRaum(String terminRaum) {
            this.terminRaum = terminRaum;
	}

	public void setTerminStandort(String terminStandort) {
            this.terminStandort = terminStandort;
	}

	public void setTerminDatum(String terminDatum) {
            this.terminDatum = terminDatum;
	}

	public void setDozentId(int dozentId) {
            this.dozentId = dozentId;
	}

//	public void setModul(String modul) {
//            this.modul = modul;
//	}

//	public void setMatrikelNummer(int matrikelNummer) {
//            this.matrikelNummer = matrikelNummer;
//	}
//
//        private void setKlausurId(int klausurId) { //this one is private, because there should be no reason to change klausurid
//            this.klausurId = klausurId;
//        }
	/**
	 * Liefert eine komplette Zeile für ein DefaultTableModel
	 * @return Object[]
	 */
	public Object[] getRow() {
		Object[] o = { matrikelNummer, modul, dozentId, terminDatum, terminStandort, terminRaum, klausurId };
		return o;
	}

}