/*
 ******************************************************** 
 *Datei erstellt von | am
 *	Maik Nowack		 | 28.11.2014
 ********************************************************
 *Änderung von		 | am		  | was
 * Alexander Klippert	|	15.12.2014	      | deleteOrt
 ********************************************************
 */

package register;

import java.util.TreeMap;
import javax.swing.table.DefaultTableModel;

import models.Ort;
import SQL.OrtSQL;

/**
 * Schnittstelle zur Datenbankebene
 * 
 * @author Maik Nowack
 * @version 1.0
 */
public class OrtRegister {

	private TreeMap<Integer, Ort> orte;

	/**
	 * 
	 */
	public OrtRegister() {
		this.refresh();
	}

	/**
	 * Diese Methode gibt den zum unique key zugehoerigen Ort zurueck, falls zum
	 * unique key ein Ort gespeichert ist.
	 * 
	 * @param ortid
	 *            :int - unique key ueber den der Ort in der Datenbank gefunden
	 *            werden kann.
	 * @return :Ort; der Ort der unter dem unique key in der Datenbank
	 *         hinterlegt ist.
	 */
	public Ort getOrt(int ortid) {
		if (orte.containsKey(ortid))
			return orte.get(ortid);
		else
			return null;
	}

	/**
	 * @return :TreeMap<Integer, Ort>; gibt eine TreeMap mit allen in der
	 *         Datenbank gespeicherten Orten zurueck.
	 */
	public TreeMap<Integer, Ort> getOrte() {
		return orte;
	}

	/**
	 * Methode um einen Ort der Datenbank hinzuzufuegen.
	 * 
	 * @param ort
	 *            :Ort - Ein Ort der in der Datenbank hinterlegt werden soll.
	 *            Achtung die Ortid muss unique sein.
	 * @return :boolean; true wenn der Ort gespeichert werden konnte, false wenn
	 *         ein Fehler aufgetreten ist.
	 */
	public boolean addOrt(Ort ort) {
		if (ort != null) {
			boolean doneSQL = OrtSQL.insert(ort.getOrtid(), ort); // SQL
																	// eingefuegt
			if (doneSQL)
				orte.put(ort.getOrtid(), ort);
			return true;
		}
		return false;
	}

	/**
	 * Methode um die Eigenschaften eines Ortes in der Datenbank zu
	 * aktualisieren.
	 * 
	 * @param ortid
	 *            :int - unique key unter dem der Ort in der Datenbank zu finden
	 *            ist.
	 * @param ort :Ort
	 * @return :boolean; true wenn der Ort aktualisiert werden konnte, false
	 *         wenn ein Fehler aufgetreten ist.
	 */
	public boolean setOrt(int ortid, Ort ort) {
		if (ort != null) {
			Ort ok = orte.put(ortid, ort);
			if (ok == null)
				return false;
			else
				return true;
		}
		return false;
	}

	/**
	 * Methode um einen Ort an Hand seines unique keys zu loeschen.
	 * 
	 * @param ortid
	 *            :int - unique key unter dem der Ort in der Datenbank zu finden
	 *            ist.
	 * @return :boolean; true wenn der Ort geloescht werden konnte, false wenn
	 *         ein Fehler aufgetreten ist.
	 */
	public boolean deleteOrt(int ortid) {

		if (orte.containsKey(ortid)) {
			boolean doneSQL = OrtSQL.delete(ortid); // SQL eingefügt
			if (doneSQL) {
				Ort ok = orte.remove(ortid);
				if (ok != null)
					return true;
			}
		}
		return false;
	}

	/**
	 * 
	 */
	public void refresh() {
		orte = OrtSQL.getTreeMap();
	}

	/**
	 * Methode zum befuellen der Tabelle mit den Orten.
	 * 
	 * @return :DefaultTableModel;
	 */
	public DefaultTableModel getTableModel() {
		String[] columns = { "Standort", "Raum", "Adresse", "Ort", "Ort ID" };

		DefaultTableModel dtm = new DefaultTableModel(columns, 0) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (Ort t : orte.values()) {
			dtm.addRow(t.getRow());
		}

		return dtm;
	}

	/**
	 * Methode zur vergabe des unique keys.
	 * 
	 * @return :int; gibt den hoechsten verwendeteten unique key mit eins
	 *         erhoeht zurueck
	 */
	public int lastKey() {
		return orte.lastKey() + 1;
	}
}
