/**
 * @author Marius Hagen
 * Hält in der TreeMap alle Termine und bietet Möglichkeiten zur Manipulation
 */

package register;

import javax.swing.table.DefaultTableModel;

import java.util.TreeMap;

import models.Termin;
import SQL.TerminSQL;

public class TerminRegister {

	private TreeMap<Integer, Termin> termine;
	private String studiengang;

	/**
	 * Läd alle Termine mithilfe der TerminSQL aus der Datenbank
	 */
	public TerminRegister() {
		this.termine = TerminSQL.getTreeMap();
	}

	/**
	 * Läd nur Termine in bezug auf einen Studiengang
	 * @param studiengang
	 */
	public TerminRegister(String studiengang) {
		this.studiengang = studiengang;
		this.termine = TerminSQL.getTreeMap(studiengang);
	}

	/**
	 * Sucht das TerminModel zu der angegebenen terminid
	 * @param terminid
	 * @return Termin
	 */
	public Termin getTermin(int terminid) {
		return termine.get(terminid);
	}

	/**
	 * Fuegt einen Termin hinzu
	 * @param termin
	 * @return boolean
	 */
	public boolean addTermin(Termin termin) {
		boolean erg = TerminSQL.insert(termin.getTerminid(), termin);
		if (erg) {
			termine.put(termin.getTerminid(), termin);
			return true;
		}
		return false;
	}

	/**
	 * Veraendert das TerminModel an der gewünschten terminid
	 * @param terminid
	 * @param termin
	 * @return boolean
	 */
	public boolean setTermin(int terminid, Termin termin) {
		boolean erg = TerminSQL.update(termin.getTerminid(), termin);
		if (erg) {
			termine.put(terminid, termin);
			return true;
		} else
			return false;
	}

	/**
	 * Loescht den Termin an der gewuenschten position
	 * @param terminid
	 * @return boolean
	 */
	public boolean deleteTermin(int terminid) {
		boolean doneSQL = TerminSQL.delete(terminid);
		if (doneSQL)
			termine.remove(terminid);
		return doneSQL;
	}

	/**
	 * Laed die Termine erneut aus der Datenbank
	 */
	public void refresh() {
		if (studiengang == null) {
			this.termine = TerminSQL.getTreeMap();
		} else {
			this.termine = TerminSQL.getTreeMap(studiengang);
		}
	}

	/**
	 * Generiert aus der TreeMap ein DefaultTableModel
	 * @return DefaultTableModel
	 */
	public DefaultTableModel getTableModel() {

		String[] columns = { "Datum", "Standort", "Raum", "1. Zeitraum",
				"2. Zeitraum", "Sperre", "Termin ID", "Studiengang" };

		DefaultTableModel dtm = new DefaultTableModel(columns, 0) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (Termin t : termine.values()) {
			dtm.addRow(t.getRow());
		}

		return dtm;
	}

	/**
	 * Generiert aus der TreeMap ein DefaultTableModel, welches nur Termine zu einem bestimmten Studiengang enthalten soll
	 * @param studiengang
	 * @return DefaultTableModel
	 */
	public DefaultTableModel getTableModel(String studiengang) {
		String[] columns = { "Datum", "Standort", "Raum", "1. Zeitraum",
				"2. Zeitraum", "Sperre", "Termin ID", "Studiengang" };

		DefaultTableModel dtm = new DefaultTableModel(columns, 0) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (Termin t : termine.values()) {
			if (t.getStudiengang().equals(studiengang)) {
				dtm.addRow(t.getRow());
			}
		}

		return dtm;
	}

	/**
	 * Generiert fortlaufend neue Schluessel
	 */
	public int lastKey() {
		// TODO Auto-generated method stub
		return (termine.lastKey() + 1);
	}

	/**
	 * Gibt die TreeMap weiter
	 * @return TreeMap<Integer, Termin>
	 */
	public TreeMap<Integer, Termin> getTermine() {
		return termine;
	}

}