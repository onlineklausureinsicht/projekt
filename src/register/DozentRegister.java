/**
 * @author Marius Hagen
 * Hält in der TreeMap alle Dozenten und bietet Möglichkeiten zur Manipulation
 */

package register;

import javax.swing.table.DefaultTableModel;

import java.util.TreeMap;

import SQL.DozentSQL;
import models.Dozent;

public class DozentRegister {

	private TreeMap<Integer, Dozent> dozenten = new TreeMap<Integer, Dozent>();

	public DozentRegister() {
        // TODO - implement DozentRegister.DozentRegister
        //throw new UnsupportedOperationException();
        refresh(); //this will load the list via sql-class
	}

	/**
	 * Dozent Hinzufuegen
	 * @param dozent
	 */
	public void addDozent(Dozent dozent) {
//	    int dozent_id = dozenten.lastKey()+1;
//	    dozent.setDozent_id(dozent_id); //no other generated keys than my have to be used!
//	    boolean doneSQL = DozentSQL.insert(dozent_id, dozent);
//	    if(doneSQL)dozenten.put(dozent_id, dozent);
		
		dozenten.put(dozent.getDozentId(), dozent);
	}

	/**
	 * Dozenten veraendern / anlegen mit bestimmter ID, wenn noch nicht vorhanden
	 * @param dozent
	 */
	public void setDozent(int dozent_id, Dozent dozent) {
	    // TODO - implement DozentRegister.setDozent
	    //throw new UnsupportedOperationException();
	    boolean doneSQL = DozentSQL.update(dozent_id, dozent);
	    if(doneSQL) dozenten.put(dozent_id, dozent);
	}

	/**
	 * Dozent-Model anhand der ID heraussuchen
	 * @param dozent_id
	 */
	public Dozent getDozent(int dozent_id) {
	    // TODO - implement DozentRegister.getDozent
	    //throw new UnsupportedOperationException();
	    return dozenten.get(dozent_id);
	}

	public TreeMap<Integer, Dozent> getDozentenMap() {
        // TODO - implement DozentRegister.getDozentenMap
        return dozenten;
	}

	/**
	 * Das DozentenRegister anhand einer anderen TreeMap speisen
	 * @param dozenten
	 */
	public void setDozenten(TreeMap<Integer, Dozent> dozenten) {
        boolean doneSQL = true;
        for(int key : dozenten.keySet())
        {
            doneSQL &= DozentSQL.update(key, dozenten.get(key));
            //only if every changed sql went good, doneSQL stays true.
        }
        if(doneSQL) this.dozenten = dozenten;
	}

	/**
	 * Einen Dozenten aus den Register und der Datenbank entfernen
	 * @param dozentid
	 */
	public void deleteDozent(int dozentid) {
	//  boolean doneSQL = DozentSQL.delete(dozent_id);
	//  if(doneSQL) dozenten.remove(dozent_id);
			
		Dozent d = dozenten.get(dozentid);
		d.setDelete(true);
    }

	/**
	 * Register neu aus Datenbank laden
	 */
    public void refresh() {
        this.dozenten = DozentSQL.getTreeMap();
    }

    /**
     * Alle Dozenten mit ihren Daten in einem TableModel auflisten
     * @return DefaultTableModel
     */
    public DefaultTableModel getTableModel() {

        String[] ColumnNames = {"ID", "Nachname", "Vorname", "EMail Adresse"};
        DefaultTableModel dtm = new DefaultTableModel((Object[])ColumnNames,0){
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
            
        for(Dozent d : dozenten.values()) {
            if(!d.isDelete()) { dtm.addRow(d.getRow()); }
        }

        return dtm;
    }
    
    /**
     * Dozenten-IDs fortlaufend generieren
     * @return int
     */
    public int lastKey() {
		return dozenten.lastKey()+1;
	}

}