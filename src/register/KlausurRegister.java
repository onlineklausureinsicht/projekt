/**
 * @author Marius Hagen
 * Hält in der TreeMap alle Klausuren und bietet Möglichkeiten zur Manipulation
 */

package register;

import javax.swing.table.DefaultTableModel;

import java.util.TreeMap;

import SQL.KlausurSQL;
import models.Klausur;

public class KlausurRegister {

	private TreeMap<Integer, Klausur> klausuren = new TreeMap<Integer, Klausur>();
	private int matnr;
	
	/**
	 * Initialisierung des Register Objekts ohne abhängingkeit vom Studenten
	 */
	public KlausurRegister() { //only call from Verwalter
		matnr = 0;
		refresh(); //this will load the map via sql-class
	}
	
	/**
	 * Initialisierung des Register Objekts mit abhängingkeit vom Studenten
	 * @param matrikelnummer
	 */
	public KlausurRegister(int matrikelnummer) { //only call from Student
		matnr = matrikelnummer;
		refresh(); //this will load the map via sql-class
	}
	
	public TreeMap<Integer, Klausur> getKlausurenMap() {
		return this.klausuren;
	}

	/**
	 * Läd eine neue TreeMap ins Register und versucht dieses in der Datenbank festzuschreiben
	 * @param klausuren
	 * @return boolean
	 */
	public boolean setKlausurenMap(TreeMap<Integer, Klausur> klausuren) {
		
		boolean doneSQL = true;
		for(int key : klausuren.keySet())
		{
			doneSQL &= KlausurSQL.update(key, klausuren.get(key));
			//only if every changed sql went good, doneSQL stays true.
		}
		if(doneSQL) this.klausuren = klausuren;
		return doneSQL;
	}

	/**
	 * Sucht zu der Klausurid das Passende KlausurModel heraus.
	 * @param klausurid
	 * @return Klausur
	 */
	public Klausur getKlausur(int klausurid) {
		return klausuren.get(klausurid);
	}

	/**
	 * Fügt eine Klausur in die TreeMap hinzu
	 * @param klausur
	 * @return boolean
	 */
	public boolean addKlausur(Klausur klausur) {
		boolean doneSQL = KlausurSQL.insert(klausuren.lastKey()+1, klausur);
        if(doneSQL) klausuren.put(klausuren.lastKey()+1, klausur);
        return doneSQL;
	}

	/**
	 * Ueberschreibt das KlausurModel an der gewünschten ID, oder legt diese an, wenn die ID noch nicht vergeben ist
	 * @param id
	 * @param klausur
	 * @return
	 */
	public boolean setKlausur(int id, Klausur klausur) {
		boolean doneSQL = KlausurSQL.update(id, klausur);
		if(doneSQL) klausuren.put(id, klausur);
		return doneSQL;
	}

	/**
	 * Löscht Das KlausurModel an der ID
	 * @param id
	 * @return boolean
	 */
	public boolean deleteKlausur(int id) {
		boolean doneSQL = KlausurSQL.delete(id);
		if(doneSQL) klausuren.remove(id);
		return doneSQL;
	}

	/**
	 * Läd die Klausur-TreeMap neu aus der Datenbank
	 */
	public void refresh() {
		if(matnr != 0) this.klausuren = KlausurSQL.getTreeMap(matnr);   //as student
		else this.klausuren = KlausurSQL.getTreeMap();                  //as verwalter
	}

	/**
	 * Generiert aus der TreeMap ein DefaultTableModel
	 * @return
	 */
	public DefaultTableModel getTableModel() {

		String[] ColumnNames = {"Matrikelnummer", "Modulbezeichnung", "Dozent ID", "Termin Datum", "Termin Standort", "Termin Raum", "Klausur ID"};
		
		DefaultTableModel dtm = new DefaultTableModel((Object[])ColumnNames,0){
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		for(Klausur k : klausuren.values()) {
			dtm.addRow(k.getRow());
		}
		
		return dtm;
	}
	
	/**
	 * Generiert fortlaufend den nächsten Schluessel
	 * @return int
	 */
	public int lastKey() {
		return klausuren.lastKey()+1;
	}

}