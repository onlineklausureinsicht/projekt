/**
 * @author Marius Hagen
 * Haelt in der TreeMap alle Module und bietet Moeglichkeiten zur Manipulation
 */

package register;

import javax.swing.table.DefaultTableModel;

import java.util.TreeMap;

import models.Modul;
import SQL.ModulSQL;

public class ModulRegister {
    private TreeMap<String,Modul> module = new TreeMap<String,Modul>();

    public ModulRegister() {
        refresh(); //this will load the list via sql-class
    }

    public TreeMap<String,Modul> getModulMap() {
        return module;
    }
    
    /**
	 * Läd eine neue TreeMap ins Register und versucht dieses in der Datenbank festzuschreiben
	 * @param module
	 */
    public void setModulMap(TreeMap<String,Modul> module) {

        boolean doneSQL = true;
        for(String key : module.keySet())
        {
            doneSQL &= ModulSQL.update(key, module.get(key));
            //only if every changed sql went good, doneSQL stays true.
        }
        if(doneSQL) this.module = module;
    }

    /**
     * Sucht anhand der bezeichnung das Modul und gibt dieses Zurueck.
     * @param bezeichnung
     * @return Modul
     */
    public Modul getModul(String bezeichnung) {
        return module.get(bezeichnung);
    }

    /**
     * Fuegt ein Modul in die Datenbank und ins Register ein.
     * @param modul
     * @throws IllegalAccessException
     */
    public void addModul(Modul modul) throws IllegalAccessException {
        if(module.get(modul.getBezeichnung()) != null) {
        	
        	//this method is taking the TreeMap-key out of the object loading in TM
            boolean doneSQL = ModulSQL.insert(modul.getBezeichnung(), modul);
            if(doneSQL)
            {
                //here bezeichnung need to be predefined unlike in other registers
                module.put(modul.getBezeichnung(), modul);
            }
        }
        //have to be erased if Modul get more or an other than this primary-key
        else throw new IllegalAccessException("Es ist bereits ein Modul mit dieser Bezeichnung vorhanden");
    }

    /**
     * Veraendert Das modul anhand der Schluessels(bezeichnung) oder fuegt dieses ein,
     * wenn es noch keinen Eintrag zu der Bezeichnung(Schluessel) in der TreeMap gibt.
     * @param bezeichnung
     * @param modul
     */
    public void setModul(String bezeichnung, Modul modul) {
        boolean doneSQL = ModulSQL.update(bezeichnung, modul);
        if(doneSQL) module.put(bezeichnung, modul);
    }
    /**
     * Loescht das Modul mit dieser Bezeichnung
     * @param bezeichnung
     */
    public void deleteModul(String bezeichnung) {
        boolean doneSQL = ModulSQL.delete(bezeichnung);
        if(doneSQL) module.remove(bezeichnung);
    }

    /**
     * Laed alle Module erneut aus der Datenbank
     * Kann genutzt werden um asynchronitaet auszugleichen
     */
    public void refresh() {
        this.module = ModulSQL.getTreeMap();
    }

    /**
     * Generiert aus der TreeMap ein DefaultTableModel,
     * in dem alle Module mit ihren Eigenschaften aufgelistet sind.
     * @return DefaultTableModel
     */
    public DefaultTableModel getTableModel() {

        String[] ColumnNames = {"Modulbezeichnung"};
        DefaultTableModel dtm = new DefaultTableModel((Object[])ColumnNames,0){
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for(Modul m : module.values()) {
            dtm.addRow(m.getRow());
        }

        return dtm;
    }

}