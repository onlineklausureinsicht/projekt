/**
 * @author Marius Hagen
 * Hält in der TreeMap alle Studiengaenge und bietet Moeglichkeiten zur Manipulation
 */

package register;

import java.util.TreeMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

import SQL.StudiengangSQL;
import models.Studiengang;

public class StudiengangRegister {

	private TreeMap<String, Studiengang> studiengaenge = new TreeMap<String, Studiengang>();
	
	public StudiengangRegister() {
		refresh(); //this will load the map via sql-class
	}
	
	/**
	 * Laesst auf die TreeMap zugreifen.
	 * @return TreeMap<String, Studiengang>
	 */
	public TreeMap<String, Studiengang> getStudiengangMap() {
		return this.studiengaenge;
	}

	/**
	 * Sucht zu der Bezeichnung(Schluessel) den Passenden Studiengang heraus
	 * @param bezeichnung
	 * @return Studiengang
	 */
	public Studiengang getStudiengang(String bezeichnung) {
		return studiengaenge.get(bezeichnung);
	}

	/**
	 * Fuegt einen Studiengang anhand seiner Bezeichnung ins Register und ueber StudiengangSQL in der Datenbank ein.
	 * @param studiengang
	 * @return boolean, true wenn hinzugefuegt
	 */
	public boolean addStudiengang(Studiengang studiengang) {
		boolean doneSQL = StudiengangSQL.insert(studiengang.getBezeichnung(), studiengang);
        if(doneSQL) studiengaenge.put(studiengang.getBezeichnung(), studiengang);
        return doneSQL;
	}

	/**
	 * Loescht den Studiengang anhand seiner Bezeichnung wieder
	 * @param bezeichnung
	 * @return boolean, true wenn gelöscht
	 */
	public boolean deleteStudiengang(String bezeichnung) {
		boolean doneSQL = StudiengangSQL.delete(bezeichnung);
		if(doneSQL) studiengaenge.remove(bezeichnung);
		return doneSQL;
	}

	/**
	 * Laed die Daten des Studiengangs erneut aus der Datenbank in das Register
	 */
	public void refresh() {
		studiengaenge = StudiengangSQL.getTreeMap();
	}

	/**
	 * Generiert aus der TreeMap ein DefaultTableModel.
	 * @return DefaultTableModel
	 */
	public DefaultTableModel getTableModel() {

		String[] ColumnNames = {"Bezeichnung"};
		
		DefaultTableModel dtm = new DefaultTableModel((Object[])ColumnNames,0){
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		for(Studiengang s : studiengaenge.values()) {
			dtm.addRow(s.getRow());
		}
		
		return dtm;
	}
	
	/**
	 * Generiert die Daten aus der TreeMap als DefaultComboBoxModel
	 * @return DefaultComboBoxModel
	 */
	public DefaultComboBoxModel<String> getCbm () {
		DefaultComboBoxModel<String> dcbm = new DefaultComboBoxModel();
		for(Studiengang studgang : this.studiengaenge.values()) {
			dcbm.addElement(studgang.getBezeichnung());
		}
		
		return dcbm;
	}

}