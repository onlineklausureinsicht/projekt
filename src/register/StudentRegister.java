package register;

import javax.swing.table.DefaultTableModel;

import java.util.TreeMap;

import SQL.StudentSQL;
import models.Student;

/**
 * Registerklasse, die alle Studentenobjekte h�lt und Methoden bereitstellt, um z.B. ein DefaultTableModel zu erstellen
 * @author A. Zensen
 *
 */
public class StudentRegister {

	private TreeMap<Integer, Student> studenten = new TreeMap<Integer, Student>();

	public StudentRegister() {
		this.studenten = StudentSQL.getTreeMap();
	}

	public TreeMap<Integer, Student> getStudentenMap() {
		return studenten;
	}

	/** Angedachte Methode f�r ein Speicherkonzept, das ablaufen soll,
	 * wenn die Datenbank wieder verf�gbar ist, sich aber Objekte im Register ver�ndert haben - nicht weiter verfolgt
	 * @param studenten
	 */
//	public void setStudenten(TreeMap<Integer, Student> studenten) {
//		boolean doneSQL = true;
//		for (int key : studenten.keySet()) {
//			doneSQL &= StudentSQL.updateStudent(key, studenten.get(key));
//			// only if every changed sql went good, doneSQL stays true.
//		}
//		if (doneSQL)
//			this.studenten = studenten;
//	}

	public Student getStudent(int matnr) {
		return studenten.get(matnr);
	}

	public void addStudent(Student student) {
		studenten.put(student.getMatnr(), student);
	}

	public void setStudent(Student student) {
		studenten.put(student.getMatnr(), student);
	}

	public void deleteStudent(Student student) {
		studenten.remove(student.getMatnr());
	}

	public void refresh() {
		this.studenten = StudentSQL.getTreeMap();
	}

	public DefaultTableModel getTableModel() {

		String[] ColumnNames = { "MatrikelNr", "Nachname", "Vorname", "EMail", "Studiengang", "Benutzer" };

		DefaultTableModel dtm = new DefaultTableModel((Object[]) ColumnNames, 0) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		for (Student stud : studenten.values()) {
			dtm.addRow(stud.getRow());
		}

		return dtm;
	}

}