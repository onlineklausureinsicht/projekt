package register;

import java.util.HashSet;

import SQL.TerminKlausurSQL;

import defaults.ZweiTypen;

import models.Termin;

/**
 * 
 * @author Maik Nowack
 *
 */
public class TerminKlausurRegister {

	private HashSet<Integer> matNr;

	/**
	 * Konstruktor - initialisiert die Variablen.
	 */
	public TerminKlausurRegister() {
		matNr = null;
	}

	/**
	 * Diese Methode wird aufgerufen, wenn sich ein Termin geaendert hat.
	 * 
	 * @param termin_neu
	 *            :Termin
	 * @param termin_alt
	 *            :Termin
	 * @return boolean: true wenn der Termin geaendert werden konnte, false wenn
	 *         der Termin nicht geaendert werden konnte.
	 */
	public boolean changeTerminKlausur(Termin termin_neu, Termin termin_alt) {
		ZweiTypen<Boolean, HashSet<Integer>> doneSQL = TerminKlausurSQL.update(
				termin_neu, termin_alt);
		if (doneSQL.getFirst()) {
			matNr = doneSQL.getSecond();
			return true;
		} else
			return false;
	}

	/**
	 * Diese Methode wird aufgerufen, wenn ein Termin abgesagt wurde.
	 * 
	 * @param termin
	 *            :Termin
	 * @return boolean: true wenn der Termin geloescht werden konnte, false wenn
	 *         der Termin nicht geloescht werden konnte.
	 */
	public boolean deleteTerminKlausur(Termin termin) {
		ZweiTypen<Boolean, HashSet<Integer>> doneSQL = TerminKlausurSQL
				.delete(termin);
		if (doneSQL.getFirst()) {
			matNr = doneSQL.getSecond();
			return true;
		} else
			return false;
	}

	/**
	 * Wird benoetigt um die Studenten benachrichtigen zu koennen, bei denen der
	 * Termin geaendert oder abgesagt wurde.
	 * 
	 * @return HashSet<Integer> mit den Matrikelnummern der Studenten, bei denen
	 *         sich der Termin geaendert hat.
	 */
	public HashSet<Integer> getMatNr() {

		return matNr;

	}

}