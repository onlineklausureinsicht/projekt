package defaults;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

/**
 * Klasse zum versenden einer E-Mail.
 * 
 * @author Maik Nowack
 * @version 1.0
 */
public class SendMail {

	private String to;
	private String cc;
	private String from;
	private String subject;
	private String message;

	/*
	 * MimeMessage benoetigt Session und Session benoetigt Properties.
	 */
	private Properties props;
	private Session session;
	private MimeMessage msg;

	/**
	 * Leerer Konstruktor, Variablen muessen ueber die Getter-Methoden gefuellt
	 * werden.
	 */
	public SendMail() {

	}

	/**
	 * Konstruktor, mit Initialisierung der Variablen.
	 * 
	 * @param to
	 *            :String - E-Mail-Adresse des Empfaengers
	 * @param cc
	 *            :String - optionale E-Mail-Adresse eines weiteren Empfaengers,
	 *            null wenn kein weiterer Empfaenger eingetragen werden soll.
	 * @param from
	 *            :String - E-Mail-Adresse des Senders
	 * @param subject
	 *            :String - Betreffzeile
	 * @param message
	 *            :String - Textkoerper
	 */
	public SendMail(String to, String cc, String from, String subject,
			String message) {
		this.to = to;
		this.cc = cc;
		this.from = from;
		this.subject = subject;
		this.message = message;
	}

	/**
	 * Methode zum versenden einer E-Mail, dazu muss der Benutzername und das
	 * Password an diese Methode uebergeben werden.
	 * 
	 * @param name
	 *            :String - Benutzername, der zum anmelden am E-Mail-Server
	 *            verwendet wird
	 * @param pwd
	 *            :String - Passwort, das zum anmelden am E-Mail-Server
	 *            verwendet wird
	 */
	public void send(String name, String pwd) {

		/*
		 * Wenn der Sender oder der Empfaenger nicht gesetzt sind, wird die
		 * Funktion nicht ausgefuehrt.
		 */

		if (!(to == null || from == null)) {

			try {

				session = mailSession(name, pwd);

				msg = new MimeMessage(session);

				msg.setFrom(new InternetAddress(from));

				/*
				 * Message.RecipientType.TO Message.RecipientType.CC
				 * Message.RecipientType.BCC
				 */
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
						this.to));
				if (cc != null)
					msg.addRecipient(Message.RecipientType.CC,
							new InternetAddress(this.cc));

				msg.setSubject(subject);
				msg.setText(message);

				Transport.send(msg);

			} catch (MessagingException msgE) {
				JOptionPane.showMessageDialog(null, msgE.getMessage(),
						"Fehlermeldung", JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	/**
	 * Methode zum versenden einer Datei.
	 * 
	 * @param name
	 *            :String - Benutzername, der zum anmelden am E-Mail-Server
	 *            verwendet wird
	 * @param pwd
	 *            :String - Passwort, das zum anmelden am E-Mail-Server
	 *            verwendet wird
	 * @param dat
	 *            :String - Pfad+Dateiname+Suffix
	 */
	public void sendAttachment(String name, String pwd, String dat) {

		/*
		 * Wenn der Sender oder der Empfaenger nicht gesetzt sind, wird die
		 * Funktion nicht ausgefuehrt.
		 */
		if (!(to == null || from == null)) {

			try {

				session = mailSession(name, pwd);

				msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(from));
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
						this.to));
				if (cc != null)
					msg.addRecipient(Message.RecipientType.CC,
							new InternetAddress(this.cc));
				msg.setSubject(subject);

				/*
				 * EMails mit Anhang bestehen aus einem sogennanten Multipart.
				 * Zuerst wird der Textkoerper erstellt und dann wird der Anhang
				 * hinzugefuegt.
				 * 
				 * Fuer den Textkoerper BodyPart msgBodyPart = new
				 * MimeBodyPart();
				 * 
				 * Den Multipart erstellen Multipart multipart = new
				 * MimeMultipart();
				 * 
				 * Den TextKoerper fuellen und dem Multipart hinzufuegen
				 * msgBodyPart.setText("Dies ist der Textkoerper");
				 * multipart.addBodyPart(msgBodyPart);
				 * 
				 * Nun den Anhang hinzufuegen, die Variable msgBodyPart wird
				 * wiederverwendet msgBodyPart = new MimeBodyPart(); DataSource
				 * source = new FileDataSource(dat);
				 * msgBodyPart.setDataHandler(new DataHandler(source));
				 * msgBodyPart.setFileName(dat);
				 * multipart.addBodyPart(msgBodyPart);
				 * 
				 * Nun werden die einzelnen Teile der EMail hinzugefuegt
				 * msg.setContent(multipart);
				 * 
				 * Und ab damit Transport.send(msg);
				 */

				MimeBodyPart msgBodyPart = new MimeBodyPart();

				MimeMultipart multipart = new MimeMultipart();

				msgBodyPart.setText(message);
				multipart.addBodyPart(msgBodyPart);

				msgBodyPart = new MimeBodyPart();
				DataSource source = new FileDataSource(dat);
				msgBodyPart.setDataHandler(new DataHandler(source));
				msgBodyPart.setFileName(dat);
				multipart.addBodyPart(msgBodyPart);

				msg.setContent(multipart);

				Transport.send(msg);

			} catch (MessagingException msgE) {
				JOptionPane.showMessageDialog(null, msgE.getMessage(),
						"Fehlermeldung", JOptionPane.ERROR_MESSAGE);
				// System.err.println("Fehler: " + msgE.getMessage());
			}
		}

	}

	/**
	 * Methode zum einrichten der E-Mail Schnittstelle zum E-Mail-Server.
	 * 
	 * @param user
	 *            :String - Benutzername, der zum anmelden am E-Mail-Server
	 *            verwendet wird
	 * @param pwd
	 *            :String - Passwort, das zum anmelden am E-Mail-Server
	 *            verwendet wird
	 * @return :Session - Objekt mit den Einstellungen des E-Mail-Servers
	 */
	private Session mailSession(String user, String pwd) {

		props = new Properties();

		// Zum Empfangen

		// Die Anmeldedaten hinterlegen
		props.setProperty("mail.pop3.user", user);
		props.setProperty("mail.pop3.password", pwd);

		// Zum Senden

		// Den Postausgangsserver setzen
		props.setProperty("mail.smtp.host", "smtp.fh-bielefeld.de");

		// Verlangt der Postausgangsserver eine Authorisierung mit Benutzername
		// und Password
		props.setProperty("mail.smtp.auth", "true");

		// Den Port fuer den Postausgangsserver setzen (465 = SSL)
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.setProperty("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");

		return Session.getInstance(props, new javax.mail.Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(props
						.getProperty("mail.pop3.user"), props
						.getProperty("mail.pop3.password"));
			}
		});
	}

	/*
	 * Es folgen die Getter und Setter Methoden fuer die Variablen
	 */

	/**
	 * @return :String; gibt den eingestellten Empfaenger zurueck. 
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to :String - E-Mail-Adresse des Empfaengers 
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return :String; gibt den eingestellten optionalen zweiten Empfaenger zurueck.
	 */
	public String getCc() {
		return cc;
	}

	/**
	 * @param cc :String - optionale E-Mail-Adresse eines weiteren Empfaengers,
	 *            null wenn kein weiterer Empfaenger eingetragen werden soll.
	 */
	public void setCc(String cc) {
		this.cc = cc;
	}

	/**
	 * @return :String; gibt den eingestellten Absender zurueck.
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from :String - E-Mail-Adresse des Senders
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return :String; gibt die eingestellte Betreffzeile zurueck.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject :String - Betreffzeile
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return :String; gibt den eingestellten Textkoerper zurueck.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param msg :String - Textkoerper
	 */
	public void setMessage(String msg) {
		this.message = msg;
	}

}