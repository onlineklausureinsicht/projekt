package defaults;
import java.util.Random;

public class PWGenerator {
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	static Random rnd = new Random();
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		System.out.println(PWGenerator.next(4));
//	}
	
	public static String next( int len ) {
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();
	}
}
