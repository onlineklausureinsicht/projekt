/**
 * @author Marius Hagen & Alexander Klippert
 * Startet den DesktopController und damit auch alles folgende
 * im Nimbus-Style, wenn moeglich.
 */

package defaults;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import controller.DesktopController;

public class DesktopTester {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		/*
		 * Die DesktopTester.java ist dafuer verantwortlich, dass sich
		 * der Desktop durch den DesktopController oeffnet.
		 */

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DesktopController controller = new DesktopController();	
					//View will be created in controller, so don't make a new one!
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
