package defaults;

/**
 * Diese Klasse wurde konstruiert, um sicher unterschiedliche Datentypen bei
 * einem Funktionsaufruf zurueckgeben zu koennen. Die Datentypen muessen ueber
 * ihre Wrapperklassen eingebunden werden, z.B. 'Integer' an Stelle von 'int'.
 * 
 * @author Maik Nowack
 * @version 1.0
 * @param F - erster Datentyp z.B. Boolean
 * @param S - zweiter Datentyp z.B. HashSet<Integer>
 */

public class ZweiTypen<F, S> {

	private F first;
	private S second;

	/**
	 * Konstruktor - initialisiert die Variablen
	 * 
	 * @param first :F - entspricht dem Datentyp der beim erstellen gewaehlt wurde
	 * @param second :S - entspricht dem Datentyp der beim erstellen gewaehlt wurde
	 */
	public ZweiTypen(F first, S second) {

		this.first = first;
		this.second = second;

	}

	/**
	 * 
	 * 
	 * @return F 
	 */
	public F getFirst() {
		return first;
	}

	/**
	 * @param first :F - entspricht dem Datentyp der beim erstellen gewaehlt wurde
	 */
	public void setFirst(F first) {
		this.first = first;
	}
	/**
	 * 
	 * @return S
	 */
	public S getSecond() {
		return second;
	}
	
	/**
	 * @param second :S - entspricht dem Datentyp der beim erstellen gewaehlt wurde
	 */
	public void setSecond(S second) {
		this.second = second;
	}

}