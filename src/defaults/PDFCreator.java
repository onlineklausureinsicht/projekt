/**
 * @author Marius Hagen
 * Erstellt mit Hilfe von IText die PDF.
 * Die PDF ist gegliedert, besitzt eine Tabelle und differenzierte Schrift.
 */

package defaults;

import java.io.FileOutputStream;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFCreator {

	public static void createKlausurenListe(String path, String ueberschrift, String[] lines, String stat){
		Document document = new Document();
		try {	
			PdfWriter.getInstance(document, new FileOutputStream("temp.pdf"));
			document.open(); 
			
			
			Font titleFont = new Font(Font.FontFamily.UNDEFINED, 20, Font.BOLD);
			Font chapterFont = new Font(Font.FontFamily.UNDEFINED, 16, Font.BOLD);
			Font subChapterFont = new Font(Font.FontFamily.UNDEFINED, 12, Font.BOLD);
			Font contentFont = new Font(Font.FontFamily.UNDEFINED, 12, Font.NORMAL);
			
			Anchor anchor = new Anchor(ueberschrift, titleFont);
			Chapter chapter;
			Paragraph object;
			Section containerDeep1, containerDeep2;
			
			//-
			chapter = new Chapter(new Paragraph(anchor), 1);
			chapter.add(new Paragraph(" ")); //<br\>
			chapter.add(new Paragraph(" ")); //<br\>
			chapter.setNumberDepth(0); //hacking the first number away
			
			//1.
			object = new Paragraph("Klausuren:", chapterFont);
			object.add(new Paragraph(" ")); //<br\>
			object.add(new Paragraph(" ")); //<br\>
			containerDeep1 = chapter.addSection(object);
			containerDeep1.setIndentation(20);
			
			//1.1
			//tabelle -> setting font to contentFont
			PdfPTable table = new PdfPTable(new float[]{ 1, 19 });
	        PdfPCell cell = new PdfPCell(new Phrase("X", contentFont));
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        table.addCell(cell);
	        cell = new PdfPCell(new Phrase("Klausur", contentFont));
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        table.addCell(cell);
	        
	        for(String line : lines) {
	        	table.addCell("");
	        	table.addCell(line);
	        }
			containerDeep1.add(table);
			
			//2.
			object = new Paragraph("Statistik:", chapterFont);
			object.add(new Paragraph(" ")); //<br\>
			object.add(new Paragraph(" ")); //<br\>
			containerDeep1 = chapter.addSection(object);
			containerDeep1.setIndentation(20);

			//2.1
			object = new Paragraph("Bestellte:", subChapterFont);
			containerDeep2 = containerDeep1.addSection(object);
			containerDeep2.setIndentation(20);
			
			//2.1.x
			object = new Paragraph(stat, contentFont);
			containerDeep2.add(object);
			/*object = new Paragraph("50/100:", contentFont);
			containerDeep2.add(object);
			
			//2.2
			object = new Paragraph("other:", subChapterFont);
			containerDeep2 = containerDeep1.addSection(object);
			containerDeep2.setIndentation(20);
			
			//2.1.x
			object = new Paragraph("bsp:", contentFont);
			containerDeep2.add(object);*/
			
			
			document.add(chapter);
			
			document.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
