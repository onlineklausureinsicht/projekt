package SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

import defaults.PWGenerator;
import models.Student;

/**
 * @author A. Zensen
 *
 */
public class StudentSQL {
	/**
	 * 
	 * Methode zum Einf�gen von Studenten. Benutzt PreparedStatement und holt
	 * sich Daten aus �bergebenem Studentenobjekt.
	 * 
	 * @param Student
	 *            student
	 * @return true if insert successful
	 */
	public static boolean insert(Student student) {
		int matnr = student.getMatnr();
		String name = student.getName();
		String vorname = student.getVorname();
		String benutzer = student.getBenutzer();
		String email = student.getEmail();
		String studiengang = student.getStudiengang();

		System.out.println("F�ge Student ein: " + matnr + " " + name + " "
				+ vorname + " " + benutzer + " " + email + " " + studiengang);

		Connection con = DBConnection.getCon();

		if (con != null) {
			if (createBenutzer(student, con)) {

				PreparedStatement pstmt = null;
				String sqlInsertStudent = "INSERT INTO Student (matnr, name, vorname, benutzer, email, studiengang) VALUES (?,?,?,?,?,?)";

				try {

					pstmt = con.prepareStatement(sqlInsertStudent);
					pstmt.setInt(1, matnr);
					pstmt.setString(2, name);
					pstmt.setString(3, vorname);
					pstmt.setString(4, benutzer);
					pstmt.setString(5, email);
					pstmt.setString(6, studiengang);

					pstmt.executeUpdate();
					return true;

				} catch (SQLException e) {
					System.err
							.println("Fehler beim Anlegen eines neuen Studenten: "
									+ e.getMessage());
					return false;
				} finally {
					try {
						if (pstmt != null) {
							pstmt.close();
						}
						if (con != null) {
							con.close();
						}
					} catch (SQLException e) {
						System.out
								.println("Fehler in StudentSQL, Methode insert: Ressource konnte nicht geschlossen werden: "
										+ e.getMessage());
					}

				}
			} else { return false;	}
		} System.out.println("Keine DB Verbindung"); return false;
	}

	/**
	 * Methode zum Erstellen eines Benutzereintrags in Relation Studentlogin.
	 * Wird zum Anlegen / Login eines Studenten ben�tigt (Attribut "benutzer").
	 * 
	 * @param Student
	 *            student
	 * @param Connection
	 *            con
	 * @return true, wenn Studentlogineintrag angelegt werden konnte.
	 */
	private static boolean createBenutzer(Student student, Connection con) {
		PreparedStatement pstmt = null;
		String sqlCreateBenutzer = "INSERT INTO STUDENTLOGIN VALUES (?, ?)";

		try {

			pstmt = con.prepareStatement(sqlCreateBenutzer);
			pstmt.setString(1, student.getBenutzer());
			// Passwort sollte an dieser Stelle verschl�sselt erzeugt werden und
			// per e-Mail an den Studenten gesendet werden.
			// Da kein Mailserver verf�gbar ist, wird dies nicht weiter
			// verfolgt, da ansonsten Logindaten des FH Mailservers �ffentlich
			// gemacht werden m�ssten.
			String pw = PWGenerator.next(4);
			pstmt.setString(2, pw);
			if (pstmt.executeUpdate() > 0) {
				System.out.println("Student wurde erfolgreich gespeichert. (Benutzerkennung: " + student.getBenutzer() + " Passwort: " + pw + ")");
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			System.out
					.println("Fehler in Methode createBenutzer in StudentSQL: "
							+ e.getMessage());
			return false;
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}

			} catch (SQLException e) {
				System.out
						.println("Fehler beim Schlie�en von PreparedStatement in Methode createBenutzer der Klasse StudentSQL: "
								+ e.getMessage());
			}
		}
	}

	/**
	 * Methode zum L�schen eines Studenten. Arbeitet mit einer Transaktion �ber
	 * mehrere Methoden.
	 * 
	 * @param matnr
	 * @return true, falls das L�schen des Studenten mit der �bergebenen matnr
	 *         erfolgreich war.
	 */
	public static boolean deleteStudent(Student student) {
		Connection con = null;
		Statement stmt = null;
		String deleteBenutzer = "DELETE FROM STUDENTLOGIN WHERE benutzer = '"
				+ student.getBenutzer() + "'";
		String deleteStudent = "DELETE FROM student WHERE matnr = "
				+ student.getMatnr();

		try {
			con = DBConnection.getCon();
			con.setAutoCommit(false);
			stmt = con.createStatement();
			stmt.addBatch(deleteBenutzer);
			stmt.addBatch(deleteStudent);
			int[] i = stmt.executeBatch();
			System.out.println("Gel�schte Eintr�ge gesamt: " + i.length);
			if (i.length != 2) {
				con.rollback();
				return false;
			}
			return true;
		} catch (SQLException e) {
			System.out
					.println("Fehler in Methode deleteStudent der Klasse StudentSQL: "
							+ e.getMessage());
			return false;
		} finally {
			try {
				if (!con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				System.out
						.println("Fehler in StudentSQL, Methode deleteStudent: Connection konnte nicht geschlossen werden.");
			}

		}

	}

	/**
	 * 
	 * Methode zum �ndern von Studentendaten (matnr ausgeschlossen)
	 * 
	 * @param student
	 * @return true, falls Update erfolgreich
	 */
	public static boolean update(Student student) {
		Connection con = DBConnection.getCon();
		PreparedStatement pstmt = null;
		try {
			pstmt = con
					.prepareStatement("UPDATE student SET name = ?, vorname = ?, email = ?, studiengang = ?, benutzer = ? WHERE matnr = ?");
			pstmt.setString(1, student.getName());
			pstmt.setString(2, student.getVorname());
			pstmt.setString(3, student.getEmail());
			pstmt.setString(4, student.getStudiengang());
			pstmt.setString(5, student.getBenutzer());
			pstmt.setInt(6, student.getMatnr());
			pstmt.execute();
			if (pstmt.getUpdateCount() != 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (!con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				System.out
						.println("Fehler in StudentSQL, Methode updateStudent: Ressource(n) konnte(n) nicht geschlossen werden: "
								+ e.getMessage());
			}

		}
		return false;
	}

	/**
	 * Erzeugt Studentenobjekte aus der Datenbankrelation Student und speichert
	 * diese in einer Treemap.
	 * 
	 * @return TreeMap<Integer, Student> (matnr, studentenObjekt) mit
	 *         Studentenobjekten
	 */
	public static TreeMap<Integer, Student> getTreeMap() {
		Connection con = DBConnection.getCon();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT * FROM student");
			TreeMap<Integer, Student> tm = new TreeMap<Integer, Student>();
			while (rs.next()) {
				Student stud = new Student(rs.getInt("matnr"),
						rs.getString("name"), rs.getString("vorname"),
						rs.getString("email"), rs.getString("studiengang"),
						rs.getString("benutzer"));
				tm.put(stud.getMatnr(), stud);

			}
			return tm;

		} catch (SQLException e) {
			System.out
					.println("Fehler in Methode getTreeMap der Klasse StudentSQL: "
							+ e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				System.out
						.println("Fehler beim Schlie�en von Ressourcen in StudentSQL Methode getTreeMap: "
								+ e.getMessage());
			}
		}
		return null;
	}

	/**
	 * 
	 * Pr�ft, ob der �bergebene Benutzer existiert
	 * 
	 * @param benutzer
	 * @return true, falls benutzer in Studentlogin existriert
	 * 
	 * 
	 */
	public static boolean benutzerExists(String benutzer) {
		Connection con = DBConnection.getCon();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sqlBenutzer = "Select benutzer from studentlogin where benutzer = ?";

		try {
			pstmt = con.prepareStatement(sqlBenutzer);
			pstmt.setString(1, benutzer);
			rs = pstmt.executeQuery();

			return rs.next();
		} catch (SQLException e) {
			System.out
					.println("Fehler in Klasse StudentSQL, Methode benutzerExists: "
							+ e.getMessage());
			return true;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				System.out
						.println("Fehler beim Schlie�en von Ressourcen in StudentSQL: "
								+ e.getMessage());
			}
		}
	}

}