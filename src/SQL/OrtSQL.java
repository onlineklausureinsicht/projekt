package SQL;

/*
 ******************************************************** 
 *Datei erstellt von | am
 *	Maik Nowack		 | 28.11.2014
 ********************************************************
 *Ã„nderung von		 | am		  | was
 *	Alexander Klippert		 | 15.12.2014 | *
 ********************************************************
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

import models.Ort;

/**
 * Diese Klasse beinhaltet die SQL Anweisungen fuer die Ort Relation. Dazu
 * gehoeren das einfuegen, aendern, loeschen und auslesen der Eintraege. Die
 * ausgelesenen Eintraege werden in einer TreeMap<Integer, Ort> bereit gestellt.
 * 
 * @author Maik Nowack
 * @version 1.0
 *
 */
public class OrtSQL {

	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	private static PreparedStatement pstmt = null;

	/**
	 * Methode fuer das einfuegen eines neuen Ortes in die Datenbank.
	 * 
	 * @param ortid
	 *            :int - unique key ueber die der Ort in der Datenbank eindeutig
	 *            identifiziert werden kann
	 * @param ort
	 *            :Ort
	 * @return :boolean; true wenn der Ort der Datenbank hinzugefuegt werden
	 *         konnte, false wenn dabei ein Fehler aufgetreten ist und der Ort
	 *         nicht der Datenbank hinzugefuegt werden konnte.
	 */
	public static boolean insert(int ortid, Ort ort) {

		con = DBConnection.getCon();
		String sql = new String("INSERT INTO ORT VALUES(?,?,?,?,?)");
		Ort o = ort;

		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, o.getStandort());
			pstmt.setString(2, o.getRaum());
			pstmt.setString(3, o.getAdresse());
			pstmt.setString(4, o.getOrt());
			pstmt.setInt(5, o.getOrtid());

			pstmt.execute();
			if (stmt.getUpdateCount() != 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return false;

	}

	/**
	 * Methode zum aendern eines Ortes in der Datenbank.
	 * 
	 * @param ortid
	 *            :int - unique key ueber die der Ort in der Datenbank eindeutig
	 *            identifiziert werden kann
	 * @param ort
	 *            :Ort
	 * @return :boolean; true wenn der Ort der Datenbank geaendert werden
	 *         konnte, false wenn dabei ein Fehler aufgetreten ist und der Ort
	 *         nicht der Datenbank geaendert werden konnte.
	 */
	public static boolean update(int ortid, Ort ort) {
		con = DBConnection.getCon();
		String sql = new String(
				"UPDATE ORT SET standort = ?, raum = ?, adresse = ?, ort = ?, ortid = ? where ortid = "
						+ ortid);
		Ort o = ort;

		try {
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, o.getStandort());
			pstmt.setString(2, o.getRaum());
			pstmt.setString(3, o.getAdresse());
			pstmt.setString(4, o.getOrt());
			pstmt.setInt(5, o.getOrtid());

			pstmt.execute();
			if (stmt.getUpdateCount() != 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return false;
	}

	/**
	 * Methode zum loeschen eines Ortes aus der Datenbank, an Hand des unique keys.
	 * 
	 * @param ortid
	 *            :int - unique key ueber die der Ort in der Datenbank eindeutig
	 *            identifiziert werden kann
	 * @return :boolean; true wenn der Ort aus der Datenbank geloescht werden
	 *         konnte, false wenn dabei ein Fehler aufgetreten ist und der Ort
	 *         nicht aus der Datenbank geloescht werden konnte.
	 */
	public static boolean delete(int ortid) {
		con = DBConnection.getCon();
		String sql = "DELETE FROM ORT WHERE ortid = " + ortid;
		try {
			stmt = con.createStatement();
			if (stmt.executeUpdate(sql) != 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Wird als Grundlage fuer das DefaultTableModel verwendet.
	 * 
	 * @return :TreeMap<Integer, Ort>; gibt eine TreeMap mit den in der
	 *         Datenbank eingetragenen Orten zurueck.
	 */
	public static TreeMap<Integer, Ort> getTreeMap() {

		TreeMap<Integer, Ort> orte = new TreeMap<Integer, Ort>();

		String sql = new String(
				"select standort, raum, adresse, ort, ortid from ORT");

		try {
			con = DBConnection.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String standort = rs.getString(1);
				String raum = rs.getString(2);
				String adresse = rs.getString(3);
				String ort = rs.getString(4);
				int ortid = rs.getInt(5);

				Ort o = new Ort(standort, raum, adresse, ort, ortid);
				orte.put(ortid, o);
			}
			return orte;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
