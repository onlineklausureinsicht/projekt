/**
 * @author Marius Hagen
 * Klasse zum bearbeiten der Datenbank in Relation Klausur
 */

package SQL;

import java.sql.*;
import java.util.TreeMap;

import models.Klausur;

public class KlausurSQL {

	/**
	 * Methode zum einschreiben von Klausur-Objekten in die Datenbank
	 * @param id
	 * @param klausur
	 * @return boolean: true wenn eine Ã¤nderung an der Klausur-EntitÃ¤t vorgenommen wurde. false wenn nicht.
	 */
	public static boolean insert(Integer id, Klausur klausur) {
		Connection con = DBConnection.getCon();
		PreparedStatement stmt;
		try
		{
			stmt = con.prepareStatement("INSERT INTO klausur VALUES(?,?,?,?,?,?,?)");
			
			stmt.setInt(1, klausur.getMatrikelNummer());
			stmt.setString(2, klausur.getModul());
			stmt.setInt(3, klausur.getDozentId());
			
			if(klausur.getTerminDatum() != null)				//in den Klausur-Objekten kann fÃ¼r die nachfolgenden 
				if(klausur.getTerminDatum().compareTo("") != 0) //Attribute entweder ein leerer String, oder null
					stmt.setString(4, klausur.getTerminDatum());//als Objekt vorliegen, hier behandel ich beides so,
				else stmt.setNull(4,  java.sql.Types.DATE );	//dass es in der Datenbank als NULL gespeichert wird.
			else stmt.setNull(4,  java.sql.Types.DATE );
			
			if(klausur.getTerminStandort() != null)
				if(klausur.getTerminStandort().compareTo("") != 0)
					stmt.setString(5, klausur.getTerminStandort());
				else stmt.setNull(5,  java.sql.Types.VARCHAR );
			else stmt.setNull(5,  java.sql.Types.VARCHAR );
			
			if(klausur.getTerminRaum() != null)
				if(klausur.getTerminRaum().compareTo("") != 0)
					stmt.setString(6, klausur.getTerminRaum());
				else stmt.setNull(6,  java.sql.Types.VARCHAR );
			else stmt.setNull(6,  java.sql.Types.VARCHAR );
			
			stmt.setInt(7, id); //this must be a unique
			stmt.execute();
			if(stmt.getUpdateCount() != 0) return true;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			if(e.getErrorCode() != 1 )e.printStackTrace(); //Errorcode 1 = ORA-00001: Unique Constraint (...) verletzt, java.sql.SQLIntegrityConstraintViolationException
			//dont want to see programm printing errors if just a same klausur exists.
		}
		return false;
	}
	

	/**
	 * Methode zum festschreiben von verÃ¤nderungen in Klausur-Objekten
	 * @param id
	 * @param klausur
	 * @return boolean: true wenn eine Ã¤nderung an der Klausur-EntitÃ¤t vorgenommen wurde. false wenn nicht.
	 */
	public static boolean update(Integer id, Klausur klausur) {
		Connection con = DBConnection.getCon();
		PreparedStatement stmt;
		try
		{
			stmt = con.prepareStatement("UPDATE klausur SET matnr = ?, modul = ?, dozid = ?, termindatum = ?, " +
						    "terminstandort = ?, terminraum = ?, klausurid = ? WHERE klausurid = ?");
			stmt.setInt(1, klausur.getMatrikelNummer());
			stmt.setString(2, klausur.getModul());
			stmt.setInt(3, klausur.getDozentId());
			stmt.setString(4, klausur.getTerminDatum());
			stmt.setString(5, klausur.getTerminStandort());
			stmt.setString(6, klausur.getTerminRaum());
            stmt.setInt(7, id); //this must be a unique
			stmt.setInt(8, id); //this must be a unique
			int updates = stmt.executeUpdate();
			if(updates != 0) return true;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return false;
	}

	/**
	 * Methode zum entfernen eines Klausur-Objektes aus der Datenbank
	 * @param id
	 * @return boolean: true wenn eine Ã¤nderung an der Klausur-EntitÃ¤t vorgenommen wurde. false wenn nicht.
	 */
	public static boolean delete(Integer id) {
		Connection con = DBConnection.getCon();
		PreparedStatement stmt;
		try
		{
			stmt = con.prepareStatement("DELETE FROM klausur WHERE klausurid = ?");
			stmt.setInt(1, id); //this must be a unique
			stmt.execute();
			if(stmt.getUpdateCount() != 0) return true;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	
	/**
	 * Stellt eine Auflistung aller Klausuren der Datenbank, sortierbar nach KlausurID bereit.
	 * @return TreeMap<Integer, Klausur>
	 */
	public static TreeMap<Integer, Klausur> getTreeMap() { //called as verwalter
		Connection con = DBConnection.getCon();
		Statement stmt;
		try
		{
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM klausur");
			TreeMap<Integer, Klausur> tm = new TreeMap<Integer, Klausur>();
			while(rs.next())												//hier werden alle Werte des ResultSets
			{																//in Klausur-Objekte umgewandelt
				Klausur k = new Klausur(rs.getInt("matnr"),
										rs.getString("modul"),
										rs.getInt("dozid"),
										rs.getString("termindatum"),
										rs.getString("terminstandort"),
										rs.getString("terminraum"),
                                        rs.getInt("klausurid"));
				
				tm.put(k.getKlausurId(), k);
			}
			return tm;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Stellt eine Auflistung der Klausuren zu einem bestimmten Studenten der Datenbank, sortierbar nach KlausurID bereit.
	 * @param matrikelnummer
	 * @return TreeMap<Integer, Klausur>
	 */
	public static TreeMap<Integer, Klausur> getTreeMap(int matrikelnummer) { //called as student
		Connection con = DBConnection.getCon();
		Statement stmt;
		try
		{
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM klausur WHERE matnr = " + matrikelnummer);
			TreeMap<Integer, Klausur> tm = new TreeMap<Integer, Klausur>();
			while(rs.next())												//hier werden alle Werte des ResultSets
			{																//in Klausur-Objekte umgewandelt
				Klausur k = new Klausur(rs.getInt("matnr"),
										rs.getString("modul"),
										rs.getInt("dozid"),
										rs.getString("termindatum"),
										rs.getString("terminstandort"),
										rs.getString("terminraum"),
                                        rs.getInt("klausurid"));
				
				tm.put(k.getKlausurId(), k);
			}
			return tm;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}