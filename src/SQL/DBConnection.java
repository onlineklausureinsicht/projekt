package SQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



/**
 * Klasse zur Erstellung von einem Connection Objekt
 * @author A. Zensen
 *
 */
public class DBConnection {

	private final static String url = "jdbc:oracle:thin:@aix1.fh-bielefeld.de:1521:d2";
	private final static String user = "dvi579";
	private final static String pass = "fh3744";
	

	/**
	 * Dynamische User/Pass-Wahl
	 * @param string
	 * @param string2
	 */
	public static Connection getCon(String user, String pass) {
		try {
			Connection con = DriverManager.getConnection( url, user, pass);
			return con;
			
			
		} catch (SQLException e) {
			System.err.println("Fehler in Methode getCon der Klasse DBConnection: " + e.getMessage());
			
			e.printStackTrace();
			return null;
			
		}
	}

	/**
	 * @return Connection Objekt mit festem user/pass zum dvi579
	 */
	public static Connection getCon() {
		try {
			Connection con = DriverManager.getConnection( url, user, pass);
			return con;
			
			
		} catch (SQLException e) {
			System.err.println("Fehler in Methode getCon der Klasse DBConnection: " + e.getMessage());
			
			e.printStackTrace();
			return null;
			
		}
	}

}