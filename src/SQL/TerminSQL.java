package SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.TreeMap;

import models.Termin;

/**
 * @author azensen
 *
 */
public class TerminSQL {

	/**
	 * 
	 * @param id
	 * @param termin
	 */
	public static boolean insert(Integer id, Termin termin) {
		Connection con = DBConnection.getCon();
		String sqlInsert = "INSERT INTO TERMIN VALUES(?,?,?,?,?,?,?,?)";
		Termin t = termin;
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlInsert);
			pstmt.setString(1, t.getDatum());
			pstmt.setString(2, t.getStandort());
			pstmt.setString(3, t.getRaum());
			pstmt.setString(4, t.getZeit1());
			pstmt.setString(5, t.getZeit2());
			
			if(t.isGesperrt())
				pstmt.setInt(6, 1);
			else pstmt.setNull(6, java.sql.Types.INTEGER);
			
			pstmt.setInt(7, t.getTerminid());
			pstmt.setString(8, t.getStudiengang());
			pstmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/**
	 * 
	 * @param terminid
	 * @param termin
	 */
	public static boolean update(int terminid, Termin termin) {
		Connection con = DBConnection.getCon();
		String sqlUpdate = "UPDATE TERMIN SET datum = ?, standort = ?, raum = ?, zeit1 = ?, zeit2 = ?, gesperrt = ?, terminid = ?, studiengang = ? where terminid = " + terminid;
		Termin t = termin;
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sqlUpdate);
			pstmt.setString(1, t.getDatum());
			pstmt.setString(2, t.getStandort());
			pstmt.setString(3, t.getRaum());
			pstmt.setString(4, t.getZeit1());
			pstmt.setString(5, t.getZeit2());
			
			if(t.isGesperrt())
				pstmt.setInt(6, 1);
			else pstmt.setNull(6, java.sql.Types.INTEGER);
			
			pstmt.setInt(7, t.getTerminid());
			pstmt.setString(8, t.getStudiengang());
			
			pstmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	/**
	 * 
	 * @param terminid
	 */
	public static boolean delete(int terminid) {

		Connection con = DBConnection.getCon();
		String sqlDelete = "DELETE FROM TERMIN WHERE terminid = " + terminid;
		try {
			Statement stmt = con.createStatement();
			int i = stmt.executeUpdate(sqlDelete);
			if(i > 0) { return true; }
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
		
		//		DELETE FROM table_name
//		WHERE some_column=some_value;
	}

	public static TreeMap<Integer, Termin> getTreeMap() {
		TreeMap<Integer, Termin> terminmap = new TreeMap<Integer, Termin>();
		
		Connection con = DBConnection.getCon();
		Statement stmt = null;
		ResultSet rs = null;
		String sqlQuery = "SELECT * FROM TERMIN";
		
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery);
			
			while(rs.next()) {
				String datum = rs.getString(1);
				String standort = rs.getString(2);
				String raum = rs.getString(3);
				String zeit1 = rs.getString(4);
				String zeit2 = rs.getString(5);
				
				boolean gesperrt = false;
				rs.getString(6);
				if(!rs.wasNull()) gesperrt = true;
				
				int terminid = rs.getInt(7);
				
				String studiengang = "---";
				rs.getString(8);
				if(!rs.wasNull()) studiengang = rs.getString(8);
				
				Termin termin = new Termin(terminid, datum, standort, raum, zeit1, zeit2, gesperrt, studiengang);
				terminmap.put(terminid, termin);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		return terminmap;
	}

	/**
	 * 
	 * @param studiengang
	 */
	public static TreeMap<Integer, Termin> getTreeMap(String _studiengang) {
		TreeMap<Integer, Termin> terminmap = new TreeMap<Integer, Termin>();
		
		Connection con = DBConnection.getCon();
		Statement stmt = null;
		ResultSet rs = null;
		String sqlQuery = "SELECT * FROM TERMIN WHERE STUDIENGANG = '" + _studiengang + "'";
		
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery);
			
			while(rs.next()) {
				String datum = rs.getString(1);
				String standort = rs.getString(2);
				String raum = rs.getString(3);
				String zeit1 = rs.getString(4);
				String zeit2 = rs.getString(5);
				
				boolean gesperrt = false;
				rs.getString(6);
				if(!rs.wasNull()) gesperrt = true;
				
				int terminid = rs.getInt(7);
				
				String studiengang = "---";
				rs.getString(8);
				if(!rs.wasNull()) studiengang = rs.getString(8);
				
				Termin termin = new Termin(terminid, datum, standort, raum, zeit1, zeit2, gesperrt, studiengang);
				terminmap.put(terminid, termin);
			}
			
			if(rs != null) { rs.close(); }
			if(stmt != null) { stmt.close(); }
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		return terminmap;
	}

}
