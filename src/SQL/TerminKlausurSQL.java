package SQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import defaults.ZweiTypen;
import models.Klausur;
import models.Termin;

/**
 * @author Maik Nowack
 * @version 1.0
 *
 */
public class TerminKlausurSQL {

	/**
	 * Methode zum aendern des Termins. Prueft zunaechst ob sich die Parameter
	 * Datum, Standort oder Raum geaendert haben, wenn ja wird geprueft ob der
	 * Termin in der Klausur Relation eingetragen wurde. Ist dies der Fall
	 * muessen an dieser Stelle die Werte auf NULL gesetzt werden, da diese
	 * Fremdschluessel sind. Dann wird der Termin geaendert, und die
	 * aktualisierten Werte in die Klausur Relation eingetragen.
	 * 
	 * @param termin_neu
	 *            :Termin - enthaelt die geaenderten Termindaten
	 * @param termin_alt
	 *            :Termin - enthaelt die urspruenglichen Termindaten
	 * @return ZweiTypen<Boolean, HashSet<Integer>> - True wenn die
	 *         Datenbankeintraege erfolgreich aktualisiert werden konnten, False
	 *         falls ein Fehler dabei aufgetreten ist. HashSet enthaelt die
	 *         Matrikelnummern der Studenten, bei denen sich durch die
	 *         Terminaenderung Aenderungen ergeben.
	 */
	@SuppressWarnings("resource")
	public static ZweiTypen<Boolean, HashSet<Integer>> update(
			Termin termin_neu, Termin termin_alt) {

		boolean done = false, change = false;
		HashSet<Integer> matNr = new HashSet<Integer>();
		HashSet<Klausur> klausur = new HashSet<Klausur>();
		String sql = new String(
				"select MATNR, MODUL, DOZID, TERMINDATUM, TERMINSTANDORT, TERMINRAUM, KLAUSURID from KLAUSUR where TERMINDATUM = '"
						+ termin_alt.getDatum()
						+ "' and TERMINSTANDORT = '"
						+ termin_alt.getStandort()
						+ "' and TERMINRAUM = '"
						+ termin_alt.getRaum() + "'");

		String klausurSQLup = new String(
				"UPDATE klausur SET matnr = ?, modul = ?, dozid = ?, termindatum = ?, terminstandort = ?, terminraum = ?, klausurid = ? WHERE klausurid = ?");
		String terminSQLup = new String(
				"UPDATE TERMIN SET datum = ?, standort = ?, raum = ?, zeit1 = ?, zeit2 = ?, gesperrt = ?, terminid = ?, studiengang = ? where terminid = "
						+ termin_alt.getTerminid());

		Connection con = DBConnection.getCon();
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pstmtKLAUSUR = null;
		PreparedStatement pstmtTERMIN = null;

		try {
			if (!(termin_neu.getDatum().equals(termin_alt.getDatum())
					&& termin_neu.getStandort()
							.equals(termin_alt.getStandort()) && termin_neu
					.getRaum().equals(termin_alt.getRaum()))) {
				change = true;
				if (con.getAutoCommit())
					con.setAutoCommit(false);

				stmt = con.createStatement();
				rs = stmt.executeQuery(sql);
				pstmtKLAUSUR = con.prepareStatement(klausurSQLup);

				while (rs.next()) {

					pstmtKLAUSUR.setInt(1, rs.getInt("MATNR"));
					pstmtKLAUSUR.setString(2, rs.getString("MODUL"));
					pstmtKLAUSUR.setInt(3, rs.getInt("DOZID"));
					pstmtKLAUSUR.setNull(4, java.sql.Types.DATE);
					pstmtKLAUSUR.setNull(5, java.sql.Types.VARCHAR);
					pstmtKLAUSUR.setNull(6, java.sql.Types.VARCHAR);
					pstmtKLAUSUR.setInt(7, rs.getInt("KLAUSURID"));
					pstmtKLAUSUR.setInt(8, rs.getInt("KLAUSURID"));
					pstmtKLAUSUR.addBatch();
					matNr.add(rs.getInt("MATNR"));

					Klausur k = new Klausur(rs.getInt("MATNR"),
							rs.getString("MODUL"), rs.getInt("DOZID"),
							rs.getString("termindatum"),
							rs.getString("terminstandort"),
							rs.getString("terminraum"), rs.getInt("klausurid"));
					klausur.add(k);

				}

				pstmtKLAUSUR.executeBatch();
				con.commit();
			}

			pstmtTERMIN = con.prepareStatement(terminSQLup);

			pstmtTERMIN.setString(1, termin_neu.getDatum());
			pstmtTERMIN.setString(2, termin_neu.getStandort());
			pstmtTERMIN.setString(3, termin_neu.getRaum());
			pstmtTERMIN.setString(4, termin_neu.getZeit1());
			pstmtTERMIN.setString(5, termin_neu.getZeit2());

			if (termin_neu.isGesperrt())
				pstmtTERMIN.setInt(6, 1);
			else
				pstmtTERMIN.setNull(6, java.sql.Types.INTEGER);

			pstmtTERMIN.setInt(7, termin_alt.getTerminid());
			pstmtTERMIN.setString(8, termin_neu.getStudiengang());

			pstmtTERMIN.execute();
			if (change)
				con.commit();

			if (pstmtTERMIN.getUpdateCount() != 0)
				done = true;

			if (change && done) {
				pstmtKLAUSUR = con.prepareStatement(klausurSQLup);

				for (Klausur k : klausur) {

					pstmtKLAUSUR.setInt(1, k.getMatrikelNummer());
					pstmtKLAUSUR.setString(2, k.getModul());
					pstmtKLAUSUR.setInt(3, k.getDozentId());
					pstmtKLAUSUR.setString(4, termin_neu.getDatum());
					pstmtKLAUSUR.setString(5, termin_neu.getStandort());
					pstmtKLAUSUR.setString(6, termin_neu.getRaum());
					pstmtKLAUSUR.setInt(7, k.getKlausurId());
					pstmtKLAUSUR.setInt(8, k.getKlausurId());
					pstmtKLAUSUR.addBatch();

				}

				pstmtKLAUSUR.executeBatch();
				con.commit();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			done = false;
			return new ZweiTypen<Boolean, HashSet<Integer>>(done, matNr);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (pstmtKLAUSUR != null)
					pstmtKLAUSUR.close();
				if (pstmtTERMIN != null)
					pstmtTERMIN.close();
			} catch (SQLException e) {

			}
		}

		return new ZweiTypen<Boolean, HashSet<Integer>>(done, matNr);
	}

	/**
	 * Methode zum aendern des Termins. Prueft zunaechst ob sich die Parameter
	 * Datum, Standort oder Raum geaendert haben, wenn ja wird geprueft ob der
	 * Termin in der Klausur Relation eingetragen wurde. Ist dies der Fall
	 * muessen an dieser Stelle die Werte auf NULL gesetzt werden, da diese
	 * Fremdschluessel sind. Dann erst wird der Termin geloescht.
	 * 
	 * @param termin
	 *            :Termin - der geloescht werden soll
	 * @return ZweiTypen<Boolean, HashSet<Integer>> - True wenn die
	 *         Datenbankeintraege erfolgreich aktualisiert werden konnten, False
	 *         falls ein Fehler dabei aufgetreten ist. HashSet enthaelt die
	 *         Matrikelnummern der Studenten, bei denen sich durch das Loeschen
	 *         des Termins Aenderungen ergeben.
	 */
	public static ZweiTypen<Boolean, HashSet<Integer>> delete(Termin termin) {

		boolean done = false;
		HashSet<Integer> matNr = new HashSet<Integer>();
		String sql = new String(
				"select MATNR, MODUL, DOZID, TERMINDATUM, TERMINSTANDORT, TERMINRAUM, KLAUSURID from KLAUSUR where TERMINDATUM = '"
						+ termin.getDatum()
						+ "' and TERMINSTANDORT = '"
						+ termin.getStandort()
						+ "' and TERMINRAUM = '"
						+ termin.getRaum() + "'");

		String klausurSQLup = new String(
				"UPDATE klausur SET matnr = ?, modul = ?, dozid = ?, termindatum = ?, terminstandort = ?, terminraum = ?, klausurid = ? WHERE klausurid = ?");
		String terminSQLdel = new String("DELETE FROM TERMIN WHERE terminid = "
				+ termin.getTerminid());

		Connection con = DBConnection.getCon();
		Statement stmt = null;
		ResultSet rs = null;
		PreparedStatement pstmtKLAUSUR = null;
		Statement stmtTERMIN = null;

		try {
			if (con.getAutoCommit())
				con.setAutoCommit(false);

			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			pstmtKLAUSUR = con.prepareStatement(klausurSQLup);

			while (rs.next()) {
				pstmtKLAUSUR.setInt(1, rs.getInt("MATNR"));
				pstmtKLAUSUR.setString(2, rs.getString("MODUL"));
				pstmtKLAUSUR.setInt(3, rs.getInt("DOZID"));
				pstmtKLAUSUR.setNull(4, java.sql.Types.DATE);
				pstmtKLAUSUR.setNull(5, java.sql.Types.VARCHAR);
				pstmtKLAUSUR.setNull(6, java.sql.Types.VARCHAR);
				pstmtKLAUSUR.setInt(7, rs.getInt("KLAUSURID"));
				pstmtKLAUSUR.setInt(8, rs.getInt("KLAUSURID"));
				pstmtKLAUSUR.addBatch();
				matNr.add(rs.getInt("MATNR"));
			}

			int[] b = pstmtKLAUSUR.executeBatch();
			if (b.length > 0)
				con.commit();

			stmtTERMIN = con.createStatement();
			int i = stmtTERMIN.executeUpdate(terminSQLdel);
			if (i > 0) {
				done = true;
			}

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			done = false;
			return new ZweiTypen<Boolean, HashSet<Integer>>(done, matNr);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (pstmtKLAUSUR != null)
					pstmtKLAUSUR.close();
				if (stmtTERMIN != null)
					stmtTERMIN.close();
			} catch (SQLException e) {

			}
		}

		return new ZweiTypen<Boolean, HashSet<Integer>>(done, matNr);
	}

}