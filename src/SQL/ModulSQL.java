/**
 * @author Marius Hagen
 * Klasse zum bearbeiten der Datenbank in Relation Modul
 */

package SQL;

import java.sql.*;
import java.util.TreeMap;

import models.Modul;

public class ModulSQL {
    
	/**
	 * Fuegt ein Modul in die Relation ein.
	 * Da das Modul zur Zeit nichts weiter als seine Bezeichnung (schluessel) als Attribute besitzt,
	 * muss nichts aus dem Modul gespeichert werden, weil der Schlüssel extra angegeben wird.
	 * @param bezeichnung
	 * @param modul
	 * @return boolean
	 */
    public static boolean insert(String bezeichnung, Modul modul) {
        Connection con = DBConnection.getCon();
        PreparedStatement stmt;
        try
        {
        	//this one is outdated since we decided to work without modul as key
            /*stmt = con.prepareStatement("INSERT INTO klausur VALUES(?,?)");
            stmt.setString(1, modul.getModul()); //this must be a unique
            stmt.setString(2, modul.getBezeichnung());
            */
            
            stmt = con.prepareStatement("INSERT INTO modul VALUES(?)");
            stmt.setString(1, bezeichnung);
            
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
    /**
     * Bearbeitet das Modul, welches dem Schluessel(bezeichnung) entspricht.
     * @param klausur
     * @return boolean
     */
    public static boolean update(String bezeichnung, Modul modul) {
        Connection con = DBConnection.getCon();
        PreparedStatement stmt;
        try
        {
            /*stmt = con.prepareStatement("UPDATE modul SET modnr = ?, bezeichnung = ? WHERE bezeichnung = ?");
            stmt.setString(1, modul.getModul());
            stmt.setString(2, bezeichnung);
            stmt.setString(3, bezeichnung);//this must be a unique*/
            
            stmt = con.prepareStatement("UPDATE modul SET bez = ? WHERE bez = ?");
            stmt.setString(1, bezeichnung);
            stmt.setString(2, bezeichnung);//this must be a unique
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
    /**
     * Loescht ein Modul aus der Relation
     * @param klausur
     * @return boolean
     */
    public static boolean delete(String bezeichnung) {
        Connection con = DBConnection.getCon();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("DELETE modul WHERE bez = ?");
            stmt.setString(1,bezeichnung); //this must be a unique
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
     
    /**
     * Listet die DAten aus der Datenbank als TreeMap und gibt diese zurueck
     * @return TreeMap<String, Modul>
     */
    public static TreeMap<String, Modul> getTreeMap() { //called as verwalter
        Connection con = DBConnection.getCon();
        Statement stmt;
        try
        {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM modul");
            TreeMap<String, Modul> tm = new TreeMap<String, Modul>();
            String key;
            while(rs.next())
            {
                key = rs.getString("bez");
                Modul m = new Modul(/*rs.getInt("modnr"),*/
                                    key);
                 
                tm.put(key, m);
                
            }
            return tm;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}