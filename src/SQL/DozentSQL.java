/**
 * @author Marius Hagen
 * Klasse zum bearbeiten der Datenbank in Relation Dozent
 */

package SQL;

import java.sql.*;
import java.util.TreeMap;

import models.Dozent;

public class DozentSQL {

	/**
	 * Fuegt einen Dozenten in die Relation ein
	 * @param dozent_id
	 * @param dozent
	 * @return boolean
	 */
	public static boolean insert(int dozent_id, Dozent dozent) {
        Connection con = DBConnection.getCon();
        PreparedStatement stmt;
        try
        {
            stmt = con.prepareStatement("INSERT INTO dozent VALUES(?, ?, ?, ?)");
            stmt.setInt(1, dozent_id);
            stmt.setString(2, dozent.getName());
            stmt.setString(3, dozent.getVorname());
            stmt.setString(4, dozent.getEmail());
            
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
    /**
     * Veraendert den Dozenten mit der ID
     * @param klausur
     * @return boolean
     */
    public static boolean update(int dozent_id, Dozent dozent) {
        
        PreparedStatement stmt;
        try
        {
        	Connection con = DBConnection.getCon();
        	stmt = con.prepareStatement("UPDATE dozent SET name = ?, vorname = ?, email = ? WHERE dozid = ?");
            
            stmt.setString(1, dozent.getName());
            stmt.setString(2, dozent.getVorname());
            stmt.setString(3, dozent.getEmail());
            stmt.setInt(4, dozent_id);//this must be a unique
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
    /**
     * Loescht den Dozenten mit der ID
     * @param klausur
     * @return boolean
     */
    public static boolean delete(int dozent_id) {
        PreparedStatement stmt;
        try
        {
        	Connection con = DBConnection.getCon();
            stmt = con.prepareStatement("DELETE dozent WHERE dozid = ?");
            stmt.setInt(1,dozent_id); //this must be a unique
            stmt.execute();
            if(stmt.getUpdateCount() != 0) return true;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
 
     
    /**
     * Laed die Daten aus der Datenbank und packt diese als TreeMap
     * @return TreeMap<Integer, Dozent>
     */
    public static TreeMap<Integer, Dozent> getTreeMap() {
        Statement stmt;
        try
        {
        	Connection con = DBConnection.getCon();
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM dozent");
            TreeMap<Integer, Dozent> tm = new TreeMap<Integer, Dozent>();
            int key;
            while(rs.next())
            {
                key = rs.getInt("dozid");
                Dozent d = new Dozent(key,
                                    rs.getString("name"),
                                    rs.getString("vorname"),
                                    rs.getString("email"));
                 
                tm.put(key, d);
                
            }
            return tm;
             
        } catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}