/**
 * @author Marius Hagen
 * Klasse zum bearbeiten der Datenbank in Relation Studiengang
 */

package SQL;

import java.sql.*;
import java.util.TreeMap;

import models.Studiengang;

public class StudiengangSQL {

	/**
	 * Fügt einen Studiengang in die Relation ein.
	 * Zur Zeit besteht die Studiengang-Relation nur aus dem Primärschluessel,
	 * was die angabe des Studiengangs als zweiten Parameter redundant macht.
	 * Jedoch wird diese beibehalten, für den fall, dass sich die Relation ändert,
	 * um später nicht alle Klassen an diese änderung anpassen zu müssen.
	 * 
	 * @param bezeichnung
	 * @param studiengang
	 * @return boolean, true wenn erfolgreich
	 */
	public static boolean insert(String bezeichnung, Studiengang studiengang) {
		Connection con = DBConnection.getCon();
		PreparedStatement stmt;
		try
		{
			stmt = con.prepareStatement("INSERT INTO studiengang VALUES(?)");

			stmt.setString(1, studiengang.getBezeichnung()); //this must be a unique
			stmt.execute();
			if(stmt.getUpdateCount() != 0) return true;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Löscht einen Studiengang anhand seiner Bezeichnung(schluessel) als der Relation
	 * @param bezeichnung
	 * @return boolean, true wenn erfolgreich
	 */
	public static boolean delete(String bezeichnung) {
		Connection con = DBConnection.getCon();
		PreparedStatement stmt;
		try
		{
			stmt = con.prepareStatement("DELETE FROM studiengang WHERE bez = ?");
			stmt.setString(1, bezeichnung); //this must be a unique
			stmt.execute();
			if(stmt.getUpdateCount() != 0) return true;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Listet alle Studiengange in einen TreeMap auf und gibt diese zurueck
	 * @return TreeMap<String, Studiengang>
	 */
	public static TreeMap<String, Studiengang> getTreeMap() { //called as verwalter
		Connection con = DBConnection.getCon();
		Statement stmt;
		try
		{
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM studiengang");
			TreeMap<String, Studiengang> tm = new TreeMap<String, Studiengang>();
			while(rs.next())
			{
				Studiengang s = new Studiengang(rs.getString("bez"));
				
				tm.put(s.getBezeichnung(), s);
			}
			return tm;
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}