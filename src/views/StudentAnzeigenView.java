package views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import java.awt.BorderLayout;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import controller.StudentAnzeigenController;


/**
 * @author A. Zensen
 *
 */
public class StudentAnzeigenView extends JInternalFrame implements ActionListener {
	private JTable table;
	private JButton btnAktualisieren;
	private JButton btnNeuerStudent;
	private JButton btnStudentndern;
	private JButton btnStudentLschen;
	private JButton btnSchliessen;
	private DefaultTableModel studentDTM;
	
	private StudentAnzeigenController studentAnzeigenController;


	/**Konstruktor benutzt DefaultTableModel der Studenten, Controller
	 * @param studentAnzeigenController
	 * @param studentDTM
	 */
	public StudentAnzeigenView(StudentAnzeigenController studentAnzeigenController, DefaultTableModel studentDTM) {
		setupLayout();
		
		this.studentAnzeigenController = studentAnzeigenController;
		this.studentDTM = studentDTM;
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		setupButtons(panel);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		setupTableModel(scrollPane);
		
		pack();
		setVisible(true);

	}

	/** Erstellt das TableModel bzw. aggregiert/referenziert dies
	 * @param scrollPane
	 */
	private void setupTableModel(JScrollPane scrollPane) {
		table = new JTable(this.studentDTM);
		table.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table);
	}

	/**
	 * Layoutanpassungen
	 */
	private void setupLayout() {
		setClosable(true);
		setTitle("Studenten Verwalten");
		setResizable(true);
		setMaximizable(true);
		setIconifiable(true);
		setBounds(100, 100, 573, 302);
		getContentPane().setLayout(new BorderLayout(0, 0));
	}

	/** Buttons hinzufuegen
	 * @param panel
	 */
	private void setupButtons(JPanel panel) {
		btnAktualisieren = new JButton("Aktualisieren");
		panel.add(btnAktualisieren);
		btnAktualisieren.addActionListener(this);
		// Aus Zeitmangel nicht berÃ¼cksichtigt. HierfÃ¼r mÃ¼ssten wir ein
		// Publish/Subscribe-Konzept entwickeln.
		btnAktualisieren.setEnabled(false);
		
		btnNeuerStudent = new JButton("Neuer Student");
		panel.add(btnNeuerStudent);
		btnNeuerStudent.addActionListener(this);
		
		btnStudentndern = new JButton("Student \u00E4ndern");
		panel.add(btnStudentndern);
		btnStudentndern.addActionListener(this);
		
		btnStudentLschen = new JButton("Student l\u00F6schen");
		panel.add(btnStudentLschen);
		btnStudentLschen.addActionListener(this);
		
		btnSchliessen = new JButton("Schlie\u00DFen");
		panel.add(btnSchliessen);
		btnSchliessen.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btnSchliessen) { this.dispose(); }
		if(e.getSource() == this.btnNeuerStudent) { this.studentAnzeigenController.anlegenStudent(this.getParent()); }
		if(e.getSource() == this.btnStudentLschen) { 
			
			if(table.getSelectedRowCount() > 0) {
				int matnr = (int) table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0);
				int rowid = table.convertRowIndexToModel(table.getSelectedRow());
				
				int n = JOptionPane.showConfirmDialog(
			            null,
			            "Sind Sie sicher, dass Sie " + matnr + " löschen möchten? Es werden alle zugehörigen Klausuren entfernt.",
			            "Bestätigung",
			            JOptionPane.YES_NO_OPTION);
				if(n == 0) {
				System.out.println("Student mit Matrikelnummer " + matnr + " gewählt.");
				this.studentAnzeigenController.loeschenStudent(this.getParent(), matnr, rowid);
				} else { }
			} else {
				JOptionPane.showMessageDialog(null,
						"Wählen Sie zum Löschen zunächst einen Studenten aus.",
						"Auswahlfehler", JOptionPane.INFORMATION_MESSAGE);
			}
			
			
			 }
		if(e.getSource() == this.btnStudentndern) { 
			if(table.getSelectedRowCount() > 0) {
				int matnr = (int) table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0);
				int rowid = table.convertRowIndexToModel(table.getSelectedRow());
				
				this.studentAnzeigenController.aendernStudent(this.getParent(), matnr, rowid);
			}
		}
	}

}
