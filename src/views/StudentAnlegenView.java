package views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.StudentAnlegenController;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.text.NumberFormatter;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

public class StudentAnlegenView extends JInternalFrame implements
		ActionListener {

	private JFormattedTextField tfMatnr;
	private JLabel lblMaxMatnr;
	private JFormattedTextField tfName;
	private JLabel lblMaxName;
	private JFormattedTextField tfVorname;
	private JLabel lblMaxVorname;
	private JFormattedTextField tfEmail;
	private JLabel lblMaxEmail;
	private JComboBox<String> cbStudiengang;
	private JLabel lblStudiengangwahl;
	private JFormattedTextField tfBenutzername;
	private JLabel lblBenutzer;
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	private StudentAnlegenController studentAnlegenController;


	/**Konstruktor benutzt ComboBoxModel mit Studiengaengen, wird controller uebergeben
	 * @param studentAnlegenController
	 * @param studgangCBM
	 */
	public StudentAnlegenView(
			StudentAnlegenController studentAnlegenController,
			DefaultComboBoxModel<String> studgangCBM) {
		this.studentAnlegenController = studentAnlegenController;
		setupLayout();		

		//Setup ButtonPanel + Buttons
		JPanel btnPanel = new JPanel();
		getContentPane().add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setupButtons(btnPanel);
		
		//Setup Formpanel + Textfields
		JPanel maskPanel = setupMaskPanel();
		setupLabelsNTextFields(studgangCBM, maskPanel);
		
		pack();
		this.setVisible(true);

	}

	/**
	 * Layoutanpassung
	 */
	private void setupLayout() {
		setIconifiable(true);
		setClosable(true);
		setTitle("Neuen Studenten anlegen");
		getContentPane().setLayout(new BorderLayout(0, 0));
	}

	/** Erstellt alle Labels und TextFields
	 * @param studgangCBM
	 * @param maskPanel
	 */
	private void setupLabelsNTextFields(
			DefaultComboBoxModel<String> studgangCBM, JPanel maskPanel) {
		JLabel lblMatrikelnummer = new JLabel("Matrikelnummer:");
		maskPanel.add(lblMatrikelnummer, "2, 2, right, default");

		// tfMatnr gibt null zurÃ¼ck, falls falscher Wert
		NumberFormat format = NumberFormat.getIntegerInstance();

		tfMatnr = new JFormattedTextField();
		maskPanel.add(tfMatnr, "4, 2, fill, default");

		lblMaxMatnr = new JLabel("(max. 10 Ziffern)");
		lblMaxMatnr.setForeground(Color.GRAY);
		maskPanel.add(lblMaxMatnr, "4, 4");

		JLabel lblName = new JLabel("Name:");
		maskPanel.add(lblName, "2, 6, right, default");

		tfName = new JFormattedTextField();
		maskPanel.add(tfName, "4, 6, fill, default");

		lblMaxName = new JLabel("(max. 50 Zeichen)");
		lblMaxName.setForeground(Color.GRAY);
		maskPanel.add(lblMaxName, "4, 8");

		JLabel lblVorname = new JLabel("Vorname:");
		maskPanel.add(lblVorname, "2, 10, right, default");

		tfVorname = new JFormattedTextField();
		maskPanel.add(tfVorname, "4, 10, fill, default");

		lblMaxVorname = new JLabel("(max. 50 Zeichen)");
		lblMaxVorname.setForeground(Color.GRAY);
		maskPanel.add(lblMaxVorname, "4, 12, left, bottom");

		JLabel lblEmail = new JLabel("E-Mail:");
		maskPanel.add(lblEmail, "2, 14, right, default");

		tfEmail = new JFormattedTextField();
		maskPanel.add(tfEmail, "4, 14, fill, default");

		lblMaxEmail = new JLabel("(max. 50 Zeichen)");
		lblMaxEmail.setForeground(Color.GRAY);
		maskPanel.add(lblMaxEmail, "4, 16");

		JLabel lblStudiengang = new JLabel("Studiengang:");
		maskPanel.add(lblStudiengang, "2, 18, right, default");

		cbStudiengang = new JComboBox<String>(studgangCBM);
		maskPanel.add(cbStudiengang, "4, 18, fill, default");

		lblStudiengangwahl = new JLabel(
				"(Bitte w\u00E4hlen Sie einen Studiengang)");
		lblStudiengangwahl.setForeground(Color.GRAY);
		maskPanel.add(lblStudiengangwahl, "4, 20");

		JLabel lblBenutzername = new JLabel("Benutzername:");
		maskPanel.add(lblBenutzername, "2, 22, right, default");

		tfBenutzername = new JFormattedTextField();
		tfBenutzername.setEnabled(false);
		tfBenutzername.setEditable(false);
		maskPanel.add(tfBenutzername, "4, 22, fill, default");

		lblBenutzer = new JLabel("(wird automatisch ermittelt)");
		lblBenutzer.setForeground(Color.GRAY);
		maskPanel.add(lblBenutzer, "4, 24");
	}

	/** Erstellt ButtonPanel
	 * @param btnPanel
	 */
	private void setupButtons(JPanel btnPanel) {
		btnSpeichern = new JButton("Speichern");
		btnPanel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);

		btnAbbrechen = new JButton("Abbrechen");
		btnPanel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
	}

	/**
	 * @return JPanel - Maske
	 */
	private JPanel setupMaskPanel() {
		JPanel maskPanel = new JPanel();
		getContentPane().add(maskPanel, BorderLayout.CENTER);
		maskPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		return maskPanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnAbbrechen) { this.dispose();
		}

		if (e.getSource() == this.btnSpeichern) {
			this.setFieldsGrey();
			if (checkFields()) {
			
			String benutzerEntry = this.tfVorname.getText().substring(0, 1)
					+ this.tfName.getText();
			
			
				// zulÃ¤ssigen Benutzer finden
				String benutzerTry = benutzerEntry;
				int i = 1;
				while (studentAnlegenController.benutzerExists(benutzerTry)) {
					benutzerTry = benutzerTry + i;
					if (!studentAnlegenController.benutzerExists(benutzerTry)) {
						break;
					} else {
						i++;
						benutzerTry = benutzerEntry + i;
					}
				}
				
				if (!studentAnlegenController.benutzerExists(benutzerTry)) {

					int matnr = Integer.parseInt(this.tfMatnr.getText());
					String name = this.tfName.getText();
					String vorname = this.tfVorname.getText();
					String email = this.tfEmail.getText();
					String studiengang = (String) this.cbStudiengang
							.getSelectedItem();
					Object[] fieldData = { matnr, name, vorname, email,
							studiengang, benutzerTry };
					studentAnlegenController.speichern(fieldData);
					this.dispose();
				}
			}
			
		}
	}

	/**
	 * setzt die Hinweislabels wieder grau
	 */
	private void setFieldsGrey() {
		this.lblMaxMatnr.setForeground(Color.GRAY);
		this.lblMaxName.setForeground(Color.GRAY);
		this.lblMaxVorname.setForeground(Color.GRAY);
		this.lblMaxEmail.setForeground(Color.GRAY);
		this.lblStudiengangwahl.setForeground(Color.GRAY);
	}
	
	/**Prueft Feldeingaben
	 * @return true if field entries check out correctly
	 */
	private boolean checkFields() {
		boolean status = true;
		if(studentAnlegenController.matnrExists(Integer
						.parseInt(this.tfMatnr.getText().trim()))) {
			JOptionPane.showMessageDialog(null,
					"Die eingegebene Matrikelnummer ist bereits vergeben. Bitte wÃ¤hlen Sie eine andere.",
					"Fehler: Matrikelnummer bereits vergeben", JOptionPane.INFORMATION_MESSAGE);
			status = false;
		}
			
		if (this.tfMatnr.getText().isEmpty()) {
			status = false;
			this.lblMaxMatnr.setForeground(Color.RED);

			try {
				Integer.parseInt(tfMatnr.getText());
			} catch (NumberFormatException e) {
				status = false;
			}
		}
		if (this.tfName.getText().isEmpty()
				|| this.tfName.getText().length() > 50) {
			status = false;
			this.lblMaxName.setForeground(Color.RED);
		}
		if (this.tfVorname.getText().isEmpty()
				|| this.tfVorname.getText().length() > 50) {
			status = false;
			this.lblMaxVorname.setForeground(Color.RED);
		}
		if (this.tfEmail.getText().isEmpty()
				|| this.tfEmail.getText().length() > 50) {
			status = false;
			this.lblMaxEmail.setForeground(Color.RED);
		}
		if (this.cbStudiengang.getSelectedIndex() < 0) {
			status = false;
			this.lblStudiengangwahl.setForeground(Color.RED);
		}
		// if (this.tfBenutzername.getText().length() <= 0
		// || this.tfBenutzername.getText().length() > 50) {
		// status = false;
		// this.lblBenutzer.setForeground(Color.RED);
		// }

		return status;
	}

}
