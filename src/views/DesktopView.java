/**
 * @author Marius Hagen & Alexander Klippert
 * GUI des Desktops, der die Arbeit des Verwalters unterstützt.
 */

package views;

import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.Font;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Toolkit;

import controller.DesktopController;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


public class DesktopView extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private DesktopController controller;

	// GUI Panels
	private JPanel contentPane;
	private JDesktopPane desktopPane;

	// MenueItems
	private JMenuItem mntmDesktopBeenden;

	private JMenuItem mntmDozentenVerwalten;
	private JMenuItem mntmDozentenAnlegen;

	private JMenuItem mntmKlausurenAnlegen;
	private JMenuItem mntmKlausurenAnzeigen;
	private JMenuItem mntmKlausurenBestellteAnzeigen;
	private JMenuItem mntmKlausurenDrucken;

	// 09.12.14 Edited by Maik Start
	// Item Bezeichnungen angepasst
	private JMenuItem mntmTermineVerwalten;
	private JMenuItem mntmTermineAnlegen;
	// 09.12.14 Edited by Maik End

	private JMenuItem mntmOrteAnlegen;
	private JMenuItem mntmOrteVerwalten;

	private JMenuItem mntmStudentenVerwalten;
	private JMenuItem mntmStudentenAnlegen;

	private JMenuItem mntmModuleVerwalten;
	private JMenuItem mntmModuleAnlegen;

	private JMenuItem mntmStudiengaengeVerwalten;
	private JMenuItem mntmStudiengaengeAnlegen;

	private JMenuItem mntmErsteHilfe;
	private JMenuItem mntmber;

	private BufferedImage image;

	private String[] args;

	/**
	 * Create the frame.
	 * @param controller
	 */
	public DesktopView(DesktopController controller) {
		setBackground(Color.WHITE);
		
		/*
		 * Hier wird das kleine Icon oben links im Fenster geladen.
		 */
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("images/icon.png"));
		this.controller = controller;

		setExtendedState(Frame.MAXIMIZED_BOTH);

		setTitle("Tool zur Klausurterminverwaltung");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		/*
		 * Die JMenuBar beinhaltet alle JMenus auf dem Desktop.
		 * Diese JMenus wie 'Datei' haben auch ein ein Tastatur-
		 * kuerzel bekommen. 'Datei' wird mit ALT + [a] angesprochen
		 * und das darin befindliche JMenuItem 'Beenden' kann dann mit
		 * ALT + [e] angesprochen werden.
		 */

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnDatei = new JMenu("Datei");
		mnDatei.setMnemonic('a');
		menuBar.add(mnDatei);

		mntmDesktopBeenden = new JMenuItem("Beenden");
		mntmDesktopBeenden.setMnemonic('e');
		mntmDesktopBeenden.addActionListener(this);
		mnDatei.add(mntmDesktopBeenden);

		JMenu mnTermine = new JMenu("Termine");
		mnTermine.setMnemonic('t');
		menuBar.add(mnTermine);

		mntmTermineVerwalten = new JMenuItem("Verwalten");
		mntmTermineVerwalten.setFont(new Font("SansSerif", Font.BOLD, 12));
		mntmTermineVerwalten.addActionListener(this);
		mnTermine.add(mntmTermineVerwalten);

		mntmTermineAnlegen = new JMenuItem("Anlegen");
		mntmTermineAnlegen.addActionListener(this);
		mnTermine.add(mntmTermineAnlegen);

		JMenu mnKlausur = new JMenu("Klausuren");
		mnKlausur.setMnemonic('K');
		menuBar.add(mnKlausur);

		mntmKlausurenAnzeigen = new JMenuItem("Anzeigen");
		mntmKlausurenAnzeigen.setFont(new Font("SansSerif", Font.BOLD, 12));
		mntmKlausurenAnzeigen.addActionListener(this);
		mnKlausur.add(mntmKlausurenAnzeigen);

		mntmKlausurenAnlegen = new JMenuItem("Anlegen");
		mntmKlausurenAnlegen.addActionListener(this);
		mnKlausur.add(mntmKlausurenAnlegen);

		JMenu mnKlausurenBestellungen = new JMenu("Bestellungen");
		mnKlausur.add(mnKlausurenBestellungen);

		mntmKlausurenBestellteAnzeigen = new JMenuItem("Anzeigen");
		mntmKlausurenBestellteAnzeigen.addActionListener(this);
		mnKlausurenBestellungen.add(mntmKlausurenBestellteAnzeigen);

		mntmKlausurenDrucken = new JMenuItem("Drucken");
		mntmKlausurenDrucken.addActionListener(this);
		mnKlausurenBestellungen.add(mntmKlausurenDrucken);

		JMenu mnStudenten = new JMenu("Studenten");
		mnStudenten.setMnemonic('s');
		menuBar.add(mnStudenten);

		mntmStudentenVerwalten = new JMenuItem("Verwalten");
		mntmStudentenVerwalten.setFont(new Font("Segoe UI", Font.BOLD, 12));
		mntmStudentenVerwalten.addActionListener(this);
		mnStudenten.add(mntmStudentenVerwalten);

		mntmStudentenAnlegen = new JMenuItem("Anlegen");
		mntmStudentenAnlegen.addActionListener(this);
		mnStudenten.add(mntmStudentenAnlegen);

		JMenu mnDozent = new JMenu("Dozenten");
		mnDozent.setMnemonic('D');
		menuBar.add(mnDozent);

		mntmDozentenVerwalten = new JMenuItem("Verwalten");
		mntmDozentenVerwalten.setFont(new Font("SansSerif", Font.BOLD, 12));
		mntmDozentenVerwalten.addActionListener(this);
		mnDozent.add(mntmDozentenVerwalten);

		mntmDozentenAnlegen = new JMenuItem("Anlegen");
		mntmDozentenAnlegen.addActionListener(this);
		mnDozent.add(mntmDozentenAnlegen);

		JMenu mnOrt = new JMenu("Orte");
		mnOrt.setMnemonic('O');
		menuBar.add(mnOrt);

		mntmOrteVerwalten = new JMenuItem("Verwalten");
		mntmOrteVerwalten.setFont(new Font("SansSerif", Font.BOLD, 12));
		mntmOrteVerwalten.addActionListener(this);
		mnOrt.add(mntmOrteVerwalten);

		mntmOrteAnlegen = new JMenuItem("Anlegen");
		mntmOrteAnlegen.addActionListener(this);
		mnOrt.add(mntmOrteAnlegen);

		JMenu mnModule = new JMenu("Module");
		mnModule.setEnabled(false);
		menuBar.add(mnModule);

		mntmModuleVerwalten = new JMenuItem("Verwalten");
		mntmModuleVerwalten.addActionListener(this);
		mnModule.add(mntmModuleVerwalten);

		mntmModuleAnlegen = new JMenuItem("Anlegen");
		mntmModuleAnlegen.addActionListener(this);
		mnModule.add(mntmModuleAnlegen);

		JMenu mnStudiengnge = new JMenu("Studieng\u00E4nge");
		mnStudiengnge.setEnabled(false);
		menuBar.add(mnStudiengnge);

		mntmStudiengaengeVerwalten = new JMenuItem("Verwalten");
		mntmStudiengaengeVerwalten.addActionListener(this);
		mnStudiengnge.add(mntmStudiengaengeVerwalten);

		mntmStudiengaengeAnlegen = new JMenuItem("Anlegen");
		mntmStudiengaengeAnlegen.addActionListener(this);
		mnStudiengnge.add(mntmStudiengaengeAnlegen);

		JMenu mnHilfe = new JMenu("Hilfe");
		menuBar.add(mnHilfe);

		mntmErsteHilfe = new JMenuItem("Erste Hilfe");
		mntmErsteHilfe.addActionListener(this);
		mnHilfe.add(mntmErsteHilfe);

		JSeparator separator = new JSeparator();
		separator.setMinimumSize(new Dimension(0, 5));
		mnHilfe.add(separator);

		mntmber = new JMenuItem("\u00DCber");
		mntmber.addActionListener(this);
		mnHilfe.add(mntmber);

		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		/*
		 * Hier wird das Hintergrundbild geladen.
		 * Das Hintergrundbild passt sich dynamisch an die Groesse
		 * des Frames mit .getWidth() und .getHeight() an.
		 */

		try {
			image = ImageIO.read(new File("images/BackgroundV1.PNG"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		desktopPane = new JDesktopPane() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics grphcs) {
				super.paintComponent(grphcs);
				grphcs.drawImage(image, 0, 0, desktopPane.getWidth(),
						desktopPane.getHeight(), null);
			}

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(image.getWidth(), image.getHeight());
			}
		};
		desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		desktopPane.setMinimumSize(new Dimension(0, 2));
		desktopPane.setBackground(Color.WHITE);
		contentPane.add(desktopPane, BorderLayout.CENTER);

		JLabel lblGroupF = new JLabel("Group F - Marius Hagen, Alexander Klippert, Maik Nowack, Andr\u00E9 Zensen");
		lblGroupF.setEnabled(false);
		lblGroupF.setLabelFor(desktopPane);
		lblGroupF.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblGroupF, BorderLayout.SOUTH);
		lblGroupF.setFont(new Font("Calibri", Font.PLAIN, 13));

	}

	/**
	 * Im folgenden Teil werden Alle Events zum Weiterleiten an andere Fenster/Panels/usw behandelt
	 * Da dies jedoch nur ein Prototyp ist, sind noch nicht alle Komponenten mit eingebunden
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// 10.12.14 Edited by Maik Start
		// Wenn die Register noch nicht geladen sind, liefert
		// controller.isReady() false.
		if (controller.isReady()) {
			// 10.12.14 Edited by Maik End
			if (e.getSource() == this.mntmDesktopBeenden) {
				LogInV2.main(args); // nach Beenden oeffnet sich die Login-Maske
//				System.exit(0); // Damit wuerde sich das ganze Programm beenden. Die Login-Maske taucht nicht mehr auf.
				dispose(); // Der Desktop schliesst sich
				
				/*
				 * Auf das System.exit(0); wurde verzichtet, damit
				 * der Benutzer wieder die Login-Maske geoeffnet bekommt,
				 * wie man es z. B. auch vom MS Windows bei einer Abmeldung
				 * kennt. So kann der Benutzer eine Sitzung sicher beenden
				 * und sich spaeter wieder am System anmelden, ohne das
				 * Programm neu starten zu muessen.
				 */
			}
			
			/*
			 * Bei einer Betaetigung eines JMenuItem wird die
			 * dazugehoerige Methode des Controllers aufgerufen.
			 */
			else if (e.getSource() == this.mntmDozentenVerwalten) {
				controller.dozentVerwalten();
			} else if (e.getSource() == this.mntmDozentenAnlegen) {
				controller.dozentAnlegen();
			} else if (e.getSource() == this.mntmKlausurenAnlegen) {
				controller.klausurAnlegen();
			} else if (e.getSource() == this.mntmKlausurenBestellteAnzeigen) {
				controller.klausurBestellteAnzeigen();
			} else if (e.getSource() == this.mntmKlausurenAnzeigen) {
				controller.klausurAnzeigen();
			} else if (e.getSource() == this.mntmKlausurenDrucken) {
				controller.klausurDrucken();
			} else if (e.getSource() == this.mntmTermineVerwalten) {
				controller.terminVerwalten();
			} else if (e.getSource() == this.mntmTermineAnlegen) {
				controller.terminAnlegen();
			} else if (e.getSource() == this.mntmOrteAnlegen) { 
				controller.ortAnlegen(); 					
			} else if (e.getSource() == this.mntmOrteVerwalten) {
				controller.ortVerwalten();
			} else if (e.getSource() == this.mntmStudentenVerwalten) {
				controller.studentVerwalten();
			}else if(e.getSource() == this.mntmStudentenAnlegen) {
				controller.studentAnlegen();
			}
			
			/*
			 * Die Menues fuer Module und Studiengaenge sind in dieser
			 * Version noch nicht implementiert und nur ueber
			 * den Oracle SQL-Developer zu erreichen.
			 */
			
			// else if(e.getSource() == this.mntmModuleVerwalten) {
			// controller.modulVerwalten();
			// }
			// else if(e.getSource() == this.mntmStudiengaengeVerwalten) {
			// controller.studiengangVerwalten();
			// }
			// else if(e.getSource() == this.mntmStudiengaengeAnlegen) {
			// controller.studiengangAnlegen();
			// }
			
			/*
			 * Die JMenuItems 'ErsteHilfe' und 'Ueber' oeffnen
			 * nur ein MessageDialog Fenster. Hierfuer wird kein
			 * Controller benoetigt.
			 */
			
			else if (e.getSource() == this.mntmErsteHilfe) {
				String help = new String("Fuer weitere Hilfe und Support\n"
						+ "wenden Sie sich bitte per Mail an: \n\n"
						+ "helpdesk@fh-bielefeld.de");

				JOptionPane.showMessageDialog(this, help, "Hilfe",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (e.getSource() == this.mntmber) {
				String credits = new String("Exam Administration \n"
						+ "Version 1.0 Basic \n06.01.2015 \n"
						+ "by Group F\n\n" + "CodeCrew\n\n" + "Marius Hagen \n"
						+ "Alexander Klippert \n" + "Maik Nowack \n"
						+ "Andre Zensen \n");

				JOptionPane.showMessageDialog(this, credits, "Credits",
						JOptionPane.INFORMATION_MESSAGE);
			}

		}
		
		// 10.12.14 Edited by Maik Start
		// Wenn die Register noch nicht geladen wurden,
		// folglich keine Datenbank Verbindung besteht,
		// wird verhindert das die JInternalFrames geladen werden und eine
		// Nullpointer Exception ausloesen. Stattdessen wird eine Meldung
		// angezeigt,
		// dass auf die Datenbank Verbindung gewartet wird.
		else {
			JOptionPane.showMessageDialog(this, "Warte auf Datenbank",
					"Information", JOptionPane.INFORMATION_MESSAGE);
		}
		// 10.12.14 Edited by Maik End

	}

	public void addToDesktop(JInternalFrame comp) {
		desktopPane.add(comp);
	}
}
