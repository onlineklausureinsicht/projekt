/**
 * @author Marius Hagen
 * Bestätigungs-GUI, dass das drucken einer Liste eingeleitet wird.
 */

package views;

import javax.swing.JInternalFrame;
import controller.KlausurDruckenController;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class KlausurDruckenView extends JInternalFrame implements ActionListener{

	private KlausurDruckenController controller;
	private JButton btnDrucken;
	private JButton btnShow;
	
	/**
	 * Create the Frame
	 * @param controller
	 */
	public KlausurDruckenView(KlausurDruckenController controller) {
		this.controller = controller;
		setTitle("Klausurenliste drucken");
		setClosable(true);
		setVisible(true);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_center = new JPanel();
		getContentPane().add(panel_center, BorderLayout.CENTER);
		panel_center.setLayout(null);
		
		JLabel lblWarning1 = new JLabel("Achtung!");
		lblWarning1.setBounds(33, 64, 373, 29);
		lblWarning1.setForeground(Color.RED);
		lblWarning1.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblWarning1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_center.add(lblWarning1);
		
		JLabel lblWarning2 = new JLabel("Mit Best\u00E4tigung wird die gesamte Klausurenliste gedruckt.");
		lblWarning2.setBounds(33, 108, 373, 19);
		lblWarning2.setForeground(Color.BLACK);
		lblWarning2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblWarning2.setHorizontalAlignment(SwingConstants.CENTER);
		panel_center.add(lblWarning2);
		
		btnDrucken = new JButton("Drucken");
		btnDrucken.setBounds(168, 171, 101, 29);
		btnDrucken.addActionListener(this);
		panel_center.add(btnDrucken);
		
		JLabel lblWarning3 = new JLabel("Sowie die Anmeldung zu diesem Termin gesperrt.");
		lblWarning3.setHorizontalAlignment(SwingConstants.CENTER);
		lblWarning3.setForeground(Color.BLACK);
		lblWarning3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblWarning3.setBounds(33, 126, 373, 19);
		panel_center.add(lblWarning3);
		
		
		JPanel panel_bottom = new JPanel();
		getContentPane().add(panel_bottom, BorderLayout.SOUTH);
		panel_bottom.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_left = new JPanel();
		panel_bottom.add(panel_left, BorderLayout.WEST);
		
		btnShow = new JButton("Nein, nur anzeigen");
		btnShow.addActionListener(this);
		panel_left.add(btnShow);
		
		setBounds(200,200,452,284);
	}

	/**
	 * Alternativ zum Drucken soll die PDF auch "nur" angezeigt werden können.
	 * Hier werden die Buttons mit den Controller-Methoden verknuepft.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == btnDrucken){
			controller.print();
		}
		
		else if(arg0.getSource() == btnShow){
			controller.open();
		}
	}
}
