package views;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import controller.OrtAnzeigenController;

/**
 * @author A. Klippert
 * 
 */
public class OrtAnzeigenView extends JInternalFrame implements ActionListener {
	private JTable table;
	private JButton btnNeuerOrt;
	private JButton btnOrtaendern;
	private JButton btnOrtLoeschen;
	private OrtAnzeigenController ortAnzeigenController;
	private DefaultTableModel ortDtm;
	private JButton btnSchliessen;

	/**
	 * Create the frame.
	 */

	public OrtAnzeigenView(OrtAnzeigenController controller,
			DefaultTableModel dtm) {
		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setResizable(true);
		setTitle("Ortverwaltung");
		setBounds(100, 100, 611, 347);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnNeuerOrt = new JButton("Neuer Ort");
		btnNeuerOrt.addActionListener(this);
		panel.add(btnNeuerOrt);

		btnOrtaendern = new JButton("Ort \u00E4ndern");
		btnOrtaendern.addActionListener(this);
		panel.add(btnOrtaendern);
		
		btnOrtLoeschen = new JButton("Ort l\u00F6schen");
		btnOrtLoeschen.addActionListener(this);
		panel.add(btnOrtLoeschen);

		btnSchliessen = new JButton("Schlie\u00DFen");
		btnSchliessen.addActionListener(this);
		panel.add(btnSchliessen);

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		// controller
		this.ortAnzeigenController = controller;

		// model
		this.ortDtm = dtm;
		table = new JTable(ortDtm);
		table.setAutoCreateRowSorter(true);
		table.setModel(dtm);
		
		scrollPane.setViewportView(table);
		setVisible(true);

	}
	
	/*
	 * Durch Betaetigung des Schliessen Buttons wird das Panel mit 
	 * dispose(); geschlossen. 
	 * Mit 'Loeschen' wird die lschen() Methode
	 * in der ortAnzeigenController.java ausgefuehrt. Diese loescht den
	 * Datensatz im OrtRegister mit der deleteOrt() Methode und die Zeile (Row)
	 * aus dem DefaultTableModel mit dtm.removeRow(), mit Hilfe der OrtID.
	 * Diese wird mit getValueAt(row, 4) gezogen. Das Register fuehrt
	 * unter der deleteOrt() Methode OrtSQL.delete(ortid) aus, um den
	 * Datensatz aus der Datenbank zu loeschen.
	 * 
	 * Sollte vorher Zeile markiert worden sein, so oeffnet sich ein MessageDialog
	 * Fenster mit dieser Information. 
	 * 
	 * Mit dem 'Ort aendern' Button oeffnet sich die View/ Panel, die zum Bearbeiten
	 * dieses Datensatzes ist. Siehe OrtBearbeiteView.java
	 * 
	 * Mit dem 'Neuer Ort' Button oeffnet sich die View/ Panel, die zum Anlegen
	 * eines neuen Ortes ist. Siehe OrtAnlegenView.java
	 */

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnSchliessen) {
			this.dispose();
		}
		if (e.getSource() == this.btnOrtLoeschen) {
			if (table.getSelectedRowCount() > 0) {
				int rowid = table.convertRowIndexToModel(table.getSelectedRow());

//				int ortid = (int) Integer.parseInt((String) table.getModel()
//						.getValueAt(rowid, 0));
//				System.out.println(ortid);
				ortAnzeigenController.lschen(rowid);
			} else {
				JOptionPane.showMessageDialog(null,
						"Waehlen Sie zum Loeschen zunaechst einen Ort aus.",
						"Auswahlfehler", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (e.getSource() == this.btnOrtaendern) {
			if (table.getSelectedRowCount() > 0) {
						int rowid = table
						.convertRowIndexToModel(table.getSelectedRow());
				ortAnzeigenController.ortAendern(this.getParent(),rowid);
				
				
			}
		}
		if (e.getSource() == this.btnNeuerOrt) {
			ortAnzeigenController.ortNeu(this.getParent());
						
		}

	}
}
