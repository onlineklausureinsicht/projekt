package views;

import controller.LogInC2;
import models.LogInM2;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;
import javax.swing.JFormattedTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;

/**
 * @author Alexander Klippert
 *
 */
public class LogInV2 extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private LogInC2 controller; //mvc
	
	private JPanel contentPane;
	private JPasswordField passwort;
	private JButton buttonAbbrechen;
	private JButton buttonAnmelden;
	private JFormattedTextField benutzer;
	private JLabel labelMeldung;

	private String[] args;
						
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		// LookandFeel Start
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		// LookandFeel Ende

		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				try {
					LogInM2 mdl = new LogInM2();
					LogInC2 ctrl = new LogInC2(mdl);
					LogInV2 frame = new LogInV2(ctrl);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public LogInV2(LogInC2 logincontroler) {
		setIconImage(Toolkit.getDefaultToolkit().getImage("images/icon.png"));
		
		controller = logincontroler; //mvc
		
		setTitle("Exam Administration Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600, 300, 592, 237);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		benutzer = new JFormattedTextField();
		benutzer.setText("");
		benutzer.setBounds(142, 65, 135, 29);
		contentPane.add(benutzer);

		passwort = new JPasswordField();
		passwort.setText("");
		passwort.setBounds(287, 65, 135, 29);
		contentPane.add(passwort);

		JLabel labelBenutzer = new JLabel("Benutzer:");
		labelBenutzer.setBounds(142, 48, 135, 14);
		contentPane.add(labelBenutzer);

		JLabel labelKennwort = new JLabel("Kennwort:");
		labelKennwort.setBounds(287, 48, 135, 14);
		contentPane.add(labelKennwort);

		/*
		 * Die Buttons sind reine .PNGs und saemtliche
		 * Raender und/ oder Umrandungen wurder deaktiviert.
		 */
		buttonAnmelden = new JButton("");
		buttonAnmelden.setContentAreaFilled(false);
		buttonAnmelden.setFocusPainted(false);
		buttonAnmelden.setBorder(null);
		buttonAnmelden.setBorderPainted(false);
		buttonAnmelden.setIcon(new ImageIcon("images/Anmelden.png"));
		buttonAnmelden.setBounds(285, 151, 203, 30);
		contentPane.add(buttonAnmelden);
		buttonAnmelden.addActionListener(this);

		buttonAbbrechen = new JButton("");
		buttonAbbrechen.setFocusPainted(false);
		buttonAbbrechen.setContentAreaFilled(false);
		buttonAbbrechen.setBorderPainted(false);
		buttonAbbrechen.setBorder(null);
		buttonAbbrechen.setIcon(new ImageIcon("images/Abbrechen.png"));
		buttonAbbrechen.setBounds(72, 152, 203, 29);
		contentPane.add(buttonAbbrechen);
		buttonAbbrechen.addActionListener(this);

		JSeparator separator = new JSeparator();
		separator.setBounds(142, 105, 280, 2);
		contentPane.add(separator);

		// Label Ueberschrift
		JLabel Ueberschrift = new JLabel("Klausureinsicht Anmeldung");
		Ueberschrift.setFont(new Font("Tahoma", Font.BOLD, 16));
		Ueberschrift.setBounds(144, 11, 280, 26);
		contentPane.add(Ueberschrift);
		
		labelMeldung = new JLabel("");
		labelMeldung.setForeground(Color.RED);
		labelMeldung.setBounds(142, 117, 280, 29);
		contentPane.add(labelMeldung);

		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonAnmelden) {

			char[] pw = this.passwort.getPassword();
			String pass = "";
			for (char c: pw ){
				pass += c; // wie passwort = passwort + c;
				}
			
			// Alternativ: pass = new StringBuilder().append(pw).toString();
			
			boolean status = controller.login(benutzer.getText(), pass);
			
			if(status)
			{
				// Angemeldet-Block
				
				/*
				 * Wenn 'status' von false auf true, in der LogInM2.java
				 * gesetzt wird, oeffnet sich der Desktop und das LogIn-Frame 
				 * wird mit dispose(); geschlossen.
				 */
				WindowListener[] listeners = getWindowListeners();
				for (WindowListener listener : listeners)
					listener.windowClosing(null);
				
				defaults.DesktopTester.main(args);
				dispose(); // schliesst das Login-Fenster
				
			}
			else{
				//hier bist du nicht angemeldet
				
				/*
				 * Der Benutzer bekommt eine Meldung in Form
				 * eines rot erscheinenden Labels, dass er 
				 * eine falsche Eingabe getaetigt hat.
				 */
				
				String meldung = "Benutzername oder Passwort falsch.";
				labelMeldung.setText(meldung);
				labelMeldung.setForeground(Color.RED);
				
				WindowListener[] listeners = getWindowListeners();
				for (WindowListener listener : listeners)
					listener.windowClosing(null);
				setVisible(false);
				try {
					Thread.sleep(500); 
					/*
					 * Hier wurde eine kuenstliche Pause aus Sicherheitsgruenden
					 * eingebaut: Um das Abfischen von Benutzerdaten zu verhindern!
					 * Sollte der Benutzername korrekt sein, wuerde es eine kurze Zeit laenger
					 * dauern, bis die Fehlermeldung kommt, als wenn der Benutzername schon
					 * falsch waere.
					 */
					setVisible(true);
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
			}
		}

		if (e.getSource() == buttonAbbrechen) {
			WindowListener[] listeners = getWindowListeners();
			for (WindowListener listener : listeners)
				listener.windowClosing(null);
			dispose();
		} 
	}
}
