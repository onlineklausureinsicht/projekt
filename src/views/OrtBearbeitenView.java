package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.OrtBearbeitenController;
/**
 * @author A. Klippert
 *
 */
public class OrtBearbeitenView extends JInternalFrame implements ActionListener {
	
	//buttons
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	//fields for input
	private JTextField tfOrtId;	
	private JFormattedTextField tfRaum;

	private JLabel lblMaxRaum;
	private JComboBox cbStandort;
	private JTextField tfAdresse;
	private JLabel lblMaxAdresse;
	private JLabel lblOrt;
	private JTextField tfOrt;
	private JLabel lblMaxOrt;
	private OrtBearbeitenController controller;

	
		/**
	 * Create the frame.
	 */
	public OrtBearbeitenView(OrtBearbeitenController controller) {
			setIconifiable(true);
			setClosable(true);
		this.controller = controller;
		
		Object [] bearbeitung = controller.laden();
		 
		// erstellt Panel mit der Bearbeiten-Oberfläche
		setTitle("Raum bearbeiten");
		setBounds(100, 100, 452, 382);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// Buttons zum Speichern der Eingaben oder zum Abbrechen
		
		btnSpeichern = new JButton("Speichern");
		panel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);
		
		btnAbbrechen = new JButton("Abbrechen");
		panel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
		
		/* hier wird der Rahmen im Panel erstellt,
		 * so werden die Textfields etc. nicht bis an den Rand gestreckt.
		 */
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblOrtid = new JLabel("Ort-ID:");
		panel_1.add(lblOrtid, "2, 2, right, default");
		
		/* 
		 * Mit .setEnabled(false) und .setEditable(false) ist das Textfield nicht
		 * antastbar. Eine Bearbeitung somit unmöglich. Die ID soll nicht aenderbar sein
		 * und wird auch nur durch das Programm vergeben. Eine Manuelle Aenderung 
		 * funktioniert nur direkt über den Oracle SQL-Developer.
		 */
		
		tfOrtId = new JTextField(String.valueOf((int) bearbeitung[4]));
		tfOrtId.setEnabled(false);
		tfOrtId.setEditable(false);
		panel_1.add(tfOrtId, "4, 2, fill, default");
		tfOrtId.setColumns(10);
		
		JLabel lblwirdAutomatischFestgelegtOrtid = new JLabel("(wird automatisch festgelegt)");
		lblwirdAutomatischFestgelegtOrtid.setForeground(Color.GRAY);
		panel_1.add(lblwirdAutomatischFestgelegtOrtid, "4, 4");
		
		JLabel lblStandort = new JLabel("Standort:");
		panel_1.add(lblStandort, "2, 6, right, default");
		
		/*
		 * Der Standort wird .setEnabled(false) gesetzt und somit ist dieser auch nicht
		 * aenderbar. Es handelt sich hier um einen Teil vom Primary-Key. Dieser kann nicht geaendert werden,
		 * da Oracle SQL eine 'ON UPDATE CASCADE' Anweisung nicht unterstuetzt.
		 */
		
		cbStandort = new JComboBox();
		cbStandort.setEnabled(false);
		cbStandort.setModel(new DefaultComboBoxModel(new String[] {"FB1", "FB2", "FB3", "FB4", "FB5"}));
		cbStandort.setSelectedItem(bearbeitung [0]);
		panel_1.add(cbStandort, "4, 6, fill, default");
		
		JLabel lblRaum = new JLabel("Raum:");
		panel_1.add(lblRaum, "2, 10, right, default");
		
		/*
		 * Das Textfield vom Raum wird .setEnabled(false) gesetzt und ist somit genau so wie die
		 * ID und der Standort nicht aenderbar. In diesem Fall ist es auch ein Teil vom
		 * Primary-Key und kann aufgrund der nicht vorhandenen Unterstuetzung der Anweisung 
		 * geaendert werden.
		 * Hierfuer muesste man das komplette Ort-Objekt loeschen und neu, mit den neuen Inhalten
		 * erzeugen. Jedoch muss darauf geachtet werden, dass schon ein erstellter Termin, der 
		 * dieses Ort-Objekt beinhaltet, geloescht wird!
		 */
		
		tfRaum = new JFormattedTextField();
		tfRaum.setEnabled(false);
		tfRaum.setEditable(false);
		tfRaum.setText((String) bearbeitung[1]);
		panel_1.add(tfRaum, "4, 10, fill, default");
		
		lblMaxRaum = new JLabel("(max. 20 Zeichen)");
		lblMaxRaum.setForeground(Color.GRAY);
		panel_1.add(lblMaxRaum, "4, 12");
		
		JLabel lblAdresse = new JLabel("Adresse:");
		panel_1.add(lblAdresse, "2, 14, right, default");
		
		tfAdresse = new JTextField();
		panel_1.add(tfAdresse, "4, 14, fill, default");
		tfAdresse.setText((String) bearbeitung[2]);
		tfAdresse.setColumns(10);
		
		lblMaxAdresse = new JLabel("(max. 50 Zeichen)");
		lblMaxAdresse.setForeground(Color.GRAY);
		panel_1.add(lblMaxAdresse, "4, 16");
		
		lblOrt = new JLabel("Ort:");
		lblOrt.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(lblOrt, "2, 18, right, default");
		
		tfOrt = new JTextField();
		tfOrt.setText((String) bearbeitung[3]);
		panel_1.add(tfOrt, "4, 18, fill, default");
		tfOrt.setColumns(10);
		
		lblMaxOrt = new JLabel("(max. 50 Zeichen)");
		lblMaxOrt.setForeground(Color.GRAY);
		panel_1.add(lblMaxOrt, "4, 20");
		setVisible(true);
	}
	
	/*
	 * Mit actionPerfermed() werden die Buttons abgefragt.
	 * Abbrechen laesst das Panel schliessen.
	 * Speichern fuehrt zu erst die Methode checkFields() aus.
	 * Bleibt der 'status' auf true, wird die speichern Methode aus dem Controller ausgefuehrt.
	 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// Panel schliesst sich beim Betaetigen des Abbrechen Buttons mit dispose();
			if(e.getSource() == this.btnAbbrechen) { 
				dispose(); }
			
			
			//Logik zum Speichern
			if(e.getSource() == this.btnSpeichern) {
				/*
				 * Nachdem der Speichern Button betaetigt wird,
				 *  wird die checkField() Methode ausgeführt.
				 *  Wenn status, dieser Methode, den boolean Wert true hat,
				 *  wird der if-Block ausgefuehrt und die Daten zum Speichern bereitgestellt.
				 */
				if(checkFields()) {
				
					
					//array containing all fields
					Object[] fieldData = { cbStandort.getSelectedItem().toString(), tfRaum.getText(), tfAdresse.getText(), tfOrt.getText(), Integer.parseInt(tfOrtId.getText()) };
					//call controller to save data in datastructure
					controller.speichern(fieldData );
					//nach dem Speichern wird das Panel mit dispose(); geschlossen
					dispose();

				}
			}
		}
		

		/* 
		 * checkFields() überprüft die Felder nach Eingaben. 
		 * Bei einem NOT NULL Feld muss es eine Eingabe geben,
		 * sonst wird ein JOptionPane.showMessageDialog angezeigt mit der
		 * Information und die vergessenen Felder werden rot markiert.
		 * Dafür wird der status von Typ boolean von true auf 'false' gesetzt.
		 * Wenn status = false wird, wird auch gleichzeitig ein MessageDialog
		 * geoeffnet, das dem Benutzer auf einen Eingabefehler hinweist.
		 */

		private boolean checkFields() {
			boolean status = true;
			
			if(cbStandort.getSelectedIndex() == -1) {
				cbStandort.setForeground(Color.RED);
				status = false;
			}
			
			if (tfOrt.getText().equals("")){
				lblOrt.setForeground(Color.RED);
				lblOrt.setBackground(Color.RED);
				status = false;
			}
			

			if(status == false) {
				JOptionPane.showMessageDialog(null,
						"Bitte pruefen Sie ihre Eingaben.",
						"Eingabefehler", JOptionPane.INFORMATION_MESSAGE);
			}
			return status;
		}

}
