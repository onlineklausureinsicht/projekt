/**
 * @author Marius Hagen
 * GUI zum bearbeiten einer bestimmten Klausur
 */

package views;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.KlausurBearbeitenController;


public class KlausurBearbeitenView extends JInternalFrame implements ActionListener {
	//controller
	private KlausurBearbeitenController controller;
	
	//buttons
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	//fields for input
	private JTextField tfKlausurid;	
	
	private JComboBox cbMatnr;
	private JLabel lblMatnr;
	
	private JComboBox cbModul;
	private JLabel lblModul;
	
	private JComboBox cbDozid;
	private JLabel lblDozid;

	
	/**
	 * Create the frame.
	 * @param controller
	 */
	public KlausurBearbeitenView(KlausurBearbeitenController controller) {
		setIconifiable(true);
		setClosable(true);
		this.controller = controller;
		
		
		Object[] vals = controller.laden();		//importing data
		
		
		setTitle("Klausur bearbeiten");
		setBounds(100, 100, 452, 317);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnSpeichern = new JButton("Speichern");
		panel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);
		
		btnAbbrechen = new JButton("Abbrechen");
		panel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblKlausurenid = new JLabel("Klausur-ID:");
		panel_1.add(lblKlausurenid, "2, 2, right, default");
		
		tfKlausurid = new JTextField("");
		tfKlausurid.setEditable(false);
		panel_1.add(tfKlausurid, "4, 2, fill, default");
		tfKlausurid.setColumns(10);
		tfKlausurid.setText(""+vals[0]);	//filling klausurid
		
		JLabel lblwirdAutomatischFestgelegt = new JLabel("(wird automatisch festgelegt)");
		lblwirdAutomatischFestgelegt.setForeground(Color.GRAY);
		panel_1.add(lblwirdAutomatischFestgelegt, "4, 4");
		
		lblModul = new JLabel("Modul:");
		panel_1.add(lblModul, "2, 6, right, default");
		
		int selIndexModul = 0;
		cbModul = new JComboBox();
		Object[] modulCBVals = controller.getModulCBValues();
		cbModul.setModel(new DefaultComboBoxModel(modulCBVals)); //loading from controller
		for(String s : (String[])modulCBVals){
			if(s.equals((String)vals[1])) break;
			selIndexModul++;
		}
		cbModul.setSelectedIndex(selIndexModul);	//filling modul bezeichnung
		
		panel_1.add(cbModul, "4, 6, fill, default");
		
		lblMatnr = new JLabel("Matrikelnummer:");
		panel_1.add(lblMatnr, "2, 10, right, default");
		
		int selIndexMatnr = 0;
		cbMatnr = new JComboBox();
		Object[] MatnrCBVals = controller.getMatnrCBValues();
		cbMatnr.setModel(new DefaultComboBoxModel(MatnrCBVals)); //loading from controller
		for(String s : (String[])MatnrCBVals){
			if(s.equals(vals[2])) break;
			selIndexMatnr++;
			//System.out.println("not " + s);
		}
		cbMatnr.setSelectedIndex(selIndexMatnr);	//filling matnr
		panel_1.add(cbMatnr, "4, 10, fill, default");
		
		lblDozid = new JLabel("Dozent-ID:");
		panel_1.add(lblDozid, "2, 14, right, default");
		
		int selIndexDozent = 0;
		cbDozid = new JComboBox();
		Object[] dozentCBVals = controller.getDozentCBValues();
		cbDozid.setModel(new DefaultComboBoxModel(dozentCBVals)); //loading from controller
		for(String s : (String[])dozentCBVals){
			if(s.equals(vals[3])) break;
			selIndexDozent++;
		}
		cbDozid.setSelectedIndex(selIndexDozent);	//filling dozentid
		panel_1.add(cbDozid, "4, 14, fill, default");
		
		setVisible(true);
	}


		@Override
		public void actionPerformed(ActionEvent e) {
			//Fenster schliesst sich bei Abbrechen
			if(e.getSource() == this.btnAbbrechen) { dispose(); }
			
			//Logik zum Speichern
			else if(e.getSource() == this.btnSpeichern) {
				
				if(checkFields()) {
				
					//array containing all fields
					Object[] fieldData = { Integer.parseInt(tfKlausurid.getText()), cbModul.getSelectedItem().toString(), cbMatnr.getSelectedItem().toString(), cbDozid.getSelectedItem().toString() };
					//call controller to save data in datastructure
					controller.speichern(fieldData );
					dispose();
				}
			}
		}


		/**
		 * UEberprueft ob alle Felder Ordnungsgemaess ausgefuellt sind,
		 * wenn nicht wird der Status auf false gesetzt und eine Meldung erscheint.
		 * @return boolean, bei korrekter Ausfuellung wird true zurueckgeliefert
		 */
		private boolean checkFields() {
			boolean status = true;
			lblMatnr.setForeground(Color.GRAY);
			lblModul.setForeground(Color.GRAY);
			lblDozid.setForeground(Color.GRAY);

			if(cbModul.getSelectedIndex() == -1){
				lblModul.setForeground(Color.RED);
				status = false;
			}
			
			if(cbMatnr.getSelectedIndex() == -1){
				lblMatnr.setForeground(Color.RED);
				status = false;
			}
			
			if(cbDozid.getSelectedIndex() == -1){
				lblDozid.setForeground(Color.RED);
				status = false;
			}

			if(status == false) {
				JOptionPane.showMessageDialog(null,
						"Bitte pruefen Sie ihre Eingaben.",
						"Eingabefehler", JOptionPane.INFORMATION_MESSAGE);
			}
			return status;
		}

}
