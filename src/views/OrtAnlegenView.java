package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.OrtAnlegenController;
/**
 * @author Alexander Klippert
 *
 */
public class OrtAnlegenView extends JInternalFrame implements ActionListener {
	
	//buttons
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	//fields for input
	private JTextField tfOrtId;	
	private JFormattedTextField tfRaum;

	private JLabel lblMaxRaum;
	private JComboBox cbStandort;
	private JTextField tfAdresse;
	private JLabel lblMaxAdresse;
	private JLabel lblOrt;
	private JLabel lblAdresse;
	private JLabel lblRaum;
	private JTextField tfOrt;
	private JLabel lblMaxOrt;
	private OrtAnlegenController controller;

	
		/**
	 * Create the frame.
	 */
	public OrtAnlegenView(OrtAnlegenController controller, int Klausurid) {
			setIconifiable(true);
			setClosable(true);
		this.controller = controller;
		
		
		setTitle("Neuen Raum anlegen");
		setBounds(100, 100, 452, 382);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnSpeichern = new JButton("Speichern");
		panel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);
		
		btnAbbrechen = new JButton("Abbrechen");
		panel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
		
		/* hier wird der Rahmen im Panel erstellt,
		 * so werden die Textfields etc. nicht bis an den Rand gestreckt.
		 */
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblOrtid = new JLabel("Ort-ID:");
		panel_1.add(lblOrtid, "2, 2, right, default");
		
		/*
		 * Das Textfield fuer die OrtID wird auf .setEditable(false) gestellt,
		 * da diese nicht vom Benutzer vergeben, sondern vom Programm
		 * automatisch vergeben wird
		 */
		tfOrtId = new JTextField(String.valueOf(Klausurid));
		tfOrtId.setEditable(false);
		panel_1.add(tfOrtId, "4, 2, fill, default");
		tfOrtId.setColumns(10);
		
		JLabel lblwirdAutomatischFestgelegtOrtid = new JLabel("(wird automatisch festgelegt)");
		lblwirdAutomatischFestgelegtOrtid.setForeground(Color.GRAY);
		panel_1.add(lblwirdAutomatischFestgelegtOrtid, "4, 4");
		
		JLabel lblStandort = new JLabel("Standort:");
		panel_1.add(lblStandort, "2, 6, right, default");
		
		cbStandort = new JComboBox();
		cbStandort.setModel(new DefaultComboBoxModel(new String[] {"FB1", "FB2", "FB3", "FB4", "FB5"}));
		panel_1.add(cbStandort, "4, 6, fill, default");
		
		lblRaum = new JLabel("Raum:");
		panel_1.add(lblRaum, "2, 10, right, default");
		
		tfRaum = new JFormattedTextField();
		panel_1.add(tfRaum, "4, 10, fill, default");
		
		lblMaxRaum = new JLabel("(max. 20 Zeichen)");
		lblMaxRaum.setForeground(Color.GRAY);
		panel_1.add(lblMaxRaum, "4, 12");
		
		lblAdresse = new JLabel("Adresse:");
		panel_1.add(lblAdresse, "2, 14, right, default");
		
		tfAdresse = new JTextField();
		panel_1.add(tfAdresse, "4, 14, fill, default");
		tfAdresse.setColumns(10);
		
		lblMaxAdresse = new JLabel("(max. 50 Zeichen)");
		lblMaxAdresse.setForeground(Color.GRAY);
		panel_1.add(lblMaxAdresse, "4, 16");
		
		lblOrt = new JLabel("Ort:");
		lblOrt.setHorizontalAlignment(SwingConstants.RIGHT);
		panel_1.add(lblOrt, "2, 18, right, default");
		
		tfOrt = new JTextField();
		panel_1.add(tfOrt, "4, 18, fill, default");
		tfOrt.setColumns(10);
		
		lblMaxOrt = new JLabel("(max. 50 Zeichen)");
		lblMaxOrt.setForeground(Color.GRAY);
		panel_1.add(lblMaxOrt, "4, 20");
		setVisible(true);
	}


		@Override
		public void actionPerformed(ActionEvent e) {
			//Fenster schliesst sich bei Abbrechen
			if(e.getSource() == this.btnAbbrechen) { dispose(); }
			
			
			//Logik zum Speichern
			if(e.getSource() == this.btnSpeichern) {
				
				if(checkFields()) {
					
					/*
					 * Falls in der Methode checkFields() zwischenzeitlich status = false
					 * wurde, werden hier die Labels wieder mit schwarzen Buchstaben
					 * versehen, sonst wuerden diese rot bleiben, so lange das Panel
					 * nicht geschlossen und wieder neu geoeffnet wird.
					 */
				
					lblMaxRaum.setForeground(Color.GRAY);
					lblRaum.setForeground(Color.BLACK);
					cbStandort.setForeground(Color.BLACK);
					lblOrt.setForeground(Color.BLACK);
					lblAdresse.setForeground(Color.BLACK);
					
					// array containing all fields
					Object[] fieldData = { cbStandort.getSelectedItem().toString(), tfRaum.getText(), tfAdresse.getText(), tfOrt.getText(), Integer.parseInt(tfOrtId.getText()) };
					//call controller to save data in datastructure
					controller.speichern(fieldData );
					
					// increment ortid
					// nach erfolgreicher Speicherung werden die Felder geleert 
					// bzw. auf Default gesetzt und die OrtID um 1 erhöht.
					int current = Integer.parseInt(tfOrtId.getText());
					current++;
					this.tfOrtId.setText(String.valueOf(current));
					this.cbStandort.setSelectedItem(1);
					this.tfRaum.setText("");
					this.tfAdresse.setText("");
					this.tfOrt.setText("");
				}
			}
		}

		/* 
		 * checkFields überprüft die Felder nach Eingaben. 
		 * Bei einem NOT NULL Feld muss es eine Eingabe geben,
		 * sonst wird ein JOptionPane.showMessageDialog angezeigt mit der
		 * Information und die vergessenen Felder werden rot markiert.
		 * Dafür wird der status von Typ boolean von true auf 'false' gesetzt.
		 */
		
		private boolean checkFields() {
			boolean status = true;
			
			if (tfRaum.getText().equals("")) {
				lblRaum.setForeground(Color.RED);
				lblRaum.setBackground(Color.RED);
				status = false;
			}
			
			if(cbStandort.getSelectedIndex() == -1) {
				cbStandort.setForeground(Color.RED);
				status = false;
			}
			
			if (tfOrt.getText().equals("")){
				lblOrt.setForeground(Color.RED);
				lblOrt.setBackground(Color.RED);
				status = false;
			}
			
			if (tfAdresse.getText().equals("")) {
				lblAdresse.setForeground(Color.RED);
				status = false;
			}

			if(status == false) {
				JOptionPane.showMessageDialog(null,
						"Bitte pruefen Sie ihre Eingaben.",
						"Eingabefehler", JOptionPane.INFORMATION_MESSAGE);
			}
			return status;
		}

}
