/**
 * @author Alexander Klippert, Marius Hagen
 * GUI zum anlegen von Klausuren.
 */

package views;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.KlausurAnlegenController;

public class KlausurAnlegenView extends JInternalFrame implements ActionListener {
	//controller
	private KlausurAnlegenController controller;
	
	//buttons
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	//fields for input
	private JTextField tfKlausurid;
	
	private JComboBox cbMatnr;	
	private JLabel lblMatnr;
	
	private JComboBox cbModul;
	private JLabel lblModul;
	
	private JComboBox cbDozid;
	private JLabel lblDozid;
	

	/**
	 * Create the frame.
	 * @param controller
	 * @param klausurid
	 */
	public KlausurAnlegenView(KlausurAnlegenController controller, int klausurid) {
		setIconifiable(true);
		setClosable(true);
		this.controller = controller;
		
		
		setTitle("Neue Klausur anlegen");
		setBounds(100, 100, 452, 317);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnSpeichern = new JButton("Speichern");
		panel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);
		
		btnAbbrechen = new JButton("Abbrechen");
		panel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
		
		/* hier wird der Rahmen im Panel erstellt,
		 * so werden die Textfields etc. nicht bis an den Rand gestreckt.
		 */
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblKlausurenid = new JLabel("Klausur-ID:");
		panel_1.add(lblKlausurenid, "2, 2, right, default");
		
		tfKlausurid = new JTextField(String.valueOf(klausurid));
		tfKlausurid.setEnabled(false);
		tfKlausurid.setEditable(false);
		panel_1.add(tfKlausurid, "4, 2, fill, default");
		tfKlausurid.setColumns(10);
		
		JLabel lblwirdAutomatischFestgelegt = new JLabel("(wird automatisch festgelegt)");
		lblwirdAutomatischFestgelegt.setForeground(Color.GRAY);
		panel_1.add(lblwirdAutomatischFestgelegt, "4, 4");
		
		lblModul = new JLabel("Modul:");
		panel_1.add(lblModul, "2, 6, right, default");
		
		//die ComboBoxen werden ueber StringArrays des Controllers gefuettert
		
		cbModul = new JComboBox();
		cbModul.setModel(new DefaultComboBoxModel(controller.getModulCBValues()));
		cbModul.setSelectedIndex(-1); //add empty set to behave someone forget to enter the RIGHT values
		panel_1.add(cbModul, "4, 6, fill, default");
		
		lblMatnr = new JLabel("Matrikelnummer:");
		panel_1.add(lblMatnr, "2, 10, right, default");
		
		cbMatnr = new JComboBox();
		cbMatnr.setModel(new DefaultComboBoxModel(controller.getMatnrCBValues()));
		cbMatnr.setSelectedIndex(-1); //add empty set to behave someone forget to enter the RIGHT values
		panel_1.add(cbMatnr, "4, 10, fill, default");
		
		lblDozid = new JLabel("Dozent-ID:");
		panel_1.add(lblDozid, "2, 14, right, default");
		
		cbDozid = new JComboBox();
		cbDozid.setModel(new DefaultComboBoxModel(controller.getDozentCBValues()));
		cbDozid.setSelectedIndex(-1); //add empty set to behave someone forget to enter the RIGHT values
		panel_1.add(cbDozid, "4, 14, fill, default");
		setVisible(true);
	}

		/**
		 * Hier werden Alle ButtonEvents behandelt
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			//Fenster schliesst sich bei Abbrechen
			if(e.getSource() == this.btnAbbrechen) { 
				dispose(); }
			
			//Logik zum Speichern
			else if(e.getSource() == this.btnSpeichern) {
				
				/*
				 * Wenn der 'Speichern' Button betaetigt wird,
				 * wird die checkFields() Methode ausgefuehrt
				 * und das Klausur-Objekt an die speichern() 
				 * Methode der controller Klasse uebergeben.
				 * Danach wird ueberprueft ob 'saved == true' ist.
				 * Sobald das Objekt gespeichert ist, werden die
				 * Felder wieder auf einen 'Default' Wert gebracht.
				 * In diesem Fall hier auf 'null' und 'current'.
				 * Sollte die checkFields() Methode zwischenzeitlich
				 * Labels rot markiert haben, werden diese, wenn
				 * 'status' den Wert 'true' bekommt, wieder grau.
				 * So lange 'status = false' ist, geht das Programm
				 * nicht in die if-Schleife und es wird nichts 
				 * gespeichert.
				 */
				
				if(checkFields()) {
					
					lblMatnr.setForeground(Color.BLACK);
					lblModul.setForeground(Color.BLACK);
					lblDozid.setForeground(Color.BLACK);
				
					int current = Integer.parseInt(tfKlausurid.getText());
					//array containing all fields
					Object[] fieldData = { current, cbModul.getSelectedItem().toString(), cbMatnr.getSelectedItem().toString(), cbDozid.getSelectedItem().toString() };
					//call controller to save data in datastructure
					boolean saved = controller.speichern(fieldData );
					if( saved == true ) {
						//increment klausurid
						current++;
						this.tfKlausurid.setText(String.valueOf(current));
						this.cbModul.setSelectedItem(null);
						this.cbMatnr.setSelectedItem(null);
						this.cbDozid.setSelectedItem(null);
					}
				}
			}
		}
		
		/**
		 * Die checkFields() Methode ueberprueft,
		 * ob alle Felder ausgefuellt worden sind.
		 * Dafuer wird erst ein boolean status 
		 * initialisiert. status = true
		 * Sobald ein if-Abfrage zutrifft, wird
		 * dieser Wert auf 'false' gesetzt und
		 * jede Betroffene Zeile rot markiert
		 * und zusaetzlich ein MessageDialog
		 * mit der Fehlerinformation geoeffnet.
		 * 
		 * @return boolean, wenn alle Felder ausgefuellt worder sind,
		 * bleibt 'status = true' und wird zurueckgegeben.
		 */
		private boolean checkFields() {
			boolean status = true;
			lblMatnr.setForeground(Color.GRAY);
			lblModul.setForeground(Color.GRAY);
			lblDozid.setForeground(Color.GRAY);

			if(cbModul.getSelectedIndex() == -1){
				lblModul.setForeground(Color.RED);
				status = false;
			}
			
			if(cbMatnr.getSelectedIndex() == -1){
				lblMatnr.setForeground(Color.RED);
				status = false;
			}
			
			if(cbDozid.getSelectedIndex() == -1){
				lblDozid.setForeground(Color.RED);
				status = false;
			}

			if(status == false) {
				JOptionPane.showMessageDialog(null,
						"Bitte pruefen Sie ihre Eingaben.",
						"Eingabefehler", JOptionPane.INFORMATION_MESSAGE);
			}
			return status;
		}

}
