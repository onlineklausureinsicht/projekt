/**
 * @author Marius Hagen
 * GUI zum anzeigen von bestellten Klausuren
 */

package views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.Vector;

import controller.KlausurBestellteAnzeigenController;
	
public class KlausurBestellteAnzeigenView extends JInternalFrame implements ActionListener{

	private KlausurBestellteAnzeigenController controller;
	
	private JTable table;
	private JButton btnDrucken;
	
	/**
	 * Creating the Frame
	 * @param controller
	 * @param dtm
	 */
	public KlausurBestellteAnzeigenView(KlausurBestellteAnzeigenController controller, DefaultTableModel dtm) {
		setResizable(true);
		this.controller = controller;
		setIconifiable(true);
		setMaximizable(true);
		setClosable(true);
		setVisible(true);
		
		setTitle("Liste der bestellten Klausuren");

		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Buttons = new JPanel();
		getContentPane().add(panel_Buttons, BorderLayout.SOUTH);
		panel_Buttons.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Left = new JPanel();
		panel_Buttons.add(panel_Left, BorderLayout.WEST);
		
		btnDrucken = new JButton("Liste Drucken");
		btnDrucken.addActionListener(this);
		panel_Left.add(btnDrucken);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		//Stellt die SpaltenNamen aus dem Originalen DefaultTableModel wieder her (kopie dessen, welches alle Klausuren hält)
		ArrayList<Object> header = new ArrayList<Object>();
		for(int i=0; i<dtm.getColumnCount(); i++) header.add(dtm.getColumnName(i));
		
		
		/* Im folgenden wird geprüft ob eine Klausur dem Termin-Kriterium entspricht.
		 * Die Abfrage wird aus Geschwindigkeitsgründen bitweise vorgenommen.
		 * Dafür muessen die spalten "Termin Datum", "Termin Standort" und "Termin Raum" gefuellt, also !="" sein.
		 * Da das TableModel als Zellen aber auch nullpointer halten kann,
		 * muss zuerst dies beruecksichtigt werden. filled &= !(... ! null);
		 * nur dann soll der Text verglichen werden. if(filled) filled &= ... .equals("");
		 * Wenn durch die logische konjunktion der Ausgangswert dennoch nicht beeinträchtigt wurde,
		 * Ist die Zeile gefüllt und kann aufgenommen werden.
		*/
		ArrayList<Object[]> preselection = new ArrayList<Object[]>();
		Vector<Vector<Object>> selected_data = dtm.getDataVector();
		for(int i=0; i<selected_data.size(); i++) {
			boolean filled = true;
			
			filled &= !(dtm.getValueAt(i, dtm.findColumn("Termin Datum")) == null);
			if(filled) filled &= !dtm.getValueAt(i, dtm.findColumn("Termin Datum")).toString().equals(""); //if is necessary because of null-value opportunity
			
			filled &= !(dtm.getValueAt(i, dtm.findColumn("Termin Standort")) == null);
			if(filled) filled &= !dtm.getValueAt(i, dtm.findColumn("Termin Standort")).toString().equals(""); //if is necessary because of null-value opportunity
			
			filled &= !(dtm.getValueAt(i, dtm.findColumn("Termin Raum")) == null);
			if(filled) filled &= !dtm.getValueAt(i, dtm.findColumn("Termin Raum")).toString().equals(""); //if is necessary because of null-value opportunity
			
			if(filled){ // if the columns are valid, add them to the "bestellte" arraylist
				Object[] row = selected_data.elementAt(i).toArray();
				preselection.add(row);
			}
		}
		
		DefaultTableModel eingrenzung = new DefaultTableModel(preselection.toArray(new Object[preselection.size()][header.size()] ), header.toArray());
		table = new JTable(eingrenzung);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		setBounds(300,200,500,400);
	}

	/**
	 * Verknuepfung des ButtonDrucks mit der ControllerMethode
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == btnDrucken){
			controller.print(this.getParent()); //parent for adding other windows in desktop
		}
	}

}
