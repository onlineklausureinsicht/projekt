/*
 ******************************************************** 
 *Datei erstellt von | am
 * Maik Nowack		| 08.12.14
 ********************************************************
 *Ã„nderung von		| am		| was
 *					|			|
 ********************************************************
 */

package views;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.VerwTermVerwaltungController;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

/**
 * Klasse zum verwalten der Termine.
 * 
 * @author Maik Nowack
 * @version 1.0
 */
public class VerwTermVerwaltungView extends JInternalFrame implements
		ActionListener {
	private static final long serialVersionUID = 1L;

	// JTable
	private JTable table_termine;

	// Buttons
	private JButton btn_abbr;
	private JButton btn_del;
	private JButton btn_chg;
	private JButton btn_add;

	// TableModel
	private DefaultTableModel terminDtm;

	// Controller
	private VerwTermVerwaltungController ctrl;
	private JButton btn_akt;

	/**
	 * Create the frame.
	 * 
	 * 
	 * @param ctrl
	 *            :VerwTermVerwaltungController - der zur View gehoerende
	 *            Controller
	 * @param dtm
	 *            :DefaultTableModel - das TableModel mit den Terminen zum
	 *            fuellen der Tabelle
	 */
	public VerwTermVerwaltungView(VerwTermVerwaltungController ctrl,
			DefaultTableModel dtm) {
		setTitle("Terminverwaltung");
		setIconifiable(true);
		setClosable(true);
		int x = 867;
		int y = 471;
		final Dimension d = this.getToolkit().getScreenSize();
		setBounds((int) ((d.getWidth() - x) / 2),
				(int) ((d.getHeight() - y) / 2), x, y);
		getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("428px:grow"),
						FormFactory.RELATED_GAP_COLSPEC, }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("193px:grow"),
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.UNRELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.UNRELATED_GAP_ROWSPEC, }));

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "2, 2, fill, fill");

		JPanel p_btn = new JPanel();
		getContentPane().add(p_btn, "2, 5, fill, fill");
		p_btn.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("max(50dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.BUTTON_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.BUTTON_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.BUTTON_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
				FormFactory.PREF_COLSPEC, }, new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
				FormFactory.UNRELATED_GAP_ROWSPEC, }));

		btn_add = new JButton("Termin erstellen");
		btn_add.addActionListener(this);

		btn_akt = new JButton("Aktualisieren");
		p_btn.add(btn_akt, "3, 2");
		p_btn.add(btn_add, "7, 2");

		btn_chg = new JButton("Termin \u00E4ndern");
		btn_chg.addActionListener(this);
		p_btn.add(btn_chg, "9, 2");

		btn_del = new JButton("Termin l\u00F6schen");
		btn_del.addActionListener(this);
		p_btn.add(btn_del, "11, 2");

		btn_abbr = new JButton("Abbrechen");
		btn_abbr.addActionListener(this);
		p_btn.add(btn_abbr, "15, 2");

		// Controller
		this.ctrl = ctrl;
		ctrl.setView(this);

		// Model
		// 09.12.14 Edited by Maik Start
		this.terminDtm = dtm;
		table_termine = new JTable(terminDtm);

		table_termine.getColumnModel().getColumn(0).setMinWidth(80);
		table_termine.getColumnModel().getColumn(0).setMaxWidth(80);
		table_termine.getColumnModel().getColumn(1).setMinWidth(70);
		table_termine.getColumnModel().getColumn(1).setMaxWidth(70);
		table_termine.getColumnModel().getColumn(2).setMinWidth(70);
		table_termine.getColumnModel().getColumn(2).setMaxWidth(70);
		table_termine.getColumnModel().getColumn(3).setMinWidth(110);
		table_termine.getColumnModel().getColumn(3).setMaxWidth(110);
		table_termine.getColumnModel().getColumn(4).setMinWidth(110);
		table_termine.getColumnModel().getColumn(4).setMaxWidth(110);
		table_termine.getColumnModel().getColumn(5).setMinWidth(50);
		table_termine.getColumnModel().getColumn(5).setMaxWidth(50);
		table_termine.getColumnModel().getColumn(6).setMinWidth(70);
		table_termine.getColumnModel().getColumn(6).setMaxWidth(70);

		table_termine.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table_termine);
		setVisible(true);
		// 09.12.14 Edited by Maik End

	}

	/**
	 * unused
	 */
	public void refresh() {
		table_termine = new JTable(terminDtm);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_abbr)
			this.dispose();

		if (e.getSource() == this.btn_akt) {
			this.terminDtm = ctrl.refreshDTM();
			this.table_termine.setModel(terminDtm);
		}
		if (e.getSource() == this.btn_del) {
			if (table_termine.getSelectedRowCount() > 0) {
				int dialogResult = JOptionPane.showConfirmDialog(this,
						"Diesen Termin l\u00F6schen?", "Termin anlegen",
						JOptionPane.OK_CANCEL_OPTION);
				if (dialogResult == JOptionPane.OK_OPTION) {
					int termid = (int) table_termine.getModel().getValueAt(
							table_termine.convertRowIndexToModel(table_termine
									.getSelectedRow()), 6);
					int rowid = table_termine
							.convertRowIndexToModel(table_termine
									.getSelectedRow());
					boolean del = ctrl.delete(termid, rowid);
					if (del) {
						JOptionPane.showMessageDialog(this,
								"Der Termin wurde gel\u00F6scht.", "Info",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane
								.showMessageDialog(
										this,
										"Der Termin konnte nicht gel\u00F6scht werden.",
										"Info", JOptionPane.WARNING_MESSAGE);
					}
				}
			} else {
				JOptionPane
						.showMessageDialog(
								this,
								"W\u00E4hlen Sie zum L\u00F6schen zun\u00E4chst einen Termin aus.",
								"Auswahlfehler",
								JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (e.getSource() == this.btn_chg) {
			if (table_termine.getSelectedRowCount() > 0) {
				int termid = (int) table_termine.getModel().getValueAt(
						table_termine.convertRowIndexToModel(table_termine
								.getSelectedRow()), 6);
				int rowid = table_termine.convertRowIndexToModel(table_termine
						.getSelectedRow());

				// System.out.println("termid: " + termid + "\nrowid: " +
				// rowid);

				ctrl.termChg(termid, rowid);
			} else {
				JOptionPane
						.showMessageDialog(
								this,
								"W\u00E4hlen Sie zum \u00E4ndern zun\u00E4chst einen Termin aus.",
								"Auswahlfehler",
								JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (e.getSource() == this.btn_add) {
			ctrl.terminNeu();
		}
	}

}
