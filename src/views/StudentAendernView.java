package views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import controller.StudentAendernController;
import controller.StudentAnlegenController;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.text.NumberFormatter;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

/**
 * @author A. Zensen
 *
 */
public class StudentAendernView extends JInternalFrame implements
		ActionListener {

	
	private JFormattedTextField tfMatnr;
	private JLabel lblMaxMatnr;
	private JFormattedTextField tfName;
	private JLabel lblMaxName;
	private JFormattedTextField tfVorname;
	private JLabel lblMaxVorname;
	private JFormattedTextField tfEmail;
	private JLabel lblMaxEmail;
	private JComboBox<String> cbStudiengang;
	private JLabel lblStudiengangwahl;
	private JFormattedTextField tfBenutzername;
	private JLabel lblBenutzer;
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	private StudentAendernController studentAendernController;

	/**Dem Konstruktor wird Controller, Daten des zu aendernen Studentenobjekts
	 * und ein ComboBoxModels der Studiengaenge uerbergeben
	 * @param studentAendernController
	 * @param fieldData
	 * @param studgangCBM
	 */
	public StudentAendernView(StudentAendernController studentAendernController, Object[] fieldData, DefaultComboBoxModel<String> studgangCBM) {
		//fieldData = {matnr, name, vorname, email, studiengang, benutzer};
		this.studentAendernController = studentAendernController;
		
		
		System.out.println("Ãœbergabewerte in studentAendernView:");
		for(int i = 0; i < fieldData.length; i++) {
			System.out.println(fieldData[i]);
		}
		
		setupLayout();		
		JPanel btnPanel = setupBtnPanel();
		setupButtons(btnPanel);
		JPanel maskPanel = setupMaskPanel();

		setupLabelsNTextFieldsNComboBox(fieldData, studgangCBM, maskPanel);
		pack();
		this.setVisible(true);

	}

	/** Erstellt Labels und TextFields und fuellt diese
	 * @param fieldData
	 * @param studgangCBM
	 * @param maskPanel
	 */
	private void setupLabelsNTextFieldsNComboBox(Object[] fieldData,
			DefaultComboBoxModel<String> studgangCBM, JPanel maskPanel) {
		JLabel lblMatrikelnummer = new JLabel("Matrikelnummer:");
		maskPanel.add(lblMatrikelnummer, "2, 2, right, default");
		
		System.out.println("Wert fÃ¼r Matrikelnummer: " + fieldData[0]);
		tfMatnr = new JFormattedTextField(String.valueOf(fieldData[0]));
		tfMatnr.setEditable(false);
		maskPanel.add(tfMatnr, "4, 2, fill, default");

		lblMaxMatnr = new JLabel("(nicht verÃ¤nderlich)");
		lblMaxMatnr.setForeground(Color.GRAY);
		maskPanel.add(lblMaxMatnr, "4, 4");

		JLabel lblName = new JLabel("Name:");
		maskPanel.add(lblName, "2, 6, right, default");

		tfName = new JFormattedTextField((String) fieldData[1]);
		maskPanel.add(tfName, "4, 6, fill, default");

		lblMaxName = new JLabel("(max. 50 Zeichen)");
		lblMaxName.setForeground(Color.GRAY);
		maskPanel.add(lblMaxName, "4, 8");

		JLabel lblVorname = new JLabel("Vorname:");
		maskPanel.add(lblVorname, "2, 10, right, default");

		tfVorname = new JFormattedTextField((String) fieldData[2]);
		maskPanel.add(tfVorname, "4, 10, fill, default");

		lblMaxVorname = new JLabel("(max. 50 Zeichen)");
		lblMaxVorname.setForeground(Color.GRAY);
		maskPanel.add(lblMaxVorname, "4, 12, left, bottom");

		JLabel lblEmail = new JLabel("E-Mail:");
		maskPanel.add(lblEmail, "2, 14, right, default");

		tfEmail = new JFormattedTextField((String) fieldData[3]);
		maskPanel.add(tfEmail, "4, 14, fill, default");

		lblMaxEmail = new JLabel("(max. 50 Zeichen)");
		lblMaxEmail.setForeground(Color.GRAY);
		maskPanel.add(lblMaxEmail, "4, 16");

		JLabel lblStudiengang = new JLabel("Studiengang:");
		maskPanel.add(lblStudiengang, "2, 18, right, default");

		cbStudiengang = new JComboBox<String>(studgangCBM);
		cbStudiengang.setSelectedItem(fieldData[4]);
		maskPanel.add(cbStudiengang, "4, 18, fill, default");

		lblStudiengangwahl = new JLabel(
				"(Bitte w\u00E4hlen Sie einen Studiengang)");
		lblStudiengangwahl.setForeground(Color.GRAY);
		maskPanel.add(lblStudiengangwahl, "4, 20");

		JLabel lblBenutzername = new JLabel("Benutzername:");
		maskPanel.add(lblBenutzername, "2, 22, right, default");

		tfBenutzername = new JFormattedTextField((String) fieldData[5]);
		tfBenutzername.setEditable(false);
		maskPanel.add(tfBenutzername, "4, 22, fill, default");

		lblBenutzer = new JLabel("(nicht verÃ¤nderlich)");
		lblBenutzer.setForeground(Color.GRAY);
		maskPanel.add(lblBenutzer, "4, 24");
	}

	/**
	 * Erstellt die Maske
	 */
	private JPanel setupMaskPanel() {
		JPanel maskPanel = new JPanel();
		getContentPane().add(maskPanel, BorderLayout.CENTER);
		maskPanel
				.setLayout(new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));
		return maskPanel;
	}

	/** Erstellt die Buttons
	 * @param btnPanel
	 */
	private void setupButtons(JPanel btnPanel) {
		btnSpeichern = new JButton("\u00C4nderungen speichern");
		btnPanel.add(btnSpeichern);
		btnSpeichern.addActionListener(this);

		btnAbbrechen = new JButton("Abbrechen");
		btnPanel.add(btnAbbrechen);
		btnAbbrechen.addActionListener(this);
	}

	/**
	 * Erstellt das ButtonPanel
	 */
	private JPanel setupBtnPanel() {
		JPanel btnPanel = new JPanel();
		getContentPane().add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		return btnPanel;
	}

	/**
	 * Kurze Methode fuer Layoutanpassung
	 */
	private void setupLayout() {
		setIconifiable(true);
		setClosable(true);
		setTitle("Studenten bearbeiten");
		getContentPane().setLayout(new BorderLayout(0, 0));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnAbbrechen) {
			this.getDesktopPane().remove(this);
			this.dispose();
		}

		if (e.getSource() == this.btnSpeichern) {
			
			// PrÃ¼fung der Felder
			if (checkFields()) {

				int matnr = Integer.parseInt(this.tfMatnr.getText());
				String name = this.tfName.getText();
				String vorname = this.tfVorname.getText();
				String email = this.tfEmail.getText();
				String studiengang = (String) this.cbStudiengang
						.getSelectedItem();
				String benutzer = this.tfBenutzername.getText();
				Object[] fieldData = { matnr, name, vorname, email,
						studiengang, benutzer };
				studentAendernController.speichern(fieldData);
				this.dispose();
			}
		}
	}

	/**Methode zum Pruefen der Feldeingaben
	 * @returns true if fields check out correctly
	 */
	private boolean checkFields() {
		boolean status = true;
		if (this.tfMatnr.getValue() == null) {
			status = false;
			this.lblMaxMatnr.setForeground(Color.RED);
		}
		if (this.tfName.getText().length() <= 0
				|| this.tfName.getText().length() > 50) {
			status = false;
			this.lblMaxName.setForeground(Color.RED);
		}
		if (this.tfVorname.getText().length() <= 0
				|| this.tfVorname.getText().length() > 50) {
			status = false;
			this.lblMaxVorname.setForeground(Color.RED);
		}
		if (this.tfEmail.getText().length() <= 0
				|| this.tfEmail.getText().length() > 50) {
			status = false;
			this.lblMaxEmail.setForeground(Color.RED);
		}
		if (this.cbStudiengang.getSelectedIndex() < 0) {
			status = false;
			this.lblStudiengangwahl.setForeground(Color.RED);
		}
		if (this.tfBenutzername.getText().length() <= 0
				|| this.tfBenutzername.getText().length() > 50) {
			status = false;
			this.lblBenutzer.setForeground(Color.RED);
		}

		return status;
	}

}
