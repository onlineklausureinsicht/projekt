package views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;

import controller.DozentAnlegenController;



import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DropMode;

/**
 * @author A. Zensen
 *
 */
public class DozentAnlegenView extends JInternalFrame implements ActionListener {
	//controller
	private DozentAnlegenController dozentAnlegenController;
	
	//buttons
	private JButton btnSpeichern;
	private JButton btnAbbrechen;
	//fields for input
	private JTextField tfDozid;	
	private JFormattedTextField tfName;
	private JFormattedTextField tfVorname;
	private JFormattedTextField tfEmail;

	private JLabel lblMaxName;
	private JLabel lblMaxVorname;
	private JLabel lblMaxEmail;

	public DozentAnlegenView(DozentAnlegenController controller, int dozid) {
			setIconifiable(true);
			setClosable(true);
		this.dozentAnlegenController = controller;
		
		
		setTitle("Neuen Dozent anlegen");
		setBounds(100, 100, 452, 382);
		
		JPanel btnPanel = setupBtnPanel();		
		setupButtons(btnPanel);		
		JPanel formPanel = setupFormLayout();		
		setupLabelsNTextFields(dozid, formPanel);
		
		setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		//Fenster schlie�t sich bei Abbrechen
		if(e.getSource() == this.btnAbbrechen) { dispose(); }
		
		
		//Logik zum Speichern
		if(e.getSource() == this.btnSpeichern) {
			setFieldsGray();
			if(checkFields()) {					
				
			//array containing all fields
			Object[] fieldData = { Integer.valueOf(tfDozid.getText()), tfName.getText(), tfVorname.getText(), tfEmail.getText() };
			//call controller to save data in datastructure
			dozentAnlegenController.speichern(fieldData );
			
			
			this.dispose();
			}
			
			
		}
	}


	private JPanel setupBtnPanel() {
		JPanel btnPanel = new JPanel();
		btnPanel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		return btnPanel;
	}


		private void setupButtons(JPanel btnPanel) {
			btnSpeichern = new JButton("Speichern");
			btnPanel.add(btnSpeichern);
			btnSpeichern.addActionListener(this);
			
			btnAbbrechen = new JButton("Abbrechen");
			btnPanel.add(btnAbbrechen);
			btnAbbrechen.addActionListener(this);
		}


		private JPanel setupFormLayout() {
			JPanel panel_1 = new JPanel();
			panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			getContentPane().add(panel_1, BorderLayout.CENTER);
			panel_1.setLayout(new FormLayout(new ColumnSpec[] {
					FormFactory.RELATED_GAP_COLSPEC,
					FormFactory.DEFAULT_COLSPEC,
					FormFactory.RELATED_GAP_COLSPEC,
					ColumnSpec.decode("default:grow"),
					FormFactory.RELATED_GAP_COLSPEC,},
				new RowSpec[] {
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,
					FormFactory.RELATED_GAP_ROWSPEC,
					FormFactory.DEFAULT_ROWSPEC,}));
			return panel_1;
		}


		private void setupLabelsNTextFields(int dozid, JPanel formPanel) {
			JLabel lblDozentenid = new JLabel("Dozent-ID:");
			formPanel.add(lblDozentenid, "2, 2, right, default");
			
			tfDozid = new JTextField(String.valueOf(dozid));
			tfDozid.setEditable(false);
			formPanel.add(tfDozid, "4, 2, fill, default");
			tfDozid.setColumns(10);
			
			JLabel lblwirdAutomatischFestgelegt = new JLabel("(wird automatisch festgelegt)");
			lblwirdAutomatischFestgelegt.setForeground(Color.GRAY);
			formPanel.add(lblwirdAutomatischFestgelegt, "4, 4");
			
			JLabel lblName = new JLabel("Name:");
			formPanel.add(lblName, "2, 6, right, default");
			
			tfName = new JFormattedTextField();
			formPanel.add(tfName, "4, 6, fill, default");
			
			lblMaxName = new JLabel("(max. 50 Zeichen)");
			lblMaxName.setForeground(Color.GRAY);
			formPanel.add(lblMaxName, "4, 8");
			
			JLabel lblVorname = new JLabel("Vorname:");
			formPanel.add(lblVorname, "2, 10, right, default");
			
			tfVorname = new JFormattedTextField();
			formPanel.add(tfVorname, "4, 10, fill, default");
			
			lblMaxVorname = new JLabel("(max. 50 Zeichen)");
			lblMaxVorname.setForeground(Color.GRAY);
			formPanel.add(lblMaxVorname, "4, 12");
			
			JLabel lblEmail = new JLabel("e-Mail:");
			formPanel.add(lblEmail, "2, 14, right, default");
			
			tfEmail = new JFormattedTextField();
			formPanel.add(tfEmail, "4, 14, fill, default");
			
			lblMaxEmail = new JLabel("(max. 50 Zeichen)");
			lblMaxEmail.setForeground(Color.GRAY);
			formPanel.add(lblMaxEmail, "4, 16");
		}


		private void setFieldsGray() {
			lblMaxName.setForeground(Color.GRAY);
			lblMaxVorname.setForeground(Color.GRAY);
			lblMaxEmail.setForeground(Color.GRAY);
		}

		/**
		 * Methode zum Pr�fen der Feldinhalte. Setzt Hinweislabel auf rot, wenn das betroffene Feld nicht korrekt ausgef�llt wurde.
		 * @return true, wenn alle Felder korrekt ausgef�llt worden sind.
		 */
		private boolean checkFields() {
			boolean status = true;
			String name = tfName.getText();
			String vorname = tfVorname.getText();
			String email = tfEmail.getText();
			
			if(name.length() <= 0 || name.length() > 50) {
				lblMaxName.setForeground(Color.RED);
				status = false;
			}
			if(vorname.length() <= 0 || vorname.length() > 50) {
				lblMaxVorname.setForeground(Color.RED);
				status = false;
			}
			if(email.length() <= 0 || email.length() > 50 ) {
				lblMaxEmail.setForeground(Color.RED);
				status = false;
			}
			if(status == false) {
				JOptionPane.showMessageDialog(null,
						"Bitte pr�fen Sie ihre Eingaben.",
						"Eingabefehler", JOptionPane.INFORMATION_MESSAGE);
			}
			return status;
		}

}