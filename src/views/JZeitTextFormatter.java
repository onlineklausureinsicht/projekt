package views;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

/**
 * Diese Klasse ueberprueft die Eingabe des korrekten Zeitraums. Sie erbt von
 * der Klasse JFormattedTextField. Dazu wird das JFormattedTextField mit einem
 * MaskFormatter("##:## - ##:## 'Uhr'") erweitert. Die Eingabe wird sobald die
 * Komponente den Fokus verliert, in einen validen Zeitraum korrigiert.
 * 
 * @author Maik Nowack
 * @see JFormattedTextField
 */
@SuppressWarnings("serial")
public class JZeitTextFormatter extends JFormattedTextField implements
		FocusListener {

	MaskFormatter mFormat;

	JZeitTextFormatter() {

		try {

			mFormat = new MaskFormatter("##:## - ##:## 'Uhr'");
			mFormat.setValidCharacters("0123456789");
			mFormat.setPlaceholderCharacter('0');

			mFormat.install(this);

		} catch (ParseException e) {
			System.out.println("Fehler");
		}

		this.addFocusListener(this);

	}

	/**
	 * unused
	 */
	@Override
	public void focusGained(FocusEvent e) {
	}

	/**
	 * Sobald das JFormattedTextField den Focus verliert, wird der Text
	 * eingelesen und mittels SimpleDateFormat in ein Datum geparst. Dabei
	 * korrigiert SimpleDateFormat Eingabefehler. (Dies kann abgestellt werden,
	 * es wird dann jedoch eine ParseException geworfen.) Anschliessend wird die
	 * korrigierte Zeit �ber setText wieder an das JFormattedTextField
	 * zurueckgegeben. Au�erdem wird die Uhrzeit sortiert, so dass die spaetere
	 * Uhrzeit immer rechts steht.
	 */
	@Override
	public void focusLost(FocusEvent arg0) {

		String str_time = this.getText();

		/*
		 * Nur wenn der Text geaendert wurde, soll diese Aktion durchgefuehrt
		 * werden.
		 */
		if (!(str_time.equals("00:00 - 00:00 Uhr"))) {
			String timeStr1 = str_time.substring(0, 5);
			String timeStr2 = str_time.substring(8, 13);
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			Date time1 = null;
			Date time2 = null;
			try {
				time1 = format.parse(timeStr1);
				time2 = format.parse(timeStr2);
				if (time1.compareTo(time2) < 0) {
					this.setText((format.format(time1)) + " - "
							+ (format.format(time2)) + " Uhr");
				} else {
					this.setText((format.format(time2)) + " - "
							+ (format.format(time1)) + " Uhr");
				}
			} catch (ParseException ex) {
				new FehlerMeldung("Fehlerhafte Zeiteingabe");
			}
		}
	}

}
