package views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import controller.DozentAnzeigenController;
import javax.swing.JFrame;

/**
 * @author A.Zensen
 * 
 */
public class DozentAnzeigenView extends JInternalFrame implements
		ActionListener {
	private JTable table;
	private JButton btnAktualisieren;
	private JButton btnNeuerDozent;
	private JButton btnDozentndern;
	private JButton btnDozentLschen;
	private DozentAnzeigenController dozentAnzeigenController;
	private DefaultTableModel dozentenDtm;
	private JButton btnSchliessen;

	/**
	 * Create the frame.
	 */

	public DozentAnzeigenView(DozentAnzeigenController controller,
			DefaultTableModel dtm) {
		// controller
		this.dozentAnzeigenController = controller;
		// model
		this.dozentenDtm = dtm;
		
		setupLayout();

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		setupButtons(panel);

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);


		table = new JTable(dozentenDtm);
		table.setAutoCreateRowSorter(true);
		scrollPane.setViewportView(table);
		setVisible(true);

	}

	private void setupButtons(JPanel panel) {
		btnAktualisieren = new JButton("Aktualisieren");
		panel.add(btnAktualisieren);
		btnAktualisieren.setVisible(false);
		btnAktualisieren.addActionListener(this);
		// Aus Zeitmangel nicht berücksichtigt. Hierfür müssten wir ein
		// Publish/Subscribe-Konzept entwickeln.
		btnAktualisieren.setEnabled(false);

		btnNeuerDozent = new JButton("Neuer Dozent");
		panel.add(btnNeuerDozent);
		btnNeuerDozent.addActionListener(this);

		btnDozentndern = new JButton("Dozent \u00E4ndern");
		panel.add(btnDozentndern);
		btnDozentndern.addActionListener(this);

		btnDozentLschen = new JButton("Dozent l\u00F6schen");
		panel.add(btnDozentLschen);
		btnDozentLschen.addActionListener(this);

		btnSchliessen = new JButton("Schlie\u00DFen");
		panel.add(btnSchliessen);
		btnSchliessen.addActionListener(this);
	}

	private void setupLayout() {
		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setResizable(true);
		setTitle("Dozentenverwaltung");
		setBounds(100, 100, 611, 347);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnSchliessen) {
			this.dispose();
		}
		if (e.getSource() == this.btnAktualisieren) {
			this.dozentenDtm = dozentAnzeigenController.refreshDTM();
			this.table.setModel(dozentenDtm);
		}
		if (e.getSource() == this.btnDozentLschen) {

			if (table.getSelectedRowCount() > 0) {
				int n = JOptionPane
						.showConfirmDialog(
								null,
								"Sind Sie sicher, dass Sie den Dozenten l�schen m�chten?",
								"Best�tigung", JOptionPane.YES_NO_OPTION);
				if (n == 0) {

					int dozid = (int) table.getModel()
							.getValueAt(
									table.convertRowIndexToModel(table
											.getSelectedRow()), 0);
					int rowid = table.convertRowIndexToModel(table
							.getSelectedRow());
					System.out.println(dozid);
					dozentAnzeigenController.lschen(dozid, rowid);
				}
			} else {
				JOptionPane
						.showMessageDialog(
								null,
								"W�hlen Sie zum L�schen zun�chst einen Dozenten aus.",
								"Auswahlfehler",
								JOptionPane.INFORMATION_MESSAGE);
			}
		}
		if (e.getSource() == this.btnDozentndern) {
			if (table.getSelectedRowCount() > 0) {
				int dozid = (int) table.getModel()
						.getValueAt(
								table.convertRowIndexToModel(table
										.getSelectedRow()), 0);
				int rowid = table
						.convertRowIndexToModel(table.getSelectedRow());
				dozentAnzeigenController.dozentAendern(this.getParent(), dozid,
						rowid);
			}
		}
		if (e.getSource() == this.btnNeuerDozent) {
			dozentAnzeigenController.dozentNeu(this.getParent());
		}

	}
}
