package views;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

/**
 * Diese Klasse ueberprueft die Eingabe eines korrekten Datums. Sie erbt von der
 * Klasse JFormattedTextField. Dazu wird das JFormattedTextField mit einem
 * MaskFormatter("##.##.####") erweitert. Die Eingabe wird sobald die Komponente
 * den Fokus verliert, in ein valides Datum korrigiert.
 * 
 * @author Maik Nowack
 * @see JFormattedTextField
 */
@SuppressWarnings("serial")
public class JDateTextFormatter extends JFormattedTextField implements
		FocusListener {

	private MaskFormatter mFormat;

	/**
	 * Konstruktor: Erzeugt ein JFormattedTextField mit einem
	 * MaskFormatter("##.##.####") und setzt das aktuelle Tagesdatum als Text.
	 */
	JDateTextFormatter() {

		try {

			mFormat = new MaskFormatter("##.##.####");
			mFormat.setPlaceholderCharacter('0');

			mFormat.install(this);

		} catch (ParseException e) {
			// System.out.println("Fehler");
		}

		Date heute = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		this.setText((format.format(heute)));
		this.addFocusListener(this);

	}

	/**
	 * unused
	 */
	@Override
	public void focusGained(FocusEvent e) {
	}

	/**
	 * Sobald das JFormattedTextField den Focus verliert, wird der Text
	 * eingelesen und mittels SimpleDateFormat in ein Datum geparst. Dabei
	 * korrigiert SimpleDateFormat Eingabefehler. (Dies kann abgestellt werden,
	 * es wird dann jedoch eine ParseException geworfen.) Anschliessend wird das
	 * korrigierte Datum ueber setText wieder an das JFormattedTextField
	 * zur�ckgegeben.
	 * 
	 * @param FocusEvent
	 */
	@Override
	public void focusLost(FocusEvent e) {

		String str_date = this.getText();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = null;
		try {
			date = format.parse(str_date);
			this.setText((format.format(date)));
		} catch (ParseException ex) {
			new FehlerMeldung("Fehlerhafte Datumseingabe");
		}

	}

}
