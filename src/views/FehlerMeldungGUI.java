/*
 ******************************************************** 
 *Datei erstellt von | am
 *	Maik Nowack		| 27.11.2014
 ********************************************************
 *Änderung von		| am		| was
 *					|			|
 ********************************************************
 */

package views;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

/*
 * Diese Klasse erzeugt eine Graphische Ausgabe von Fehlermeldungen,
 * dazu wird die Fehlermeldung als String an den Konstruktor dieser Klasse übergeben. 
 * Der Konstruktor übergibt die Fehlermeldung an den Konstruktor der JOptionPane. 
 */

public class FehlerMeldungGUI {

	FehlerMeldungGUI(final String meldung) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				JOptionPane op = new JOptionPane(meldung,
						JOptionPane.ERROR_MESSAGE);
				JDialog dialog = op.createDialog("Fehlermeldung");
				//Das Fenster wird immer als oberstes Fenster angezeigt.
				dialog.setAlwaysOnTop(true);
				//Das Fenster wird bleibt aktiv.
				dialog.setModal(true);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setVisible(true);
			}
		});
	}

}
