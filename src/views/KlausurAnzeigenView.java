/**
 * @author Marius Hagen
 * GUI zum anzeigen aller Klausuren
 */

package views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.KlausurAnzeigenController;

public class KlausurAnzeigenView extends JInternalFrame implements ActionListener {
	private KlausurAnzeigenController controller;
	
	private JTable table;
	private JButton btnBearbeiten;
	private JButton btnEntfernen;
	private JButton btnNeu;
	
	/**
	 * Create the Frame
	 * @param controller
	 * @param dtm
	 */
	public KlausurAnzeigenView(KlausurAnzeigenController controller, DefaultTableModel dtm) {
		setResizable(true);
		this.controller = controller;
		setIconifiable(true);
		setMaximizable(true);
		setClosable(true);
		setVisible(true);
		
		setTitle("Liste aller Klausuren");

		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Buttons = new JPanel();
		getContentPane().add(panel_Buttons, BorderLayout.SOUTH);
		panel_Buttons.setLayout(new BorderLayout(0, 0));
		
		JPanel panelRight = new JPanel();
		panel_Buttons.add(panelRight, BorderLayout.EAST);
		
		btnEntfernen = new JButton("Entfernen");
		btnEntfernen.addActionListener(this);
		panelRight.add(btnEntfernen);
		
		btnNeu = new JButton("Hinzuf\u00FCgen");
		btnNeu.addActionListener(this);
		panelRight.add(btnNeu);
		
		JPanel panelLeft = new JPanel();
		panel_Buttons.add(panelLeft, BorderLayout.WEST);
		
		btnBearbeiten = new JButton("Bearbeiten");
		btnBearbeiten.addActionListener(this);
		panelLeft.add(btnBearbeiten);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable(dtm);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		
		setBounds(300,200,500,400);
	}

	/**
	 * Hier werden alle Benutzereingaben wie gedrueckte Buttons erkannt und ausgewertet.
	 * Bearbeitet werden diese anfragen mithilfe der mitgegebenen eigenschaften im Controller.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(arg0.getSource() == btnBearbeiten){
			if(table.getSelectedRowCount() == 1){
				int modelRow = table.convertRowIndexToModel(table.getSelectedRow());
				controller.change(this.getParent(), modelRow); //parent for adding other windows in desktop
				//refresh table model
			}
			else {
				JOptionPane.showMessageDialog(null,"Bitte waehlen sie EINEN Datensatz aus.","Auswahlfehler", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
		else if(arg0.getSource() == btnEntfernen){
			if(table.getSelectedRowCount() > 0){
				int removed = 0; //zum loeschen mehrer Datensaetze gleichzeitig
				for(int tableRow : table.getSelectedRows()){
					int modelRow = table.convertRowIndexToModel(tableRow) - removed; //aufheben der removeRow(modelRow) differenz
					//System.out.println("tableRow: " + tableRow + "  modelRow: " + modelRow);
					int klausurid = (int) table.getModel().getValueAt(modelRow, ((DefaultTableModel) table.getModel()).findColumn("Klausur ID"));
					controller.erase(klausurid);
					((DefaultTableModel) table.getModel()).removeRow(modelRow); //refreshing the default table model
					removed++;
				}
			}
			else {
				JOptionPane.showMessageDialog(null,"Bitte waehlen sie gewuenschte Datensaetze aus.","Auswahlfehler", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
		else if(arg0.getSource() == btnNeu){
			controller.add(this.getParent()); //parent for adding other windows in desktop
		}
	}

}
