package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JInternalFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import com.jgoodies.forms.factories.FormFactory;

import controller.VerwTermChgController;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import java.util.HashSet;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JButton;

import models.Ort;
import models.Studiengang;

/**
 * Klasse um Termine zu aendern.
 * 
 * @author Maik Nowack
 * @version 1.0
 */
public class VerwTermChgView extends JInternalFrame implements ActionListener,
		ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// Buttons
	private JButton btn_abbr;
	private JButton btn_chg;

	private JFormattedTextField ftf_date;
	private JFormattedTextField ftf_zeit1;
	private JFormattedTextField ftf_zeit2;

	private JComboBox<String> cb_standort;
	private JComboBox<String> cb_raum;
	private JComboBox<String> cb_studiengang;

	private JCheckBox checkBox;

	private VerwTermChgController ctrl;

	// Alte Werte speichern
	private String alt_date;
	private String alt_standort;
	private String alt_raum;
	private String alt_zeit1;
	private String alt_zeit2;
	private boolean alt_gesperrt;
	private int terminid;
	private String alt_studiengang;

	/**
	 * Create the frame.
	 * 
	 * @param ctrl
	 *            :VerwTermChgController - der zur View gehoerende Controller
	 * @param datum
	 *            :String - Datum
	 * @param standort
	 *            :String - Standort
	 * @param raum
	 *            :String - Raum
	 * @param zeit1
	 *            :String - Zeitraum 1
	 * @param zeit2
	 *            :String - Zeitraum2 (optional)
	 * @param gesperrt
	 *            :boolean - Sperre
	 * @param terminid
	 *            :int - unique key des Termin, ueber den er in der Datenbank zu
	 *            finden ist
	 * @param studiengang
	 *            :String - Studiengang
	 */
	public VerwTermChgView(VerwTermChgController ctrl, String datum,
			String standort, String raum, String zeit1, String zeit2,
			boolean gesperrt, int terminid, String studiengang) {

		// Uebergebene Objekte speichern
		this.ctrl = ctrl;
		this.alt_date = datum;
		this.alt_standort = standort;
		this.alt_raum = raum;
		this.alt_zeit1 = zeit1;
		this.alt_zeit2 = zeit2;
		this.alt_gesperrt = gesperrt;
		this.terminid = terminid;
		this.alt_studiengang = studiengang;

		setTitle("Termin bearbeiten");
		setClosable(true);
		setBounds(100, 100, 621, 430);
		getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("default:grow"),
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));

		JPanel panel_edit = new JPanel();
		getContentPane().add(panel_edit, "2, 2, fill, fill");
		panel_edit.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.PREF_COLSPEC,
				ColumnSpec.decode("max(50dlu;min):grow(3)"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("50dlu"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("70dlu"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(50dlu;min):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(50dlu;min):grow(3)"),
				FormFactory.RELATED_GAP_COLSPEC, FormFactory.PREF_COLSPEC, },
				new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC,
						FormFactory.PREF_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("pref:grow"), }));

		JLabel label = new JLabel("Datum:");
		panel_edit.add(label, "4, 4, right, top");

		ftf_date = new JDateTextFormatter();
		panel_edit.add(ftf_date, "6, 4, fill, default");
		ftf_date.setText(datum);

		JLabel label_1 = new JLabel("Zeitraum 1:");
		panel_edit.add(label_1, "4, 8, right, default");

		ftf_zeit1 = new JZeitTextFormatter();
		panel_edit.add(ftf_zeit1, "6, 8, fill, default");
		ftf_zeit1.setText(zeit1);

		JLabel label_2 = new JLabel("Zeitraum 2:");
		panel_edit.add(label_2, "4, 10, right, default");

		ftf_zeit2 = new JZeitTextFormatter();
		panel_edit.add(ftf_zeit2, "6, 10, fill, default");
		ftf_zeit2.setText(zeit2);

		JLabel label_3 = new JLabel("(optional)");
		panel_edit.add(label_3, "8, 10");

		JLabel label_4 = new JLabel("Standort:");
		panel_edit.add(label_4, "4, 14, right, default");

		cb_standort = new JComboBox<String>();
		panel_edit.add(cb_standort, "6, 14, 3, 1, fill, default");
		HashSet<String> hs = new HashSet<String>();
		for (Map.Entry<Integer, Ort> entry : ctrl.getOrte().entrySet()) {
			Ort ort = entry.getValue();
			if (hs.add(ort.getStandort()))
				cb_standort.addItem(ort.getStandort());
		}
		cb_standort.setSelectedItem(alt_standort);
		cb_standort.addItemListener(this);

		JLabel label_5 = new JLabel("Raum:");
		panel_edit.add(label_5, "4, 16, right, default");

		cb_raum = new JComboBox<String>();
		panel_edit.add(cb_raum, "6, 16, fill, default");
		for (Map.Entry<Integer, Ort> entry : ctrl.getOrte().entrySet()) {
			Ort ort = entry.getValue();
			if (ort.getStandort().equals(cb_standort.getSelectedItem())) {
				cb_raum.addItem(ort.getRaum());
				// System.out.println(ort.getRaum());
			}
		}
		cb_raum.setSelectedItem(alt_raum);

		JLabel label_6 = new JLabel("Studiengang:");
		panel_edit.add(label_6, "4, 18, right, default");

		cb_studiengang = new JComboBox<String>();
		panel_edit.add(cb_studiengang, "6, 18, 3, 1, fill, default");
		for (Map.Entry<String, Studiengang> entry : ctrl.getStudiengaenge()
				.entrySet()) {
			Studiengang temp = entry.getValue();
			cb_studiengang.addItem(temp.getBezeichnung());
		}
		cb_studiengang.setSelectedItem(alt_studiengang);

		JLabel label_7 = new JLabel("Termin:");
		panel_edit.add(label_7, "4, 20, right, default");

		checkBox = new JCheckBox("gesperrt");
		panel_edit.add(checkBox, "6, 20");
		checkBox.setSelected(gesperrt);

		JPanel panel_btn = new JPanel();
		getContentPane().add(panel_btn, "2, 4, fill, fill");
		panel_btn.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("169px:grow"), FormFactory.DEFAULT_COLSPEC,
				FormFactory.UNRELATED_GAP_COLSPEC, ColumnSpec.decode("89px"),
				FormFactory.PREF_COLSPEC, }, new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC, RowSpec.decode("23px"),
				FormFactory.UNRELATED_GAP_ROWSPEC, }));

		btn_chg = new JButton("\u00C4nderung \u00FCbernehmen");
		btn_chg.addActionListener(this);
		panel_btn.add(btn_chg, "2, 2");

		btn_abbr = new JButton("Abbrechen");
		btn_abbr.addActionListener(this);
		panel_btn.add(btn_abbr, "4, 2, left, top");

		setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_abbr)
			this.dispose();
		else if (e.getSource() == btn_chg) {
			int dialogResult = JOptionPane.showConfirmDialog(this,
					"Die \u00C4nderungen \u00FCbernehmen?",
					"Termin \u00E4ndern", JOptionPane.OK_CANCEL_OPTION);
			if (dialogResult == JOptionPane.OK_OPTION) {

				Object[] fieldData = { terminid, ftf_date.getText(),
						cb_standort.getSelectedItem().toString(),
						cb_raum.getSelectedItem().toString(),
						ftf_zeit1.getText(), ftf_zeit2.getText(),
						checkBox.isSelected(),
						cb_studiengang.getSelectedItem().toString() };
				Object[] old_fieldData = { terminid, alt_date, alt_standort,
						alt_raum, alt_zeit1, alt_zeit2, alt_gesperrt,
						alt_studiengang };

				boolean change = ctrl.change(fieldData, old_fieldData);
				if (change) {
					JOptionPane.showMessageDialog(this,
							"Der Termin wurde ge\u00E4ndert.", "Info",
							JOptionPane.INFORMATION_MESSAGE);
					this.dispose();
				} else {
					JOptionPane.showMessageDialog(this,
							"Der Termin konnte nicht ge\u00E4ndert werden.",
							"Info", JOptionPane.WARNING_MESSAGE);
				}
			}
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == 1) {
			cb_raum.removeAllItems();

			/*
			 * Wenn sich der Standort aendert, werden alle Eintraege aus der
			 * Raum ComboBox entfernt und anschließend mit den Raeumen des neuen
			 * Standortes gefuellt.
			 */
			for (Map.Entry<Integer, Ort> entry : ctrl.getOrte().entrySet()) {
				Ort ort = entry.getValue();
				if (ort.getStandort().equals(cb_standort.getSelectedItem())) {
					cb_raum.addItem(ort.getRaum());
				}
			}
		}

	}

}
