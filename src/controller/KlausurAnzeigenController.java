/**
 * @author Marius Hagen
 * Controller zur GUI KlausurAnzeigenView,
 * stellt Methoden zum verwalten der Liste an Klausuren bereit.
 */

package controller;

import java.awt.Container;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import register.DozentRegister;
import register.KlausurRegister;
import register.ModulRegister;
import register.StudentRegister;
import views.KlausurAnlegenView;
import views.KlausurBearbeitenView;

public class KlausurAnzeigenController {
	
	private KlausurRegister klausurRegister;
	private DozentRegister dozentRegister;
	private ModulRegister modulRegister;
	private StudentRegister studentRegister;
	
	private DefaultTableModel dtm;
	
	private KlausurAnlegenController anlegenController;
	private KlausurAnlegenView anlegenView;
	private KlausurBearbeitenView bearbeitenView;
	private KlausurBearbeitenController bearbeitenController;
	
	
	public KlausurAnzeigenController(DefaultTableModel dtm, KlausurRegister klausurRegister, DozentRegister dozentRegister, ModulRegister modulRegister, StudentRegister studentRegister) {
		this.klausurRegister = klausurRegister;
		this.dozentRegister = dozentRegister;
		this.modulRegister = modulRegister;
		this.studentRegister = studentRegister;
		
		this.dtm = dtm;
	}

	/**
	 * Löscht mit hilfe des Registers Klausuren aus der Datenbank.
	 * @param klausurid
	 */
	public void erase(int klausurid) {
		klausurRegister.deleteKlausur(klausurid);
	}

	/**
	 * Ruft ein neus Fenster zum Anlegen neuer Klausuren auf.
	 * @param desktopPane
	 */
	public void add(Container desktopPane) {
		if (anlegenView != null) 				// wenn schon eine view existiert
			if(anlegenView.isClosed() == false) // und diese noch offen ist
				return;							// dann gibt es hier nichts zu tun
		
		anlegenController = new KlausurAnlegenController(dtm , klausurRegister, dozentRegister, modulRegister, studentRegister);
		anlegenView = new KlausurAnlegenView(anlegenController, klausurRegister.lastKey());
		desktopPane.add(anlegenView);
	}

	/**
	 * Ruft ein neues Fenster zum bearbeiten der ausgewaehlten Klausur aus.
	 * Jedoch nur, wenn diese nicht schon zu einem Termin angelegt ist.
	 * @param desktopPane
	 * @param modelRow
	 */
	public void change(Container desktopPane, int modelRow) {
		boolean filled = true;
		
		filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Datum")) == null);
		if(filled) filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Datum")).equals("")); //if is necessary because of null-value opportunity
		
		filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Raum")) == null);
		if(filled) filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Raum")).equals("")); //if is necessary because of null-value opportunity
		
		filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Standort")) == null);
		if(filled) filled &= !(dtm.getValueAt(modelRow, dtm.findColumn("Termin Standort")).equals("")); //if is necessary because of null-value opportunity
		
		if(!filled)
		{
			if (bearbeitenView != null) 				// wenn schon eine view existiert
				if(bearbeitenView.isClosed() == false) // und diese noch offen ist
					return;							// dann gibt es hier nichts zu tun
			
			bearbeitenController = new KlausurBearbeitenController(dtm , klausurRegister, dozentRegister, modulRegister, studentRegister, modelRow);
			bearbeitenView = new KlausurBearbeitenView(bearbeitenController);
			desktopPane.add(bearbeitenView);
		}
		else JOptionPane.showMessageDialog(null, "Eine terminbehaftete Klausur kann nicht bearbeitet werden.", "", JOptionPane.INFORMATION_MESSAGE);
		//showConfirmDialog(null, "Eine terminbehaftete Klausur kann nicht bearbeitet werden.");
	}
	
	
}
