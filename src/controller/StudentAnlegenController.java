package controller;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import models.Student;
import register.StudentRegister;
import views.StudentAnlegenView;
import SQL.StudentSQL;

/**Controller zum Steuern der Anlegung von neuen Studenten
 * @author A. Zensen
 *
 */
public class StudentAnlegenController {

	private StudentRegister studentRegister;
	private DefaultTableModel studentDtm;
	
	/**
	 * @param studentRegister
	 * @param studentDtm
	 * @param studentAnlegenView
	 */
	public StudentAnlegenController(StudentRegister studentRegister, DefaultTableModel studentDtm) {
		super();
		this.studentRegister = studentRegister;
		this.studentDtm = studentDtm;
	}

	/**
	 * @param fieldData = { matnr, name, vorname, email, studiengang, benutzerTry }
	 */
	public void speichern(Object[] fieldData) {		
		Student stud = new Student((int) fieldData[0], (String) fieldData[1], (String) fieldData[2], 
				(String) fieldData[3], (String) fieldData[4], (String) fieldData[5]);
		//Einfuegen in DB, Register und JTable
		if(StudentSQL.insert(stud)) {
			studentRegister.addStudent(stud);
			studentDtm.addRow(stud.getRow());
			
			JOptionPane.showMessageDialog(null,
					"Student wurde erfolgreich gespeichert. (Benutzerkennung: " + (String) fieldData[5] + ")",
					"Info", JOptionPane.INFORMATION_MESSAGE);
						
		} else {
			JOptionPane.showMessageDialog(null,
					"Fehler beim Speichern des Studenten.",
					"Fehlermeldung", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**Methode zum Pruefen, ob Student schon existiert
	 * @param matnr
	 * @return true, wenn Matrikelnummer und somit Student bereits existiert
	 */
	public boolean matnrExists(int matnr) {
		if(studentRegister.getStudentenMap().containsKey(matnr)) { return true; } else { return false; }
	}

	/**Methode zum Pruefen, ob Benutzerkennung schon existiert
	 * @param benutzer
	 * @return
	 */
	public boolean benutzerExists(String benutzer) {
		return StudentSQL.benutzerExists(benutzer);
	}

}
