/**
 * @author Marius Hagen & Alexander Klippert
 * Stellt die Logik hinter DesktopView
 */

package controller;

import java.awt.Window;

import views.DesktopView;
import views.DozentAnlegenView;
import views.DozentAnzeigenView;
import views.KlausurAnlegenView;
import views.KlausurAnzeigenView;
import views.KlausurBestellteAnzeigenView;
import views.KlausurDruckenView;
import views.OrtAnlegenView;
import views.OrtAnzeigenView;
import views.StudentAnlegenView;
import views.StudentAnzeigenView;
import views.VerwTermAnlView;
import views.VerwTermVerwaltungView;
import controller.DozentAnlegenController;
import controller.DozentAnzeigenController;
import controller.KlausurAnlegenController;
import controller.KlausurAnzeigenController;
import controller.KlausurBestellteAnzeigenController;
import controller.KlausurDruckenController;
import controller.StudentAnlegenController;
import controller.StudentAendernController;
import controller.StudentAnzeigenController;
import register.DozentRegister;
import register.KlausurRegister;
import register.ModulRegister;
import register.OrtRegister;
import register.StudentRegister;
import register.StudiengangRegister;
import register.TerminRegister;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class DesktopController {
	private DesktopView view;
	
	// Wird auf true gesetzt wenn die Register im Thread geladen wurden.
	private boolean ready = false;
	
	// Register
	private ModulRegister modulRegister;
	private StudentRegister studentRegister;
	private KlausurRegister klausurRegister;
	private TerminRegister terminRegister;
	private OrtRegister ortRegister;
	private DozentRegister dozentRegister;
	private StudiengangRegister studiengangRegister;
	
	// DTMs
	private DefaultTableModel modulDtm;
	private DefaultTableModel studentDtm;
	private DefaultTableModel dozentDtm;
	private DefaultTableModel klausurDtm;
	private DefaultTableModel ortDtm;
	private DefaultTableModel terminDtm;
	private DefaultTableModel studiengangDtm;
	// Controller
	private DozentAnzeigenController dozentAnzeigenController;
	private StudentAnlegenController studentAnlegenController;
	private KlausurAnlegenController klausurAnlegenController;
	private KlausurAnzeigenController klausurAnzeigenController;
	private KlausurBestellteAnzeigenController klausurBestellteAnzeigenController;

	private KlausurDruckenController klausurDruckenController;
	private OrtAnlegenController ortAnlegenController;
	private OrtAnzeigenController ortAnzeigenController;
	private DozentAnlegenController dozentAnlegenController;
	private VerwTermVerwaltungController terminVerwaltenController;
	private VerwTermAnlController terminAnlegenController;
	private StudentAnzeigenController studentAnzeigenController;

	// Views
	private DozentAnzeigenView dozentAnzeigenView;
	private DozentAnlegenView dozentAnlegenView;
	private StudentAnzeigenView studentAnzeigenView;
	private StudentAnlegenView studentAnlegenView;
	private OrtAnlegenView ortAnlegenView;
	private OrtAnzeigenView ortAnzeigenView;
	private KlausurAnlegenView klausurAnlegenView;
	private KlausurDruckenView klausurDruckenView;
	private KlausurAnzeigenView klausurAnzeigenView;
	private KlausurBestellteAnzeigenView klausurBestellteAnzeigenView;
	private VerwTermVerwaltungView terminVerwaltenView;
	private VerwTermAnlView terminAnlegenView;


	/**
	 * Einstiegspunkt um View zu erzeugen, und Register zu laden.
	 */
	public DesktopController() {

		view = new DesktopView(this);
		view.setVisible(true);

		// Starten der internen Thread Klasse um das Laden der Register
		// in einen Thread auszulagern.
		LadeRegisterThread registerLoader = new LadeRegisterThread();
		registerLoader.start();
		// Weitere benÃ¯Â¿Â½tigte Register unten im Thread initialisieren.

	}

	/* Es Folgen nun Methoden, die jeweils eine neue Oberfläche oeffnen.
	 * Da diese Fenster als Childs IN EINEM DESKTOP aufgerufen werden,
	 * ist es nicht möglich diese modal zu schalten. Das heißt, dass es
	 * möglich wäre mehrere Instanzen der neuen Oberfläche zu erzeugen.
	 * Die Mehrfachoeffnung ist fur das geschehen jedoch vollkommen
	 * unpraktikabel und nicht leicht zu handhaben, daher haben wir
	 * Die zu oeffnenden Fenster so gebaut, dass diese einen leichten
	 * Umgang ermöglichen. Somit ist es die beste Loesung zu verhindern
	 * dass das Fenster erneut angelegt wird.
	 */
	public void dozentAnlegen() {
		if (klausurAnlegenView != null) // wenn schon eine view existiert
			if (klausurAnlegenView.isClosed() == false) // und diese noch offen
														// ist
				return; // dann gibt es hier nichts zu tun

		dozentAnlegenController = new DozentAnlegenController(dozentRegister,
				dozentDtm);
		dozentAnlegenView = new DozentAnlegenView(dozentAnlegenController,
				dozentRegister.lastKey());
		view.addToDesktop(dozentAnlegenView);
	}

	public void dozentVerwalten() {
		if (dozentAnzeigenView != null) // wenn schon eine view existiert
			if (dozentAnzeigenView.isClosed() == false) // und diese noch offen
														// ist
				return; // dann gibt es hier nichts zu tun

		// ansonsten muss die eventuell existierende view ÃƒÆ’Ã‚Â¼berschrieben
		// (erstellt) werden.
		dozentAnzeigenController = new DozentAnzeigenController(dozentDtm,
				dozentRegister, klausurRegister);
		dozentAnzeigenView = new DozentAnzeigenView(dozentAnzeigenController,
				dozentDtm);
		view.addToDesktop(dozentAnzeigenView);
	}

	public void klausurAnzeigen() {
		// TODO Auto-generated method stub
		if (klausurAnzeigenView != null) // wenn schon eine view existiert
			if (klausurAnzeigenView.isClosed() == false) // und diese noch offen
															// ist
				return; // dann gibt es hier nichts zu tun

		klausurAnzeigenController = new KlausurAnzeigenController(klausurDtm,
				klausurRegister, dozentRegister, modulRegister, studentRegister);
		klausurAnzeigenView = new KlausurAnzeigenView(
				klausurAnzeigenController, klausurDtm);
		view.addToDesktop(klausurAnzeigenView);
	}

	public void klausurAnlegen() {
		if (klausurAnlegenView != null) // wenn schon eine view existiert
			if (klausurAnlegenView.isClosed() == false) // und diese noch offen
														// ist
				return; // dann gibt es hier nichts zu tun

		klausurAnlegenController = new KlausurAnlegenController(klausurDtm,
				klausurRegister, dozentRegister, modulRegister, studentRegister);
		klausurAnlegenView = new KlausurAnlegenView(klausurAnlegenController,
				klausurRegister.lastKey());
		view.addToDesktop(klausurAnlegenView);
	}

	public void klausurBestellteAnzeigen() {
		// TODO Auto-generated method stub
		if (klausurBestellteAnzeigenView != null) // wenn schon eine view
													// existiert
			if (klausurBestellteAnzeigenView.isClosed() == false) // und diese
																	// noch
																	// offen ist
				return; // dann gibt es hier nichts zu tun

		klausurBestellteAnzeigenController = new KlausurBestellteAnzeigenController(
				klausurRegister, studentRegister, modulRegister, terminRegister);
		klausurBestellteAnzeigenView = new KlausurBestellteAnzeigenView(
				klausurBestellteAnzeigenController, klausurDtm);
		view.addToDesktop(klausurBestellteAnzeigenView);
	}

	public void klausurDrucken() {
		// TODO Auto-generated method stub
		if (klausurDruckenView != null) // wenn schon eine view existiert
			if (klausurDruckenView.isClosed() == false) // und diese noch offen
														// ist
				return; // dann gibt es hier nichts zu tun

		klausurDruckenController = new KlausurDruckenController(
				klausurRegister, studentRegister, modulRegister, terminRegister);
		klausurDruckenView = new KlausurDruckenView(klausurDruckenController);
		view.addToDesktop(klausurDruckenView);
	}

	public void ortAnlegen() {
		if (ortAnlegenView != null) // wenn schon eine view existiert
			if (ortAnlegenView.isClosed() == false) // und diese noch offen ist
				return; // dann gibt es hier nichts zu tun

		ortAnlegenController = new OrtAnlegenController(ortDtm, ortRegister);
		ortAnlegenView = new OrtAnlegenView(ortAnlegenController,
				ortRegister.lastKey());
		view.addToDesktop(ortAnlegenView);
	}

	// public void ersteHilfe() {
	// // TODO Auto-generated method stub
	// if (ErsteHilfeView != null) // wenn schon eine view existiert
	// if(ErsteHilfeView.isClosed() == false) // und diese noch offen ist
	// return; // dann gibt es hier nichts zu tun
	//
	// ersteHilfeController = new ErsteHilfeController();
	// ersteHilfeView = new ErsteHilfeView(ersteHilfeController);
	// view.addToDesktop(ersteHilfeView);
	// }
	//
	// public void studiengangAnlegen() {
	// // TODO Auto-generated method stub
	// if (studiengangAnlegenView != null) // wenn schon eine view existiert
	// if(studiengangAnlegenView.isClosed() == false) // und diese noch offen
	// ist
	// return; // dann gibt es hier nichts zu tun
	//
	// studiengangAnlegenController = new
	// StudiengangAnlegenController(studiengangDtm, studiengangRegister);
	// studiengangAnlegenView = new
	// StudiengangAnlegenView(studiengangAnlegenController);
	// view.addToDesktop(studiengangAnlegenView);
	// }
	//
	// public void studiengangVerwalten() {
	// TODO Auto-generated method stub
	// if (studiengangVerwaltenView != null) // wenn schon eine view existiert
	// if(studiengangVerwaltenView.isClosed() == false) // und diese noch offen
	// ist
	// return; // dann gibt es hier nichts zu tun
	//
	// studiengangVerwaltenController = new
	// StudiengangVerwaltenController(studiengangDtm, studiengangRegister);
	// studiengangVerwaltenView = new
	// StudiengangVerwaltenView(studiengangVerwaltenController);
	// view.addToDesktop(studiengangVerwaltenView);
	// }
	//
	// public void modulVerwalten() {
	// // TODO Auto-generated method stub
	// if (modulVerwaltenView != null) // wenn schon eine view existiert
	// if(modulVerwaltenView.isClosed() == false) // und diese noch offen ist
	// return; // dann gibt es hier nichts zu tun
	//
	// modulVerwaltenController = new ModulVerwaltenController(modulDtm,
	// modulRegister);
	// modulVerwaltenView = new ModulVerwaltenView(modulVerwaltenController);
	// view.addToDesktop(modulVerwaltenView);
	//
	// }
	//
	public void studentAnlegen() {
		// TODO Auto-generated method stub
		if (studentAnlegenView != null) // wenn schon eine view existiert
			if (studentAnlegenView.isClosed() == false) // und diese noch offen
														// ist
				return; // dann gibt es hier nichts zu tun

		studentAnlegenController = new StudentAnlegenController(
				studentRegister, studentDtm);
		studentAnlegenView = new StudentAnlegenView(studentAnlegenController,
				studiengangRegister.getCbm());
		view.addToDesktop(studentAnlegenView);
	}

	public void studentVerwalten() {
		// TODO Auto-generated method stub
		if (studentAnzeigenView != null) // wenn schon eine view existiert
			if (studentAnzeigenView.isClosed() == false) // und diese noch offen
															// ist
				return; // dann gibt es hier nichts zu tun

		studentAnzeigenController = new StudentAnzeigenController(
				studentRegister, studiengangRegister, studentDtm,
				klausurRegister, klausurDtm);
		studentAnzeigenView = new StudentAnzeigenView(
				studentAnzeigenController, studentDtm);
		view.addToDesktop(studentAnzeigenView);

	}

	public void ortVerwalten() { // 09.12.14 editiert by Alex
		// TODO Auto-generated method stub // ortVerwalten() und ortAnlegen()
		// Kommentierung entfernt
		if (ortAnzeigenView != null) // wenn schon eine view existiert //
										// imports und privates gesetzt
			if (ortAnzeigenView.isClosed() == false) // und diese noch offen ist
				return; // dann gibt es hier nichts zu tun

		ortAnzeigenController = new OrtAnzeigenController(ortDtm, ortRegister);
		ortAnzeigenView = new OrtAnzeigenView(ortAnzeigenController, ortDtm);
		view.addToDesktop(ortAnzeigenView);
	}

	// 09.12.14 Edited by Maik Start
	/**
	 * Erzeugt die Terminverwaltungsview und ihren Controller.
	 * 
	 * @author Maik Nowack
	 */
	public void terminVerwalten() {
		if (terminVerwaltenView != null)
			if (terminVerwaltenView.isClosed() == false)
				return;
		terminVerwaltenController = new VerwTermVerwaltungController(terminDtm,
				terminRegister, ortRegister, studiengangRegister,
				klausurRegister, studentRegister);
		terminVerwaltenView = new VerwTermVerwaltungView(
				terminVerwaltenController, terminDtm);
		view.addToDesktop(terminVerwaltenView);
	}

	/**
	 * Erzeugt die Terminanlegenview und ihren Controller.
	 * 
	 * @author Maik Nowack
	 */
	public void terminAnlegen() {
		if (terminAnlegenView != null)
			if (terminAnlegenView.isClosed() == false)
				return;
		terminAnlegenController = new VerwTermAnlController(terminDtm,
				terminRegister, ortRegister, studiengangRegister);
		terminAnlegenView = new VerwTermAnlView(terminAnlegenController,
				terminRegister.lastKey());
		view.addToDesktop(terminAnlegenView);
	}

	// 09.12.14 Edited by Maik End

	// 10.12.14 Edited by Maik Start
	/**
	 * Im Thread werden die Register geladen, dadurch kann das Programm erst
	 * einmal normal weiterlaufen wenn keine Datenbankverbindung besteht. Durch
	 * ready kann dann abgefragt werden, ob die Register geladen wurden. Keine
	 * Essentielle Methode, aber falls keine Verbindung zur Datenbank
	 * hergestellt werden kann, kann man dadurch wenigstens das Programm
	 * beenden. Exkurs: Die Methode start() wird aufgerufen, um den Thread zu
	 * straten. Diese Methode fuehrt einige Initialisierungen durch und ruft
	 * dann die run()-Methode des Threads auf. Exkurs ende.
	 * 
	 * @author Maik Nowack
	 *
	 */
	class LadeRegisterThread extends Thread {
		public void run() {

			terminRegister = new TerminRegister();
			terminDtm = terminRegister.getTableModel();

			dozentRegister = new DozentRegister();
			dozentDtm = dozentRegister.getTableModel();

			klausurRegister = new KlausurRegister();
			klausurDtm = klausurRegister.getTableModel();

			modulRegister = new ModulRegister();
			modulDtm = modulRegister.getTableModel();

			ortRegister = new OrtRegister();
			ortDtm = ortRegister.getTableModel();

			studentRegister = new StudentRegister();
			studentDtm = studentRegister.getTableModel();

			studiengangRegister = new StudiengangRegister();
			studiengangDtm = studiengangRegister.getTableModel();

			setReady(true);
		}
	}

	/**
	 * Schließt das "Warte auf Datenbank" fenster automatisch,
	 * nachden die RegisterKlassen vollkommen geladen wurden.
	 * @author Marius Hagen
	 * @param ready
	 *            :boolean
	 */
	private void setReady(boolean ready) {
		this.ready = ready;
		for (Window w : view.getWindows()) {
			if (w.getClass().isInstance(new javax.swing.JDialog())) {
				w.dispose(); //schließt das JDialog
			}
		}
	}

	/**
	 * Gibt den Status der Registerklassen an.
	 * 
	 * @return boolean: True wenn die Registerklassen geladen wurden, false wenn
	 *         die Registerklassen (noch) nicht geladen wurden
	 */
	public boolean isReady() {
		return ready;
	}

	// 10.12.14 Edited by Maik End
}