package controller;

import java.beans.PropertyVetoException;
import java.util.HashSet;
import java.util.TreeMap;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import defaults.SendMail;
import models.Student;
import models.Termin;
import register.KlausurRegister;
import register.OrtRegister;
import register.StudentRegister;
import register.StudiengangRegister;
import register.TerminKlausurRegister;
import register.TerminRegister;
import views.VerwTermAnlView;
import views.VerwTermChgView;
import views.VerwTermVerwaltungView;

/**
 * @author Maik Nowack
 *
 */
public class VerwTermVerwaltungController {

	// DefaultTableModel
	private DefaultTableModel dtm;

	// Register
	private TerminRegister terminRegister;
	private OrtRegister ortRegister;
	private StudiengangRegister studiengangRegister;
	private KlausurRegister klausurRegister;
	private StudentRegister studentRegister;

	// Controller
	private VerwTermAnlController termAnlController;
	private VerwTermChgController termChgController;

	// View
	private JDesktopPane desktopPane;
	private VerwTermAnlView termAnlView;
	private VerwTermChgView termChgView;
	private VerwTermVerwaltungView verwTermVerwaltungView;

	/**
	 * Konstruktor
	 * 
	 * @param dtm
	 *            :DefaultTableModel
	 * @param terminRegister
	 *            :TerminRegister
	 * @param ortRegister
	 *            :OrtRegister
	 * @param studiengangRegister
	 *            :StudiengangRegister
	 * @param klausurRegister
	 *            :KlausurRegister
	 * @param studentRegister
	 *            :StudentRegister
	 */
	public VerwTermVerwaltungController(DefaultTableModel dtm,
			TerminRegister terminRegister, OrtRegister ortRegister,
			StudiengangRegister studiengangRegister,
			KlausurRegister klausurRegister, StudentRegister studentRegister) {

		this.dtm = dtm;
		this.terminRegister = terminRegister;
		this.ortRegister = ortRegister;
		this.studiengangRegister = studiengangRegister;
		this.klausurRegister = klausurRegister;
		this.studentRegister = studentRegister;

	}

	/**
	 * Diese Methode wird aufgerufen, um einen neuen Termin in der Datenbank zu
	 * loeschen. Wenn sich zu dem Termin bereits Studenten angemeldet haben,
	 * oeffnet sich ein Dialog der Benutzername und Passwort zum versenden der
	 * Benachrichtungs E-Mail abfragt. Wird beim Dialog die Option 'Abbrechen'
	 * gewaehlt, wird keine E-Mail versendet.
	 * 
	 * @param terminID
	 *            :int - zugehoerige TerminID zur selektierten Zeile des
	 *            DefaultTableModels
	 * @param row
	 *            :int - selektierte Zeile des DefaultTableModels
	 * @return boolean: true wenn der Termin aus der Datenbank geloescht werden
	 *         konnte, false wenn ein Fehler aufgetreten ist und der Termin
	 *         nicht aus der Datenbank geloescht werden konnte.
	 */
	public boolean delete(int terminID, int row) {

		HashSet<Integer> matNr = new HashSet<Integer>();
		Termin termin = terminRegister.getTermin(terminID);
		TreeMap<Integer, Student> studenten = studentRegister.getStudentenMap();
		boolean done = true;

		TerminKlausurRegister terminKlausurRegister = new TerminKlausurRegister();

		done = terminKlausurRegister.deleteTerminKlausur(termin);
		if (done) {

			dtm.removeRow(row);

			matNr = terminKlausurRegister.getMatNr();

			if (matNr != null) {

				String from = new String("Terminverwaltung@fh-bielefeld.de");
				String subject = new String("Termin\u00E4nderung");
				String message = new String("Einsichtstermin: "
						+ termin.getDatum() + " " + termin.getZeit1()
						+ " wurde abgesagt.");

				JTextField username = new JTextField();
				JPasswordField password = new JPasswordField();
				Object[] input = { "Benutzername:", username, "Passwort:",
						password };

				int option = JOptionPane.showConfirmDialog(null, input,
						"E-Mail senden", JOptionPane.OK_CANCEL_OPTION);
				if (option == JOptionPane.OK_OPTION) {

					SendMail mail = new SendMail(null, null, from, subject,
							message);

					for (int m : matNr) {
						Student s;
						s = studenten.get(m);
						mail.setTo(s.getEmail());
						mail.send(username.getText(), new StringBuilder()
								.append(password.getPassword()).toString());
					}

				}

			}
			return true;
		} else {
		}
		return done;
	}

	/**
	 * Diese Methode erzeugt die View und den Controller, die zum aendern eines
	 * bestehenden Termins benoetigt werden.
	 * 
	 * @param termID
	 *            :int - zugehoerige TerminID zur selektierten Zeile des
	 *            DefaultTableModels
	 * @param rowID
	 *            :int - selektierte Zeile des DefaultTableModels
	 */
	public void termChg(int termID, int rowID) {
		desktopPane = verwTermVerwaltungView.getDesktopPane();
		Termin termin = terminRegister.getTermin(termID);

		if (termChgController == null) {
			termChgController = new VerwTermChgController(dtm, rowID,
					terminRegister, ortRegister, studiengangRegister,
					klausurRegister, studentRegister);
		} else if (termChgView != null) {
			termChgView.dispose();
		}

		termChgView = new VerwTermChgView(termChgController, termin.getDatum(),
				termin.getStandort(), termin.getRaum(), termin.getZeit1(),
				termin.getZeit2(), termin.isGesperrt(), termin.getTerminid(),
				termin.getStudiengang());
		desktopPane.add(termChgView);

		try {
			termChgView.setSelected(true);
		} catch (PropertyVetoException e) {

			// Hier noch eine Graphische Fehlermeldung einbauen

			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode erzeugt die View und den Controller, die zum anlegen eines
	 * neuen Termins benoetigt werden.
	 * 
	 */
	public void terminNeu() {
		desktopPane = verwTermVerwaltungView.getDesktopPane();
		if (termAnlController == null) {
			termAnlController = new VerwTermAnlController(dtm, terminRegister,
					ortRegister, studiengangRegister);
		}
		if (termAnlView != null)
			termAnlView.dispose();
		termAnlView = new VerwTermAnlView(termAnlController,
				terminRegister.lastKey());
		desktopPane.add(termAnlView);

		termAnlView.setVisible(true);

		try {
			termAnlView.setSelected(true);
		} catch (PropertyVetoException e) {
			// Hier noch eine Graphische Fehlermeldung einbauen
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode holt das aktuelle DefaultTableModel aus dem TerminRegister.
	 * 
	 * @return DefaultTableModel
	 */
	public DefaultTableModel refreshDTM() {
		this.dtm = terminRegister.getTableModel();
		return this.dtm;

	}

	/**
	 * @param verwTermVerwaltungView
	 *            :VerwTermVerwaltungView
	 */
	public void setView(VerwTermVerwaltungView verwTermVerwaltungView) {
		this.verwTermVerwaltungView = verwTermVerwaltungView;

	}

}
