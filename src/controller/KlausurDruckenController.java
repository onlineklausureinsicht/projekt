/**
 * @author Marius Hagen
 * Erstellt für IText eine Lister der angemeldeten Klausuren zu einem Termin.
 */

package controller;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JOptionPane;

import models.Klausur;
import models.Modul;
import models.Student;
import models.Termin;
import defaults.PDFCreator;
import register.KlausurRegister;
import register.ModulRegister;
import register.StudentRegister;
import register.TerminRegister;

public class KlausurDruckenController {

	private KlausurRegister klausurRegister;
	private StudentRegister studentRegister;
	private ModulRegister modulRegister;
	private TerminRegister terminRegister;
	private String statistikpuffer;
	
	public KlausurDruckenController(KlausurRegister klausurRegister, StudentRegister studentRegister, ModulRegister modulRegister, TerminRegister terminRegister) {
		this.klausurRegister = klausurRegister;
		this.studentRegister = studentRegister;
		this.modulRegister = modulRegister;
		this.terminRegister = terminRegister;
	}

	/**
	 * Erstellt die Liste zum Termin t und sperrt diesen, so dass keine Anmeldungen mehr erfogen k�nnen
	 * @param t
	 * @return String[] anzuzeigende Daten der Klausur innerhalb des Listeneintrags
	 */
	private String[] createList(Termin t) {
		
		ArrayList<Klausur> bestellte = new ArrayList<Klausur>();
		bestellte.addAll(klausurRegister.getKlausurenMap().values());
		
		int max = bestellte.size();
		int min;
		for(min=0; min<bestellte.size(); min++) {
			
			boolean filled = true;
			
			filled &= !(bestellte.get(min).getTerminDatum() == null);
			if(filled) filled &= bestellte.get(min).getTerminDatum().equals(t.getDatum()); //if is necessary because of null-value opportunity
			
			filled &= !(bestellte.get(min).getTerminRaum() == null);
			if(filled) filled &= bestellte.get(min).getTerminRaum().equals(t.getRaum()); //if is necessary because of null-value opportunity
			
			filled &= !(bestellte.get(min).getTerminStandort() == null);
			if(filled) filled &= bestellte.get(min).getTerminStandort().equals(t.getStandort()); //if is necessary because of null-value opportunity
			
			if(!filled){ // if the columns are valid, add them to the "bestellte" arraylist later
				bestellte.remove(min--);	//deleting some out of the arraylist will
										//move down the coming objects beginning to that index witch is deleted
										//and also decrement the size of arraylist. So we have to test this "i" again.
			}
		}
		
		//Sorting
		Collections.sort(bestellte, new Comparator<Klausur>() {
	        @Override
	        public int compare(Klausur  k1, Klausur  k2)
	        {
	            int where = k1.getModul().compareTo(k2.getModul());
	            if( where == 0 ){
	            	if(k1.getMatrikelNummer() < k2.getMatrikelNummer()) where = -1;
	            	else where = 1;
	            }
	            return where;
	        }
		});
		
		//create statistics
		statistikpuffer = "" + min + " von " + max + " Klausuren.";
		
		//pack all lines for return
		ArrayList<String> lines = new ArrayList<String>();
		for(Klausur k : bestellte) {
			Student s= studentRegister.getStudent(k.getMatrikelNummer());
			Modul m = modulRegister.getModul(k.getModul());
			lines.add(k.getMatrikelNummer() + " (" + s.getName() + ", " + s.getVorname() + ") " + k.getModul()); //m.getPruefnummer()
		}
		
		return lines.toArray(new String[lines.size()] );
	}
	
	private String getStat() {
		return statistikpuffer;
	}
	
	/**
	 * Erstellt die PDF, die man hinterher drucken oder oeffnen kann.
	 * @return File
	 */
	private File createFile() {
		File f= new File("temp.pdf");
		
		ArrayList<String> boxLines = new ArrayList<String>();
		for(Termin t : terminRegister.getTermine().values()) {
			boxLines.add("" + t.getTerminid() + " " + t.getDatum() + " " + t.getZeit1() + " " + t.getStandort() + " " + t.getRaum());
		}
		
		String selected = (String) JOptionPane.showInputDialog(null, "Zu welchem Termin moechten sie drucken?","Terminwahl",JOptionPane.QUESTION_MESSAGE, null, boxLines.toArray(new String[boxLines.size()]), boxLines.get(0));
		int terminid = Integer.parseInt(selected.substring(0, selected.indexOf(" ")));
		Termin t = terminRegister.getTermin(terminid);
		
		//sperrt den termin und schreibt ihn fest
		t.setGesperrt(true);
		terminRegister.setTermin(t.getTerminid(), t);
		
		String[] lines = createList(t);
		String stats = getStat();
		
		//itext starts here
		PDFCreator.createKlausurenListe(f.getAbsolutePath(),"Liste bestellter Klausuren des Termins:\n\t" + t.getDatum() + " " + t.getZeit1() + " " + t.getRaum() + " " + t.getStandort(), lines, stats);
		return f;
	}
	
	/**
	 * Versucht die PDF zu drucken.
	 */
	public void print() {
		File f = createFile();
		try {
			Desktop.getDesktop().print(f);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Versucht die PDF in einem vorinstallierten Reader zu oeffnen.
	 */
	public void open() {
		File f = createFile();
		try {
			Desktop.getDesktop().open(f);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
