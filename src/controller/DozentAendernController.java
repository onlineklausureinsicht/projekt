package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import SQL.DozentSQL;
import models.Dozent;
import register.DozentRegister;

/**Controller zur Steuerung von Ver�nderungen an einem Dozenten
 * @author A. Zensen
 *
 */
public class DozentAendernController {

	private DozentRegister dozentRegister;
	private DefaultTableModel dozentDtm;
	private int rowid;
	
	public DozentAendernController(DozentRegister dozentRegister, DefaultTableModel dozentDtm, int rowid) {
		this.dozentRegister = dozentRegister;
		this.dozentDtm = dozentDtm;
		this.rowid = rowid;
	}

	/** Speichert die Ver�nderungen lokal in den Registern und persistent in der DB; macht �nderungen in JTable sichtbar
	 * @param fieldData bestehend aus dozid, name, vorname und email
	 */
	public void speichern(Object[] fieldData) {
		Dozent doz = dozentRegister.getDozent((int) fieldData[0]);
		doz.setUpdate(true);
		doz.setName((String) fieldData[1]);
		doz.setVorname((String) fieldData[2]);
		doz.setEmail((String) fieldData[3]);
		
		this.dozentDtm.setValueAt((String) fieldData[1], rowid, 1);
		this.dozentDtm.setValueAt((String) fieldData[2], rowid, 2);
		this.dozentDtm.setValueAt((String) fieldData[3], rowid, 3);
		
		String statusMsg = "Dozent wurde ";
		
		if(DozentSQL.update(doz.getDozentId(), doz))  {
			statusMsg = statusMsg + "lokal und in der Datenbank gespeichert.";
			doz.setUpdate(false);
		} else { statusMsg = statusMsg + "nur lokal gespeichert."; }
		//hier m�sste ein "sp�ter-persistent-speichern-wenn-DB-nicht-verf�gbar-Konzept greifen,
		//welches �ber die Flags laufen sollte; siehe line 31: doz.setUpdate(true);
		JOptionPane.showMessageDialog(null,
				statusMsg,
				"Info", JOptionPane.INFORMATION_MESSAGE);
		
		
	}

}
