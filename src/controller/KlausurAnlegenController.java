/**
 * @author Marius Hagen
 * Controller Stellt der KlausurAnlegenView Methoden dum bearbeiten der Daten bereit.
 */

package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;

import register.DozentRegister;
import register.KlausurRegister;
import register.ModulRegister;
import register.StudentRegister;
import models.Dozent;
import models.Klausur;
import models.Modul;
import models.Student;

public class KlausurAnlegenController {
	private KlausurRegister register;
	private DozentRegister dozentRegister;
	private ModulRegister modulRegister;
	private StudentRegister studentRegister;
	private DefaultTableModel klausurDtm;
	
	public KlausurAnlegenController(DefaultTableModel klausurDtm, KlausurRegister register, DozentRegister dozentRegister, ModulRegister modulRegister, StudentRegister studentRegister) {
		super();
		this.register = register;
		this.dozentRegister = dozentRegister;
		this.modulRegister = modulRegister;
		this.studentRegister = studentRegister;
		this.klausurDtm = klausurDtm;
	}
	
	/**
	 * Hier werden die Eintraege der View zurueck in den Controller gespeist,
	 * der die Eintraege zerkleinert und an die Datenhaltungsschicht weitergibt.
	 * @param fieldData
	 */
	public boolean speichern(Object[] fieldData) {
		int klausurid = (int) fieldData[0];
		String modul = (String) fieldData[1];
		String mat = (String) fieldData[2]; //this one will be in form of "1003 (Name, Vorname)"
		String doz = (String) fieldData[3]; //this one will be in form of "1003 (Name, Vorname)"
		
		int dozid = Integer.parseInt( doz.substring(0, doz.indexOf(" (")) );
		int matnr = Integer.parseInt( mat.substring(0, mat.indexOf(" (")) );
		
		Klausur klausur = new Klausur(matnr, modul, dozid, "","","", klausurid);
		
		boolean done = register.addKlausur(klausur);
		if( done ) {
			klausurDtm.addRow(klausur.getRow());
			JOptionPane.showMessageDialog(null,"Klausur wurde gespeichert.","Info", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(null,"Diese Klausur existiert bereits","Info", JOptionPane.WARNING_MESSAGE);
		}
		return done;
	}
	
	/**
	 * Stellt die Liste der Modul-ComboBox bereit
	 * @return String[]
	 */
	public String[] getModulCBValues() {
		
		ArrayList<String> al = new ArrayList<String>();
		for(Modul m : modulRegister.getModulMap().values()) {
            al.add(m.getBezeichnung());
        }
		return al.toArray( new String[al.size()] );
	}
	
	/**
	 * Stellt die Liste der Dozent-ComboBox bereit
	 * @return String[]
	 */
	public String[] getDozentCBValues() {
		ArrayList<String> al = new ArrayList<String>();
		for(Dozent d : dozentRegister.getDozentenMap().values()) {
            al.add(d.getDozentId() + " (" + d.getName() + ", " + d.getVorname() + ")");
            //this one will be in form of "1003 (Name, Vorname)"
        }
		return al.toArray( new String[al.size()] );
	}
	
	/**
	 * Stellt die Liste der Student-ComboBox bereit
	 * @return String[]
	 */
	public String[] getMatnrCBValues() {
		ArrayList<String> al = new ArrayList<String>();
		for(Student s : studentRegister.getStudentenMap().values()) {
            al.add(s.getMatnr() + " (" + s.getName() + ", " + s.getVorname() + ")");
            //this one will be in form of "1003 (Name, Vorname)"
        }
		return al.toArray( new String[al.size()] );
	}
	
}
