package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import SQL.DozentSQL;
import register.DozentRegister;
import models.Dozent;

/** Controller, der das Anlegen von neuen Dozenten steuert.
 * @author A. Zensen
 *
 */
public class DozentAnlegenController {
	private DozentRegister dozentRegister;
	private DefaultTableModel dozentDtm;	
	/**
	 * @param dozentRegister
	 * @param dozentDtm
	 */
	public DozentAnlegenController(DozentRegister dozentRegister, DefaultTableModel dozentDtm) {
		super();
		this.dozentRegister = dozentRegister;
		this.dozentDtm = dozentDtm;
	}

	/** Methode zum Speichern eines neuen Dozenten.
	 * @param fieldData bestehend aus dozid, name, vorname, email
	 */
	public void speichern(Object[] fieldData) {
		int dozid = (int) fieldData[0];
		String name = (String) fieldData[1];
		String vorname = (String) fieldData[2];
		String email = (String) fieldData[3];
		
		String statusMsg = "Dozent wurde";
		//erster Parameter true bezieht sich auf ein nicht weiter verfolgtes "sp�ter-persistent-speichern-Konzept";
		//true signalisiert, dass der Dozent neu ist und noch nicht in der DB gespeichert wurde
		Dozent dozent = new Dozent(true, dozid, name, vorname, email);
		if(DozentSQL.insert(dozent.getDozentId(), dozent)) {
			statusMsg = statusMsg + " in der Datenbank und lokal gespeichert.";
		} else { statusMsg = statusMsg + " nur lokal gespeichert."; }
		dozentRegister.addDozent(dozent);
		dozentDtm.addRow(dozent.getRow());
		JOptionPane.showMessageDialog(null,
				statusMsg,
				"Info", JOptionPane.INFORMATION_MESSAGE);				
	}
}
