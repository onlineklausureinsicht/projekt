package controller;

import java.awt.Container;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import SQL.DBConnection;
import SQL.DozentSQL;
import models.Dozent;
import models.Klausur;
import register.DozentRegister;
import register.KlausurRegister;
import views.DesktopView;
import views.DozentAendernView;
import views.DozentAnlegenView;

/** Controller, der die Grundansicht aller Dozenten steuert und
 * die Funktion "L�schen" bereitstellt
 * @author A. Zensen
 *
 */
public class DozentAnzeigenController {
	private DefaultTableModel dozentDtm;
	private DozentRegister dozentRegister;
	private KlausurRegister klausurRegister;
	private DozentAnlegenController dozentAnlegencontroller;
	private DozentAnlegenView dozentAnlegenView;
	private DozentAendernController dozentAendernController;
	private DozentAendernView dozentAendernView;
	
	/**
	 * @param dtm
	 * @param dozentRegister
	 * @param klausurRegister
	 */
	public DozentAnzeigenController(DefaultTableModel dtm,
			DozentRegister dozentRegister, KlausurRegister klausurRegister) {
		super();
		this.dozentDtm = dtm;
		this.dozentRegister = dozentRegister;
		this.klausurRegister = klausurRegister;
	}

	/** Methode zum Loeschen von Dozenten;
	 * schliesst ausserdem alle offenen anlegen-/aendern-Fenster,
	 * um Inkonsistenzen zu unterbinden
	 * @param dozid
	 * @param row
	 */
	public void lschen(int dozid, int row) {
		//Schliessen aller offenen Fenster, die Inkonsistenzen verursachen koennten
		if(dozentAnlegenView != null) {
			dozentAnlegenView.dispose();
		}
		if(dozentAendernView != null) {
			dozentAendernView.dispose();
		}
		//loeschen aus Register
		dozentRegister.deleteDozent(dozid);
		String statusMsg = "Dozent wurde";
		
		//alle Verweise auf den Dozenten in den Klausuren werden auf NULL gesetzt
		String sqlUpdateDozInKlausuren = "UPDATE Klausur SET dozid = NULL WHERE dozid = " + dozid;
		String deleteDozent = "DELETE from Dozent where dozid = " + dozid;
		
		for(Klausur kl : klausurRegister.getKlausurenMap().values()) {
			if(kl.getDozentId() == dozid) {
				kl.setDozentId(0);
			}
		}
		
		Connection con = DBConnection.getCon();
		if(con != null) {
			Statement stmt = null;
		try {			
			stmt = con.createStatement();
			int results = stmt.executeUpdate(sqlUpdateDozInKlausuren);
			
			System.out.println("Updated Klausuren: " + results);	
			results = stmt.executeUpdate(deleteDozent);
			
			if(results == 1) {
				statusMsg = statusMsg + " in der Datenbank und lokal gel�scht.";
			} else { statusMsg = statusMsg + " nur lokal gel�scht."; }
			
			System.out.println("Gel�schte Dozenten: " + results);	
						
		} catch (SQLException e) {
			System.out.println("Fehler in dozentRegister.deleteDozent: " + e.getMessage());
			//			e.printStackTrace();
		}		
		int rowCount = dozentDtm.getRowCount();
		for(int i = 0; i < rowCount; i++) {
			if((int) dozentDtm.getValueAt(i, 0) == dozid) {
				System.out.println("L�sche aus Table Dozid:" + (int) dozentDtm.getValueAt(i, 0));
				dozentDtm.removeRow(i);
			}
		}
		JOptionPane.showMessageDialog(null,
				statusMsg,
				"Info", JOptionPane.INFORMATION_MESSAGE);
		} else {
			//Nachricht, wenn keine DB Verbindung verfuegbar ist
			JOptionPane.showMessageDialog(null,
					"Keine Datenbankverbindung. Versuchen Sie es erneut.",
					"Datenbankverbindung nicht verf�gbar", JOptionPane.INFORMATION_MESSAGE);
		}
				
	}

	public void dozentAendern(Container desktopPane, int dozid, int rowid) {
		Dozent dozent = dozentRegister.getDozent(dozid);
		if(dozentAendernController == null) {
			dozentAendernController = new DozentAendernController(dozentRegister, dozentDtm, rowid);
		}
		if(dozentAendernView != null) { dozentAendernView.dispose(); }
			dozentAendernView = new DozentAendernView(dozentAendernController, dozid, dozent.getName(), dozent.getVorname(), dozent.getEmail());
			desktopPane.add(dozentAendernView);		
		try {
			dozentAendernView.setSelected(true);
		} catch (PropertyVetoException e) {	e.printStackTrace(); }
	}

	public void dozentNeu(Container desktopPane) {
		//Singleton-Ansatz
		if(dozentAnlegencontroller == null) {
			dozentAnlegencontroller = new DozentAnlegenController(dozentRegister, dozentDtm);
		}
		if(dozentAnlegenView != null) dozentAnlegenView.dispose();
			dozentAnlegenView = new DozentAnlegenView(dozentAnlegencontroller, dozentRegister.getDozentenMap().lastKey()+1);
			desktopPane.add(dozentAnlegenView);
		
		dozentAnlegenView.setVisible(true);		
		try {
			dozentAnlegenView.setSelected(true);
		} catch (PropertyVetoException e) {	e.printStackTrace(); }
	}

	public DefaultTableModel refreshDTM() {
		dozentRegister.refresh();
		this.dozentDtm = dozentRegister.getTableModel();
		return this.dozentDtm;
		
	}

}