package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import register.StudentRegister;
import models.Student;
import SQL.StudentSQL;

/**Controller, der das Aendern von Studentendetails steuert
 * @author A. Zensen
 *
 */
public class StudentAendernController {

	private StudentRegister studentRegister;
	private DefaultTableModel studentDtm;
	private int rowid;
	/**
	 * @param studentRegister
	 * @param studentDtm
	 * @param rowid
	 */
	public StudentAendernController(StudentRegister studentRegister, DefaultTableModel studentDtm, int rowid) {
		super();
		this.studentRegister = studentRegister;
		this.studentDtm = studentDtm;
		this.rowid = rowid;
	}	
	/** Methode zum Speichern der Aenderungen
	 * @param fieldData = { matnr, name, vorname, email, studiengang, benutzerTry };
	 */
	public void speichern(Object[] fieldData) {
		//update der Werte am Studentenobjekt
		Student stud = studentRegister.getStudent((int) fieldData[0]);
		stud.setName((String) fieldData[1]);
		stud.setVorname((String) fieldData[2]);
		stud.setEmail((String) fieldData[3]);
		stud.setStudiengang((String) fieldData[4]);
		//aenderungen in DB und im Table wirksam machen
		if(StudentSQL.update(stud)) {
			studentDtm.setValueAt(stud.getName(), rowid, 1);
			studentDtm.setValueAt(stud.getVorname(), rowid, 2);
			studentDtm.setValueAt(stud.getEmail(), rowid, 3);
			studentDtm.setValueAt(stud.getStudiengang(), rowid, 4);
			
			JOptionPane.showMessageDialog(null,
					"Aenderungen wurden erfolgreich gespeichert.",
					"Info", JOptionPane.INFORMATION_MESSAGE);
			
		} else {
			JOptionPane.showMessageDialog(null,
					"Fehler beim Speichern der Aenderungen.",
					"Fehlermeldung", JOptionPane.INFORMATION_MESSAGE);
		}
	}	
}
