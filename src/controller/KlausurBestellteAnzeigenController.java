/**
 * @author Marius Hagen
 * Stellt die Logik hinter der KlausurBestellteAnzeigenView
 */

package controller;

import java.awt.Container;

import register.KlausurRegister;
import register.ModulRegister;
import register.StudentRegister;
import register.TerminRegister;
import views.KlausurDruckenView;

public class KlausurBestellteAnzeigenController {
	
	private KlausurRegister klausurRegister;
	private StudentRegister studentRegister;
	private ModulRegister modulRegister;
	private TerminRegister terminRegister;
	
	private KlausurDruckenView druckenView;
	private KlausurDruckenController druckenController;

	
	public KlausurBestellteAnzeigenController(KlausurRegister klausurRegister, StudentRegister studentRegister, ModulRegister modulRegister, TerminRegister terminRegister){
		this.klausurRegister = klausurRegister;
		this.studentRegister = studentRegister;
		this.modulRegister = modulRegister;
		this.terminRegister = terminRegister;
	}
	
	/**
	 * Nimmt einen Container als Parent für Die KlausurDruckenView.
	 * Reicht der View die Register weiter.
	 * @param desktopPane
	 */
	public void print(Container desktopPane) {
		if (druckenView != null) 				// wenn schon eine view existiert
			if(druckenView.isClosed() == false) // und diese noch offen ist
				return;							// dann gibt es hier nichts zu tun
		
		druckenController = new KlausurDruckenController(klausurRegister, studentRegister, modulRegister, terminRegister);
		druckenView = new KlausurDruckenView(druckenController);
		desktopPane.add(druckenView);
	}

}
