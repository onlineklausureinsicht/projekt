
package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import SQL.OrtSQL;

import models.Ort;

import register.OrtRegister;
/**
 * @author Alexander Klippert
 *
 */

public class OrtBearbeitenController {
	
	private OrtRegister register;
	private DefaultTableModel ortDtm;
	private int modelRow;

	public OrtBearbeitenController(DefaultTableModel ortDtm, OrtRegister register, int modelRow) {
		super();
		this.register = register;
		this.ortDtm = ortDtm;
		this.modelRow = modelRow;
		
	}
	
	public Object[] laden() {
		
		/*
		 * Hier werden die Werte aus dem DefaultTableModel geladen.
		 */
		
		Object[] fieldData = new Object[5];
		
		fieldData[0] = ortDtm.getValueAt(modelRow, ortDtm.findColumn("Standort")).toString(); // Standort
		
		fieldData[1] = ortDtm.getValueAt(modelRow, ortDtm.findColumn("Raum")).toString(); // Raum
		
		fieldData[2] = ortDtm.getValueAt(modelRow, ortDtm.findColumn("Adresse")).toString(); // Adresse

		fieldData[3] = ortDtm.getValueAt(modelRow, ortDtm.findColumn("Ort")).toString(); // sOrt - Stadt

		fieldData[4] = Integer.parseInt(ortDtm.getValueAt(modelRow, ortDtm.findColumn("Ort ID")).toString()); // OrtID

		return fieldData;
	}
	/*
	 * Die Methode speichern() wird durch das Betaetigen des Speichern Buttons in der
	 * OrtBearbeitenView.java ausgefuehrt. 
	 */

	public void speichern(Object[] fieldData) {
		String standort = (String) fieldData[0];
		String raum = (String) fieldData[1];
		String adresse = (String) fieldData[2];
		String sort = (String) fieldData[3];
		int ortid = (int) fieldData[4];
		
		Ort ort = register.getOrt(ortid);
		ort.setStandort(standort);
		ort.setRaum(raum);
		ort.setOrtid(ortid);
		ort.setOrt(sort);
		ort.setAdresse(adresse);
		
		register.setOrt(ortid, ort);
		/* ort in dtm finden, entweder per rowid (Uebergabe), oder per ortid (dann durch dtm iterieren)
		* dann weiter mit ortDtm.setValueAt(aValue, row, column); fuer alle veraenderten Spalten
		*/ 
		int rowid = 0;
		for(int i = 0; i < ortDtm.getRowCount(); ++i) {
			if(ortDtm.getValueAt(i, ortDtm.findColumn("Ort ID")).equals(ort.getOrtid())) {
				rowid = i;
				break;
			}
		}
		
		/* 
		 * Hier wird die Aenderung des neuen Ort-Objektes
		 * gespeichert. Wenn der Vorgang erfolgreich war, 
		 * oeffnet sich ein ein Fenster mit dieser
		 * Information. 
		 */
		
		ortDtm.setValueAt((String) fieldData[2], rowid, ortDtm.findColumn("Adresse"));
		ortDtm.setValueAt((String) fieldData[3], rowid, ortDtm.findColumn("Ort"));
		
		if(OrtSQL.update(ort.getOrtid(), ort)) {
		
		JOptionPane.showMessageDialog(null,
				"Ort wurde gespeichert.",
				"Info", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
