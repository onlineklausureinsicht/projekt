/**
 * @author Marius Hagen
 * Controller der KlausurBearbeitenView. Er behandelt die Daten und stellt diese bereit.
 */

package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;

import models.Dozent;
import models.Klausur;
import models.Modul;
import models.Student;
import register.DozentRegister;
import register.KlausurRegister;
import register.ModulRegister;
import register.StudentRegister;

public class KlausurBearbeitenController {

	private KlausurRegister register;
	private DozentRegister dozentRegister;
	private ModulRegister modulRegister;
	private StudentRegister studentRegister;
	private DefaultTableModel klausurDtm;
	private int modelRow;
	
	public KlausurBearbeitenController(DefaultTableModel klausurDtm, KlausurRegister register, DozentRegister dozentRegister, ModulRegister modulRegister, StudentRegister studentRegister, int modelRow) {
		super();
		this.register = register;
		this.dozentRegister = dozentRegister;
		this.modulRegister = modulRegister;
		this.studentRegister = studentRegister;
		this.klausurDtm = klausurDtm;
		this.modelRow = modelRow;
	}

	/**
	 * Generiert die fertigen Eintraege der View und speist diese ueber den Rueckgabewert.
	 * @return Object[]
	 */
	public Object[] laden() {
		
		Object[] fieldData = new Object[4];
		fieldData[0] = Integer.parseInt(klausurDtm.getValueAt(modelRow, klausurDtm.findColumn("Klausur ID")).toString()); //klausurid
		
		Modul modul = modulRegister.getModul(klausurDtm.getValueAt(modelRow, klausurDtm.findColumn("Modulbezeichnung")).toString());
		String modbez = modul.getBezeichnung();
		fieldData[1] = modbez; //modul
		
		Student mat = studentRegister.getStudent(Integer.parseInt(klausurDtm.getValueAt(modelRow, klausurDtm.findColumn("Matrikelnummer")).toString()));
		String matCBVal = mat.getMatnr() + " (" + mat.getName() + ", " + mat.getVorname() + ")";
		fieldData[2] = matCBVal; //matnr //this one will be in form of "1003 (Name, Vorname)"
		
		Dozent doz = dozentRegister.getDozent(Integer.parseInt(klausurDtm.getValueAt(modelRow, klausurDtm.findColumn("Dozent ID")).toString()));
		String dozCBVal = doz.getDozentId() + " (" + doz.getName() + ", " + doz.getVorname() + ")";
		fieldData[3] = dozCBVal; //dozid //this one will be in form of "1003 (Name, Vorname)"
		
		return fieldData;
	}
	
	/**
	 * Hier werden die Eintraege der View zurueck in den Controller gespeist,
	 * der die Eintraege zerkleinert und an die Datenhaltungsschicht weitergibt.
	 * @param fieldData
	 */
	public void speichern(Object[] fieldData) {
		int klausurid = (int) fieldData[0];
		String modul = (String) fieldData[1];
		String mat = (String) fieldData[2]; //this one will be in form of "1003 (Name, Vorname)"
		String doz = (String) fieldData[3]; //this one will be in form of "1003 (Name, Vorname)"
		
		int dozid = Integer.parseInt( doz.substring(0, doz.indexOf(" (")) ); //just take only the key
		int matnr = Integer.parseInt( mat.substring(0, mat.indexOf(" (")) ); //just take only the key
		Klausur klausur = new Klausur(matnr, modul, dozid, "","","", klausurid);
		boolean done = register.setKlausur(klausurid, klausur);
		if(done == true)
		{
			klausurDtm.removeRow(modelRow);
			klausurDtm.addRow(klausur.getRow());
			JOptionPane.showMessageDialog(null,
					"Klausur wurde gespeichert.",
					"Info", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			JOptionPane.showMessageDialog(null,"Speichern fehlgeschlagen",
					"Info", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Stellt die Liste der Modul-ComboBox bereit
	 * @return String[]
	 */
	public String[] getModulCBValues() {
		
		ArrayList<String> al = new ArrayList<String>();
		for(Modul m : modulRegister.getModulMap().values()) {
            al.add(m.getBezeichnung());
        }
		return al.toArray( new String[al.size()] );
	}
	
	/**
	 * Stellt die Liste der Dozent-ComboBox bereit
	 * @return String[]
	 */
	public String[] getDozentCBValues() {
		ArrayList<String> al = new ArrayList<String>();
		for(Dozent d : dozentRegister.getDozentenMap().values()) {
            al.add(d.getDozentId() + " (" + d.getName() + ", " + d.getVorname() + ")");
            //this one will be in form of "1003 (Name, Vorname)"
        }
		return al.toArray( new String[al.size()] );
	}
	
	/**
	 * Stellt die Liste der Student-ComboBox bereit
	 * @return String[]
	 */
	public String[] getMatnrCBValues() {
		ArrayList<String> al = new ArrayList<String>();
		for(Student s : studentRegister.getStudentenMap().values()) {
            al.add(s.getMatnr() + " (" + s.getName() + ", " + s.getVorname() + ")");
            //this one will be in form of "1003 (Name, Vorname)"
        }
		return al.toArray( new String[al.size()] );
	}

}
