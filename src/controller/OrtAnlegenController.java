/**
 * @author Marius Hagen & Alexander Klippert
 * Stellt die Logik hinter OrtAnlegenView
 */

package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import models.Ort;
import register.OrtRegister;

public class OrtAnlegenController {
	
	private OrtRegister register;
	private DefaultTableModel ortDtm;
	

	public OrtAnlegenController(DefaultTableModel ortDtm, OrtRegister register) {
		super();
		this.register = register;
		this.ortDtm = ortDtm;
	}

	public void speichern(Object[] fieldData) {
		String standord = (String) fieldData[0];
		String raum = (String) fieldData[1];
		String adresse = (String) fieldData[2];
		String sort = (String) fieldData[3];
		int ortid = (int) fieldData[4];
				
		/* 
		 * Hier wird ein neues Ort-Objekt gespeichert. 
		 * Wenn der Vorgang erfolgreich war, 
		 * oeffnet sich ein ein Fenster mit 
		 * dieser Information. 
		 */
		
		Ort ort = new Ort(standord, raum, adresse, sort, ortid);
		register.addOrt(ort);
		ortDtm.addRow(ort.getRow());
		JOptionPane.showMessageDialog(null,
				"Ort wurde gespeichert.",
				"Info", JOptionPane.INFORMATION_MESSAGE);
		
	}

}
