package controller;

import models.LogInM2;
import views.LogInV2;
/**
 * @author Alexander Klippert
 *
 */
public class LogInC2 {

	private LogInV2 view;
	private LogInM2 model;

	public LogInC2(LogInM2 mdl) {
		// TODO Auto-generated constructor stub
		view = new LogInV2(this);
		model = mdl;
	}
	/*
	 * Erstellt ein model mit den Daten aus der
	 * LogInV2.java 
	 */

	public boolean login(String user, String pw) {
		return model.getLogin(user, pw);
	}
}
