package controller;

import java.awt.Container;

import javax.swing.table.DefaultTableModel;

import register.OrtRegister;
import views.OrtAnlegenView;
import views.OrtBearbeitenView;

/**
 * @author A. Klippert
 * 
 */
public class OrtAnzeigenController {
	private DefaultTableModel dtm;
	private OrtRegister ortRegister;
	private OrtBearbeitenController ortBearbeitenController;
	private OrtBearbeitenView ortBearbeitenView;
	private OrtAnlegenController ortAnlegenController;
	private OrtAnlegenView ortAnlegenView;

	public OrtAnzeigenController(DefaultTableModel dtm, OrtRegister ortRegister) {
		// super();
		this.dtm = dtm;
		this.ortRegister = ortRegister;
	}

	public void lschen(int row) {
		
		/*
		 * Hier wird der ausgewaehlte Ort (markierte Zeile)
		 * geloescht. mit deleteOrt(ortid) ueber das Register
		 * aus der Datenbank und mit dtm.removeRow(row) aus dem
		 * DefaultTableModel.
		 * Die OrtID wurde vorher aus dem DTM mit .getValueAt(row, 4)
		 * ge'parseInt't.
		 */
		
		int ortid = (int) Integer.parseInt(dtm.getValueAt(row, 4).toString());
		ortRegister.deleteOrt(ortid);
		dtm.removeRow(row);
	}

	public void ortAendern(Container desktopView ,int rowid) {
		// TODO Auto-generated method stub
		
		/*
		 * Diese Methode ist dafuer verantworlich,
		 * dass sich die OrtBearbeitenView.java oeffnet.
		 */

		ortBearbeitenController = new OrtBearbeitenController(dtm, ortRegister, rowid);
		ortBearbeitenView = new OrtBearbeitenView(ortBearbeitenController);
		desktopView.add(ortBearbeitenView);
	}

	public void ortNeu(Container desktopView) {
		// TODO Auto-generated method stub
		
		/*
		 * Diese Methode ist dafuer verantwortlich,
		 * dass sich die OrtAnlegenView.java oeffnet 
		 * und die OrtID mit lastKey() im Register hochzaehlt.
		 */

		ortAnlegenController = new OrtAnlegenController(dtm, ortRegister);
		ortAnlegenView = new OrtAnlegenView(ortAnlegenController, ortRegister.lastKey());
		desktopView.add(ortAnlegenView);

	}
}
