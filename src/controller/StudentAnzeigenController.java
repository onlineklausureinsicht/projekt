package controller;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import models.Klausur;
import models.Student;
import SQL.StudentSQL;
import register.KlausurRegister;
import register.StudentRegister;
import register.StudiengangRegister;
import views.StudentAendernView;
import views.StudentAnlegenView;
import views.StudentAnzeigenView;

/**Controller, der Grundansicht aller Studenten ermoeglich
 * und die Funktion Loeschen anbietet
 * @author A. Zensen
 *
 */
public class StudentAnzeigenController {
	private StudentRegister studentRegister;
	private DefaultTableModel studentDtm;	
	private StudiengangRegister studgangRegister;	
	private KlausurRegister klausurRegister;
	private DefaultTableModel klausurDtm;	
	
	private StudentAendernController studentAendernController;
	private StudentAendernView studentAendernView;	
	private StudentAnlegenController studentAnlegenController;
	private StudentAnlegenView studentAnlegenView;
	/**
	 * @param studentRegister
	 * @param studgangRegister
	 * @param studentDtm
	 * @param klausurRegister
	 * @param klausurDtm
	 */
	public StudentAnzeigenController(StudentRegister studentRegister, StudiengangRegister studgangRegister, DefaultTableModel studentDtm, KlausurRegister klausurRegister, DefaultTableModel klausurDtm) {
		super();		
		this.studentRegister = studentRegister;
		this.studgangRegister = studgangRegister;
		this.studentDtm = studentDtm;
		this.klausurRegister = klausurRegister;
		this.klausurDtm = klausurDtm;		
	}	
	/** Methode zum Anlegen eines neuen Studenten / Weiterleitung an StudentAnlegenController
	 * @param desktop
	 */
	public void anlegenStudent(Container desktop) {
		if(this.studentAnlegenView != null) {
			this.studentAnlegenView.dispose();
			desktop.remove(this.studentAnlegenView);
			this.studentAnlegenView = null;
		}		
		studentAnlegenController = new StudentAnlegenController(studentRegister, studentDtm);
		studentAnlegenView = new StudentAnlegenView(studentAnlegenController, this.studgangRegister.getCbm());
		desktop.add(studentAnlegenView);
		studentAnlegenView.toFront();		
	}
	/** Methode zum Loeschen eines Studenten
	 * @param desktop
	 * @param matnr
	 * @param rowid
	 */
	public void loeschenStudent(Container desktop, int matnr, int rowid) {
		System.out.println("Versuche Student mit matnr " + matnr + " zu entfernen...");
		
		if(studentAnlegenView != null)
			desktop.remove(studentAnlegenView);
		if(studentAendernView != null)
			desktop.remove(studentAendernView);
		
		
		if(StudentSQL.deleteStudent(studentRegister.getStudent(matnr))) {
			System.out.println("Erfolg in DB! Versuche Klausuren zu l�schen..");
			//Klausuren in DB sollten kaskadierend gel�scht worden sein.			
			//L�schen der Klausuren und Studenten aus DTMs / Register..
			int klCount = 0;
			for(Klausur kla : klausurRegister.getKlausurenMap().values()) {
				//System.out.println("Matnr der in der Iteration gew�hlten Klausur: " + kla.getMatrikelNummer());
				if(kla.getMatrikelNummer() == matnr) {
					for(int i = 0; i < klausurDtm.getRowCount(); i++) {
						if((int)klausurDtm.getValueAt(i, 0) == kla.getMatrikelNummer()) {
							System.out.println("L�sche Klausur mit Matnr: " + kla.getMatrikelNummer());
							klausurDtm.removeRow(i);
							klausurRegister.deleteKlausur(kla.getKlausurId());
						}
					}					
					klCount++;
				}
			}
			studentDtm.removeRow(rowid);
			studentRegister.deleteStudent(studentRegister.getStudent(matnr));
			JOptionPane.showMessageDialog(null,
					"Student wurde gel�scht. Au�erdem wurden " + klCount + " Klausuren entfernt.",
					"Info", JOptionPane.INFORMATION_MESSAGE);
		}		
	}
	/** Methode zum Aendern eines Studenten / Weiterleitung an StudentAendernController
	 * @param desktop
	 * @param matnr
	 * @param rowid
	 */
	public void aendernStudent(Container desktop, int matnr, int rowid) {
		if(this.studentAendernView != null) {
			this.studentAendernView.dispose();
			desktop.remove(this.studentAendernView);
			this.studentAendernView = null;
		}		
		Student stud = studentRegister.getStudent(matnr);				
		System.out.println("�bergabewerte an studentAendernView:");
		for(int i = 0; i < stud.getRow().length; i++) {
			System.out.println(stud.getRow()[i]);
		}
		studentAendernController = new StudentAendernController(this.studentRegister, this.studentDtm, rowid);		
		studentAendernView = new StudentAendernView(studentAendernController, stud.getRow(), this.studgangRegister.getCbm());
		System.out.println("F�ge studentAendernView zum Desktop hinzu..");
		desktop.add(studentAendernView);		
		studentAendernView.toFront();
	}	
}
