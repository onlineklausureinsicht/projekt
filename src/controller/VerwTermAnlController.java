/*
 ******************************************************** 
 *Datei erstellt von | am
 * Maik Nowack		| 08.12.14
 ********************************************************
 *Aenderung von		| am		| was
 *					|			|
 ********************************************************
 */

package controller;

import java.util.TreeMap;

import javax.swing.table.DefaultTableModel;

import models.Ort;
import models.Studiengang;
import models.Termin;

import register.OrtRegister;
import register.StudiengangRegister;
import register.TerminRegister;

/**
 * @author Maik Nowack
 *
 */
public class VerwTermAnlController {

	// TableModel
	private DefaultTableModel dtm;
	// TreeMap
	private TreeMap<Integer, Ort> orte;
	private TreeMap<String, Studiengang> studiengaenge;
	// Register
	private TerminRegister terminRegister;

	/**
	 * Konstruktor
	 * 
	 * @param dtm :DefaultTableModel
	 * @param terminRegister :TerminRegister
	 * @param ortRegister :OrtRegister
	 * @param studiengangRegister :StudiengangRegister
	 */
	public VerwTermAnlController(DefaultTableModel dtm,
			TerminRegister terminRegister, OrtRegister ortRegister,
			StudiengangRegister studiengangRegister) {

		this.dtm = dtm;
		this.orte = ortRegister.getOrte();
		this.studiengaenge = studiengangRegister.getStudiengangMap();
		this.terminRegister = terminRegister;

	}

	/**
	 * Diese Methode wird aufgerufen, um einen neuen Termin in der Datenbank einzupflegen.
	 * 
	 * @param fieldData
	 *            :Object[] - [0]: int (terminid) [1]: String (datum) [2]:
	 *            String (standort) [3]: String (raum) [4]: String (zeit) [5]:
	 *            String (zeit optional) [6]: boolean (sperre) [7]: String
	 *            (studiengang)
	 * @see Termin
	 * @return boolean: true wenn der Termin in der Datenbank eingetragen werden
	 *         konnte, false wenn ein Fehler aufgetreten ist und der Termin
	 *         nicht in der Datenbank eingetragen werden konnte.
	 */
	public boolean speichern(Object[] fieldData) {
		int terminID = (int) fieldData[0];
		String date = (String) fieldData[1];
		String standort = (String) fieldData[2];
		if (standort == null) {
			standort = new String("fb5");
		}
		String raum = (String) fieldData[3];
		if (raum == null) {
			raum = new String("Audimax");
		}
		String zeit1 = (String) fieldData[4];
		String zeit2 = (String) fieldData[5];
		boolean gesperrt = (boolean) fieldData[6];
		String studiengang = (String) fieldData[7];
		if (studiengang == null) {
			studiengang = new String("Wirtschaftsinformatik");
		}

		Termin termin = new Termin(terminID, date, standort, raum, zeit1,
				zeit2, gesperrt, studiengang);
		termin.setDate(date);

		boolean erg = terminRegister.addTermin(termin);

		if (erg)
			dtm.addRow(termin.getRow());

		return erg;
	}

	/**
	 * Wird zum befuellen der ComboBox benoetigt.
	 * 
	 * @return TreeMap<Integer, Ort> mit allen Orten die in der Datenbank hinterlegt sind.
	 */
	public TreeMap<Integer, Ort> getOrte() {
		return orte;
	}

	/**
	 * Wird zum befuellen der ComboBox benoetigt.
	 * 
	 * @return TreeMap<String, Studiengang> mit allen Studiengaengen die in der Datenbank hinterlegt sind.
	 */
	public TreeMap<String, Studiengang> getStudiengaenge() {
		return studiengaenge;
	}

}
