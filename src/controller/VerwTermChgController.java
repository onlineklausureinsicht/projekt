package controller;

import java.util.HashSet;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import defaults.SendMail;
import models.Ort;
import models.Student;
import models.Studiengang;
import models.Termin;
import register.KlausurRegister;
import register.OrtRegister;
import register.StudentRegister;
import register.StudiengangRegister;
import register.TerminKlausurRegister;
import register.TerminRegister;

/**
 * @author Maik Nowack
 *
 */
public class VerwTermChgController {
	
	//DefaultTableModel
	private DefaultTableModel dtm;
	
	//TreeMap
	private TreeMap<Integer, Ort> orte;
	private TreeMap<String, Studiengang> studiengaenge;
	private TreeMap<Integer, Student> studenten;
	
	//Register
	@SuppressWarnings("unused")
	private TerminRegister terminRegister;

	//Variable
	private int rowID;

	/**
	 * Konstruktor
	 * 
	 * @param dtm
	 *            :DefaultTableModel
	 * @param rowID
	 *            :int
	 * @param terminRegister
	 *            :TerminRegister
	 * @param ortRegister
	 *            :OrtRegister
	 * @param studiengangRegister
	 *            :StudiengangRegister
	 * @param klausurRegister
	 *            :KlausurRegister
	 * @param studentRegister
	 *            :StudentRegister
	 */
	public VerwTermChgController(DefaultTableModel dtm, int rowID,
			TerminRegister terminRegister, OrtRegister ortRegister,
			StudiengangRegister studiengangRegister,
			KlausurRegister klausurRegister, StudentRegister studentRegister) {

		this.dtm = dtm;
		this.orte = ortRegister.getOrte();
		this.studiengaenge = studiengangRegister.getStudiengangMap();
		this.studenten = studentRegister.getStudentenMap();
		this.rowID = rowID;
		this.terminRegister = terminRegister;

	}

	/**
	 * Wird zum befuellen der ComboBox benoetigt.
	 * 
	 * @return TreeMap<Integer, Ort> mit allen Orten die in der Datenbank
	 *         hinterlegt sind.
	 */
	public TreeMap<Integer, Ort> getOrte() {
		return orte;
	}

	/**
	 * Wird zum befuellen der ComboBox benoetigt.
	 * 
	 * @return TreeMap<String, Studiengang> mit allen Studiengaengen die in der
	 *         Datenbank hinterlegt sind.
	 */
	public TreeMap<String, Studiengang> getStudiengaenge() {
		return studiengaenge;
	}

	/**
	 * Diese Methode wird aufgerufen, um einen neuen Termin in der Datenbank zu
	 * aendern. Wenn sich zu dem Termin bereits Studenten angemeldet haben,
	 * oeffnet sich ein Dialog der Benutzername und Passwort zum versenden der
	 * Benachrichtungs E-Mail abfragt. Wird beim Dialog die Option 'Abbrechen'
	 * gewaehlt, wird keine E-Mail versendet.
	 * 
	 * @param fieldData
	 *            :Object[] - [0]: int (terminid) [1]: String (datum) [2]:
	 *            String (standort) [3]: String (raum) [4]: String (zeit) [5]:
	 *            String (zeit optional) [6]: boolean (sperre) [7]: String
	 *            (studiengang)
	 * @param old_fieldData
	 *            :Object[] - [0]: int (terminid) [1]: String (datum) [2]:
	 *            String (standort) [3]: String (raum) [4]: String (zeit) [5]:
	 *            String (zeit optional) [6]: boolean (sperre) [7]: String
	 *            (studiengang)
	 * @return boolean: true wenn der Termin in der Datenbank geaendert werden
	 *         konnte, false wenn ein Fehler aufgetreten ist und der Termin in
	 *         der Datenbank nicht geaendert werden konnte.
	 */
	public boolean change(Object[] fieldData, Object[] old_fieldData) {

		HashSet<Integer> matNr = new HashSet<Integer>();
		Termin old_termin, termin;
		boolean done = false;
		TerminKlausurRegister terminKlausurRegister = new TerminKlausurRegister();

		/*
		 * fieldData[0] = TerminID {Integer}, fieldData[1] = Datum {String},
		 * fieldData[2] = Standort {String}, fieldData[3] = Raum {String},
		 * fieldData[4] = Zeit {String}, fieldData[5] = Zeit (optional)
		 * {String}, fieldData[6] = Gesperrt {Boolean}, fieldData[7] =
		 * Studiengang {String}
		 */

		old_termin = new Termin(Integer.parseInt(old_fieldData[0].toString()),
				old_fieldData[1].toString(), old_fieldData[2].toString(),
				old_fieldData[3].toString(), old_fieldData[4].toString(),
				old_fieldData[5].toString(), (boolean) old_fieldData[6],
				old_fieldData[7].toString());
		termin = new Termin(Integer.parseInt(fieldData[0].toString()),
				fieldData[1].toString(), fieldData[2].toString(),
				fieldData[3].toString(), fieldData[4].toString(),
				fieldData[5].toString(), (boolean) fieldData[6],
				fieldData[7].toString());

		old_termin.setDate(old_fieldData[1].toString());
		termin.setDate(fieldData[1].toString());

		done = terminKlausurRegister.changeTerminKlausur(termin, old_termin);
		if (done) {
			dtm.removeRow(rowID);
			dtm.addRow(termin.getRow());
			matNr = terminKlausurRegister.getMatNr();

			if (matNr != null) {

				String from = new String("Terminverwaltung@fh-bielefeld.de");
				String subject = new String("Termin\u00E4nderung");
				String message = new String("Einsichtstermin alt: "
						+ old_fieldData[1].toString() + " "
						+ old_fieldData[4].toString()
						+ "\nEinsichtstermin neu:" + fieldData[1].toString()
						+ " " + fieldData[4].toString());

				JTextField username = new JTextField();
				JPasswordField password = new JPasswordField();
				Object[] input = { "Benutzername:", username, "Passwort:",
						password };

				int option = JOptionPane.showConfirmDialog(null, input,
						"E-Mail senden", JOptionPane.OK_CANCEL_OPTION);
				if (option == JOptionPane.OK_OPTION) {

					SendMail mail = new SendMail(null, null, from, subject,
							message);
					for (int m : matNr) {
						Student s;
						s = studenten.get(m);
						mail.setTo(s.getEmail());
						mail.send(username.getText(), new StringBuilder()
								.append(password.getPassword()).toString());
					}
				} else {
				}

			}
		}

		return done;
	}
}
